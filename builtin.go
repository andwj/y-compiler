// Copyright 2019 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

import "fmt"

type Builtin struct {
	name string
	kind *Type
	cl   *Closure
}

func SetupBuiltins() {
	// boolean logic
	RegisterBuiltin("and", "(lambda int int int)")
	RegisterBuiltin("or", "(lambda int int int)")
	RegisterBuiltin("not", "(lambda int int)")

	// comparisons
	RegisterBuiltin("int-eq?", "(lambda int int int)")
	RegisterBuiltin("int-lt?", "(lambda int int int)")
	RegisterBuiltin("int-gt?", "(lambda int int int)")
	RegisterBuiltin("int-ne?", "(lambda int int int)")
	RegisterBuiltin("int-le?", "(lambda int int int)")
	RegisterBuiltin("int-ge?", "(lambda int int int)")

	RegisterBuiltin("str-eq?", "(lambda str str int)")
	RegisterBuiltin("str-lt?", "(lambda str str int)")
	RegisterBuiltin("str-gt?", "(lambda str str int)")
	RegisterBuiltin("str-ne?", "(lambda str str int)")
	RegisterBuiltin("str-le?", "(lambda str str int)")
	RegisterBuiltin("str-ge?", "(lambda str str int)")

	// string manipulation
	RegisterBuiltin("hex$", "(lambda int str)")
	RegisterBuiltin("char$", "(lambda int str)")
	RegisterBuiltin("quote$", "(lambda str str)")
	RegisterBuiltin("parse-int", "(lambda str int)")

	RegisterBuiltin("str-len", "(lambda str int)")
	RegisterBuiltin("str-empty?", "(lambda str int)")
	RegisterBuiltin("str-subseq", "(lambda str int int str)")

	RegisterBuiltin("str-lower", "(lambda str str)")
	RegisterBuiltin("str-upper", "(lambda str str)")

	// character tests
	RegisterBuiltin("char-letter?", "(lambda int int)")
	RegisterBuiltin("char-digit?", "(lambda int int)")
	RegisterBuiltin("char-symbol?", "(lambda int int)")
	RegisterBuiltin("char-mark?", "(lambda int int)")
	RegisterBuiltin("char-space?", "(lambda int int)")
	RegisterBuiltin("char-control?", "(lambda int int)")

	RegisterBuiltin("char-lower", "(lambda int int)")
	RegisterBuiltin("char-upper", "(lambda int int)")

	// integer operations
	RegisterBuiltin("int-add", "(lambda int int int)")
	RegisterBuiltin("int-sub", "(lambda int int int)")
	RegisterBuiltin("int-mul", "(lambda int int int)")
	RegisterBuiltin("int-div", "(lambda int int int)")
	RegisterBuiltin("int-mod", "(lambda int int int)")
	RegisterBuiltin("int-pow", "(lambda int int int)")

	RegisterBuiltin("abs", "(lambda int int)")
	RegisterBuiltin("min", "(lambda int int int)")
	RegisterBuiltin("max", "(lambda int int int)")

	RegisterBuiltin("str-add", "(lambda str str str)")
	RegisterBuiltin("str-min", "(lambda str str str)")
	RegisterBuiltin("str-max", "(lambda str str str)")

	// bitwise operations
	RegisterBuiltin("bit-or", "(lambda int int int)")
	RegisterBuiltin("bit-and", "(lambda int int int)")
	RegisterBuiltin("bit-xor", "(lambda int int int)")
	RegisterBuiltin("bit-not", "(lambda int int)")

	RegisterBuiltin("shift-left", "(lambda int int int)")
	RegisterBuiltin("shift-right", "(lambda int int int)")

	// random numbers
	RegisterBuiltin("rand-seed", "(lambda int int)")
	RegisterBuiltin("rand-int", "(lambda int int)")
	RegisterBuiltin("rand-range", "(lambda int int int)")

	// I/O and misc
	RegisterBuiltin("error", "(lambda str int)")
	RegisterBuiltin("print", "(lambda str int)")
	RegisterBuiltin("input", "(lambda str str)")
	RegisterBuiltin("enable-editor", "(lambda int)")
}

func RegisterBuiltin(name string, typestr string) {
	kind := ParseTypeFromString(typestr)

	cl := new(Closure)
	cl.builtin = &Builtin{name, kind, cl}
	cl.kind = kind
	cl.debug_name = name

	MakeGlobal(name).loc.MakeFunction(cl)
}

//----------------------------------------------------------------------

type GenericBuiltin struct {
	name   string
	args   int
	result BaseType
	cl     *Closure

	validator func(children []*Type) error
}

var generic_builtins []*GenericBuiltin
var generic_lookup map[string]int

// BVAL_Same1 is a fake value kind indicating that the result
// of the generic builtin is same as the first argument.
const BVAL_First BaseType = 0x81

func SetupGenericBuiltins() {
	generic_builtins = make([]*GenericBuiltin, 0)
	generic_lookup = make(map[string]int)

	// array handling
	RegisterGeneric("len", 1, VAL_Int, CHK_Len)
	RegisterGeneric("empty?", 1, VAL_Int, CHK_Empty)
	RegisterGeneric("copy", 1, BVAL_First, CHK_Copy)
	RegisterGeneric("subseq", 3, BVAL_First, CHK_Subseq)

	RegisterGeneric("append!", 2, BVAL_First, CHK_Append)
	RegisterGeneric("insert!", 3, BVAL_First, CHK_Insert)
	RegisterGeneric("delete!", 2, BVAL_First, CHK_Delete)
	RegisterGeneric("join!", 2, BVAL_First, CHK_Join)

	RegisterGeneric("find", 2, VAL_Int, CHK_Find)
	RegisterGeneric("filter!", 2, BVAL_First, CHK_Filter)
	RegisterGeneric("map!", 2, BVAL_First, CHK_Map)
	RegisterGeneric("sort!", 2, BVAL_First, CHK_Sort)

	// miscellaneous
	RegisterGeneric("same-obj?", 2, VAL_Int, CHK_SameObj)
}

func RegisterGeneric(name string, args int, result BaseType,
	validator func([]*Type) error) {

	gen := new(GenericBuiltin)

	gen.name = name
	gen.args = args
	gen.result = result
	gen.validator = validator

	kind := NewType(result)

	cl := new(Closure)
	cl.builtin = &Builtin{name, kind, cl}
	cl.kind = kind
	cl.debug_name = name

	gen.cl = cl

	generic_lookup[name] = len(generic_builtins)
	generic_builtins = append(generic_builtins, gen)
}

//----------------------------------------------------------------------

type OperatorInfo struct {
	name string

	int_func *GlobalDef
	str_func *GlobalDef

	unary       bool
	precedence  int
	right_assoc bool
}

func (info *OperatorInfo) GetDef(kind BaseType) *GlobalDef {
	switch kind {
	case VAL_Int:
		return info.int_func

	case VAL_String:
		return info.str_func

	default:
		return nil
	}
}

// NOTE: to implement unary negation, would need a separate map
var operators map[string]*OperatorInfo

func SetupOperators() {
	operators = make(map[string]*OperatorInfo)

	// unary operators
	RegisterOperator(0, "!", "not", "")
	RegisterOperator(0, "~", "bit-not", "")

	// binary infix operators
	RegisterOperator(1, "||", "or", "")
	RegisterOperator(2, "&&", "and", "")

	RegisterOperator(3, "==", "int-eq?", "str-eq?")
	RegisterOperator(3, "!=", "int-ne?", "str-ne?")
	RegisterOperator(4, "<=", "int-le?", "str-le?")
	RegisterOperator(4, ">=", "int-ge?", "str-ge?")
	RegisterOperator(4, "<", "int-lt?", "str-lt?")
	RegisterOperator(4, ">", "int-gt?", "str-gt?")

	RegisterOperator(5, "+", "int-add", "str-add")
	RegisterOperator(5, "-", "int-sub", "")
	RegisterOperator(6, "*", "int-mul", "")
	RegisterOperator(6, "/", "int-div", "")
	RegisterOperator(6, "%", "int-mod", "")
	RegisterOperator(7, "**", "int-pow", "")

	RegisterOperator(5, "|", "bit-or", "")
	RegisterOperator(5, "^", "bit-xor", "")
	RegisterOperator(6, "&", "bit-and", "")

	RegisterOperator(6, "<<", "shift-left", "")
	RegisterOperator(6, ">>", "shift-right", "")
}

func RegisterOperator(prec int, op string, int_func, str_func string) {
	info := new(OperatorInfo)

	get_def := func(name string) *GlobalDef {
		if name == "" {
			return nil
		}
		idx, exist := global_lookup[name]
		if !exist {
			panic("unknown builtin '" + name + "' for operator")
		}

		def := globals[idx]
		loc := def.loc

		if loc.kind.base != VAL_Function || loc.Clos == nil || loc.Clos.builtin == nil {
			panic("invalid builtin '" + name + "' for operator")
		}

		return def
	}

	info.name = op

	info.int_func = get_def(int_func)
	info.str_func = get_def(str_func)

	info.unary = (prec == 0)
	info.precedence = prec
	info.right_assoc = (op == "**")

	operators[op] = info
}

func (op *OperatorInfo) IsComparison() bool {
	return op.precedence == 3 || op.precedence == 4
}

func HasOperator(t *Token) bool {
	for _, child := range t.Children {
		if child.Kind == TOK_Name {
			if operators[child.Str] != nil {
				return true
			}
		}
	}
	return false
}

//----------------------------------------------------------------------

func CHK_Len(children []*Type) error {
	a := children[0]
	if a.base != VAL_Array {
		return fmt.Errorf("len requires an array")
	}
	return Ok
}

func CHK_Empty(children []*Type) error {
	a := children[0]
	if a.base != VAL_Array {
		return fmt.Errorf("empty? requires an array")
	}
	return Ok
}

func CHK_Copy(children []*Type) error {
	a := children[0]
	if a.base != VAL_Array {
		return fmt.Errorf("copy requires an array")
	}
	return Ok
}

func CHK_Append(children []*Type) error {
	a := children[0]
	b := children[1]
	if a.base != VAL_Array {
		return fmt.Errorf("append! requires an array")
	}
	if !a.sub.Equal(b) {
		return fmt.Errorf("append! element is wrong type, wanted %s, got %s",
			a.sub.String(), b.String())
	}
	return Ok
}

func CHK_Insert(children []*Type) error {
	arr := children[0]
	idx := children[1]
	val := children[2]

	if arr.base != VAL_Array {
		return fmt.Errorf("insert! requires an array")
	}
	if idx.base != VAL_Int {
		return fmt.Errorf("delete! requires an integer index")
	}
	if !arr.sub.Equal(val) {
		return fmt.Errorf("insert! element is wrong type, wanted %s, got %s",
			arr.sub.String(), val.String())
	}
	return Ok
}

func CHK_Delete(children []*Type) error {
	a := children[0]
	b := children[1]
	if a.base != VAL_Array {
		return fmt.Errorf("delete! requires an array")
	}
	if b.base != VAL_Int {
		return fmt.Errorf("delete! requires an integer index")
	}
	return Ok
}

func CHK_Join(children []*Type) error {
	a := children[0]
	b := children[1]
	if a.base != VAL_Array || b.base != VAL_Array {
		return fmt.Errorf("join! requires two arrays")
	}
	if !a.Equal(b) {
		return fmt.Errorf("join! given incompatible types: %s and %s",
			a.String(), b.String())
	}
	return Ok
}

func CHK_Subseq(children []*Type) error {
	arr := children[0]
	start := children[1]
	end := children[2]

	if arr.base != VAL_Array {
		return fmt.Errorf("subseq requires an array, got %s",
			arr.String())
	}

	if start.base != VAL_Int {
		return fmt.Errorf("subseq requires an integer start, got %s",
			start.String())
	}

	if end.base != VAL_Int {
		return fmt.Errorf("subseq requires an integer end, got %s",
			end.String())
	}

	return Ok
}

func CHK_Find(children []*Type) error {
	arr := children[0]
	pred := children[1]

	if arr.base != VAL_Array {
		return fmt.Errorf("find requires an array, got %s",
			arr.String())
	}

	if pred.base != VAL_Function {
		return fmt.Errorf("find requires a predicate function, got %s",
			pred.String())
	}

	if len(pred.param) != 1 {
		return fmt.Errorf("find requires a predicate with one parameter")
	}

	p1 := pred.param[0].kind

	if !p1.Equal(arr.sub) {
		return fmt.Errorf("bad find predicate: parameter is wrong type, wanted %s, got %s",
			arr.sub.String(), p1.String())
	}

	if pred.sub.base != VAL_Int {
		return fmt.Errorf("bad find predicate: result is wrong type, wanted int, got %s",
			pred.sub.String())
	}

	return Ok
}

func CHK_Filter(children []*Type) error {
	arr := children[0]
	pred := children[1]

	if arr.base != VAL_Array {
		return fmt.Errorf("filter! requires an array, got %s",
			arr.String())
	}

	if pred.base != VAL_Function {
		return fmt.Errorf("filter! requires a predicate function, got %s",
			pred.String())
	}

	if len(pred.param) != 1 {
		return fmt.Errorf("filter! requires a predicate with one parameter")
	}

	p1 := pred.param[0].kind

	if !p1.Equal(arr.sub) {
		return fmt.Errorf("bad filter! predicate: parameter is wrong type, wanted %s, got %s",
			arr.sub.String(), p1.String())
	}

	if pred.sub.base != VAL_Int {
		return fmt.Errorf("bad filter! predicate: result is wrong type, wanted int, got %s",
			pred.sub.String())
	}

	return Ok
}

func CHK_Map(children []*Type) error {
	arr := children[0]
	pred := children[1]

	if arr.base != VAL_Array {
		return fmt.Errorf("map! requires an array, got %s",
			arr.String())
	}

	if pred.base != VAL_Function {
		return fmt.Errorf("map! requires a predicate function, got %s",
			pred.String())
	}

	if len(pred.param) != 1 {
		return fmt.Errorf("map! requires a predicate with one parameter")
	}

	p1 := pred.param[0].kind

	if !p1.Equal(arr.sub) {
		return fmt.Errorf("bad map! predicate: parameter is wrong type, wanted %s, got %s",
			arr.sub.String(), p1.String())
	}

	if !pred.sub.Equal(arr.sub) {
		return fmt.Errorf("bad map! predicate: result is wrong type, wanted %s, got %s",
			arr.sub.String(), pred.sub.String())
	}

	return Ok
}

func CHK_Sort(children []*Type) error {
	arr := children[0]
	pred := children[1]

	if arr.base != VAL_Array {
		return fmt.Errorf("sort! requires an array, got %s",
			arr.String())
	}

	if pred.base != VAL_Function {
		return fmt.Errorf("sort! requires a predicate function, got %s",
			pred.String())
	}

	if len(pred.param) != 2 {
		return fmt.Errorf("sort! requires a predicate with two parameters")
	}

	p1 := pred.param[0].kind
	p2 := pred.param[1].kind

	if !p1.Equal(arr.sub) || !p2.Equal(arr.sub) {
		return fmt.Errorf("bad sort! predicate: parameter is wrong type, wanted %s, got %s and %s",
			arr.sub.String(), p1.String(), p2.String())
	}

	if pred.sub.base != VAL_Int {
		return fmt.Errorf("bad sort! predicate: result is wrong type, wanted int, got %s",
			pred.sub.String())
	}

	return Ok
}

//----------------------------------------------------------------------

func CHK_SameObj(children []*Type) error {
	a_kind := children[0]
	b_kind := children[1]

	if !a_kind.Castable(b_kind) {
		return fmt.Errorf("same-obj? given incompatible types: %s and %s",
			a_kind.String(), b_kind.String())
	}

	switch a_kind.base {
	case VAL_Array, VAL_Tuple, VAL_Function:
		return Ok
	}

	return fmt.Errorf("same-obj? given non-object type: %s",
		a_kind.String())
}
