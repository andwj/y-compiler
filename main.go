// Copyright 2019 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

import "os"
import "os/exec"
import "path/filepath"

import "bufio"
import "bytes"
import "fmt"

import "gitlab.com/andwj/argv"

var Ok error = nil

/* command-line options */

var Options struct {
	memory   int  // fasm "-m" option value
	asm_only bool // only produce the .asm file
	verbose  bool // be more chatty
	help     bool
}

var Files struct {
	source   string
	assembly string
	output   string

	// current assembly output file
	out *os.File
}

type GlobalDef struct {
	// memory loc for the value
	loc *Value

	// its name (for debugging)
	name string
}

// all the global definitions
var globals []*GlobalDef
var global_lookup map[string]int

const GLOB_NIL = 0
const GLOB_TRUE = 1
const GLOB_EOF = 2

// the unparsed definitions of a program.
var raw_definitions []*Token

// the unparsed expressions of a program, which get compiled and
// executed after all the global definitions are handled.
var raw_expressions []*Token

//----------------------------------------------------------------------

func main() {
	err := HandleArgs()
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %s\n", err.Error())
		os.Exit(1)
	}

	SetupTypes()
	SetupOutputDefs()
	SetupGlobals()

	SetupBuiltins()
	SetupGenericBuiltins()
	SetupOperators()

	lambda_templates = make([]*Closure, 0)

	status := ProcessAllFiles()

	if Options.verbose && status == 0 {
		fmt.Printf("Success.\n")
	}

	os.Exit(status)
}

func HandleArgs() error {
	Options.memory = 64000

	argv.Generic("o", "output", &Files.output, "file", "the output executable")
	argv.Integer("m", "memory", &Options.memory, "kb", "memory used by fasm")
	argv.Enabler("S", "asm", &Options.asm_only, "only create the asm code")
	argv.Enabler("v", "verbose", &Options.verbose, "show what is being done")
	argv.Enabler("h", "help", &Options.help, "display this help text")

	err := argv.Parse()
	if err != nil {
		return err
	}

	if Options.help {
		fmt.Println("Usage: y-compiler input.yb [-o output]")
		fmt.Println("")
		fmt.Println("Available options:")
		argv.Display(os.Stdout)
		os.Exit(0)
	}

	unparsed := argv.Unparsed()

	if len(unparsed) == 0 {
		return fmt.Errorf("Missing input filename")
	} else if len(unparsed) > 1 {
		return fmt.Errorf("Too many input filenames")
	}

	Files.source = unparsed[0]

	if Files.output == "" {
		// WISH : this should be ".exe" for windows
		ext := ""

		Files.output = ReplaceExtension(Files.source, ext)
	}

	Files.assembly = ReplaceExtension(Files.output, ".asm")

	return Ok
}

func ReplaceExtension(fn string, ext string) string {
	old_ext := filepath.Ext(fn)

	if old_ext != "" {
		fn = fn[0 : len(fn)-len(old_ext)]
	}

	return fn + ext
}

func SetupGlobals() {
	globals = make([]*GlobalDef, 0)
	global_lookup = make(map[string]int)

	// need to create a string-def for the EOF global
	def := CreateStringDef("\x1A")
	def.label = "eof_str"

	MakeGlobal("NIL").loc.MakeInt(NIL)
	MakeGlobal("TRUE").loc.MakeInt(TRUE)
	MakeGlobal("EOF").loc.MakeString("\x1A")

	args_type := NewType(VAL_Array)
	args_type.sub = str_type

	args := MakeGlobal("ARGS")
	args.loc.MakeArray(0, args_type)
	CreateArrayDef(args.loc.Array)

	env_pair_type := NewType(VAL_Tuple)
	env_pair_type.AddParam("", str_type)
	env_pair_type.AddParam("", str_type)

	env_type := NewType(VAL_Array)
	env_type.sub = env_pair_type

	envs := MakeGlobal("ENVS")
	envs.loc.MakeArray(0, env_type)
	CreateArrayDef(envs.loc.Array)

	MakeGlobal("EXIT-STATUS").loc.MakeInt(0)
	MakeGlobal("STACK-TRACE").loc.MakeInt(8)
}

func ProcessAllFiles() (status int) {
	if Options.verbose {
		fmt.Printf("Opening file: %s\n", Files.assembly)
	}

	f, err := os.Open(Files.source)
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %s\n", err.Error())
		return 1
	}
	defer f.Close()

	return ProcessFile(f)
}

func ProcessFile(f *os.File) (status int) {
	var err error

	if Options.verbose {
		fmt.Printf("Creating file: %s\n", Files.assembly)
	}

	Files.out, err = os.Create(Files.assembly)
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %s\n", err.Error())
		return 2
	}

	lex := NewLexer(f)
	/// lex.DumpTokens()

	if Options.verbose {
		fmt.Printf("Compiling.....\n")
	}

	err = ProcessCode(lex)

	Files.out.Close()
	Files.out = nil

	if err != nil {
		DeleteAssembly()

		fmt.Fprintf(os.Stderr, "COMPILE ERROR: %s\n", err.Error())

		err = LocErrorWithFile(err, Files.source)

		switch t := err.(type) {
		case *LocError:
			s := t.Location()
			if s != "" {
				fmt.Fprintf(os.Stderr, "Occurred%s\n", s)
			}
		}

		return 3
	}

	if Options.asm_only {
		return 0
	}

	err = AssembleOutput()

	// remove intermediate assembly, except when -S is used
	DeleteAssembly()

	if err != nil {
		fmt.Fprintf(os.Stderr, "ASSEMBLY ERROR: %s\n", err.Error())
		return 4
	}

	// mark the output file as executable
	if Options.verbose {
		fmt.Printf("Marking file as executable: %s\n", Files.output)
	}

	err = os.Chmod(Files.output, 0755)
	if err != nil && Options.verbose {
		fmt.Printf("Failed to mark as executable: %s\n", err.Error())
	}

	// status zero means Ok
	return 0
}

func DeleteAssembly() {
	if Options.verbose {
		fmt.Printf("Deleting file: %s\n", Files.assembly)
	}

	os.Remove(Files.assembly)
}

func AssembleOutput() error {
	if Options.verbose {
		fmt.Printf("Building executable: %s\n", Files.output)
	}

	assembler := "fasm"

	mem_arg := fmt.Sprintf("%d", Options.memory)

	cmd := exec.Command(assembler, "-m", mem_arg, Files.assembly, Files.output)

	// run the command, get its stdout/stderr text
	text, err := cmd.CombinedOutput()

	// force showing assembler messages if it fails
	if Options.verbose || err != nil {
		DisplayAssemblerOutput(text)
	}

	return err
}

func DisplayAssemblerOutput(text []byte) {
	r := bytes.NewReader(text)

	sc := bufio.NewScanner(r)

	for sc.Scan() {
		fmt.Printf("| %s\n", sc.Text())
	}
}

//----------------------------------------------------------------------

func ProcessCode(lex *Lexer) error {
	var err error

	raw_definitions = make([]*Token, 0)
	raw_expressions = make([]*Token, 0)

	for {
		t := lex.Parse()

		if t.Kind == TOK_ERROR {
			return LocErrorf(t.LineNum, "%s", t.Str)
		}

		if t.Kind == TOK_EOF {
			break
		}

		err = ParseToken(t)
		if err != nil {
			// fallback line number in case we don't have any
			return LocErrorAt(err, t.LineNum)
		}
	}

	err = HandleAllDefinitions()
	if err != nil {
		return err
	}

	OutPreamble()

	err = CompileAllExpressions()
	if err != nil {
		return err
	}

	err = OutputAllFunctions()
	if err != nil {
		return err
	}

	OutLibraryCode()

	OutImmutableData()

	// a R/W segment must be last on Linux (it seems)
	OutMutableData()

	return Ok
}

func ParseToken(t *Token) error {
	if t.Kind == TOK_Expr {
		if len(t.Children) == 0 {
			return LocErrorf(t.LineNum, "empty expr in ()")
		}

		head := t.Children[0]

		if head.Match("type") {
			return ParseTypeDef(t.Children)
		}

		// remember a definition for later processing
		if head.Match("def") || head.Match("method") {
			raw_definitions = append(raw_definitions, t)
			return Ok
		}
	}

	// merely remember the expression, it will be handled later
	raw_expressions = append(raw_expressions, t)
	return Ok
}

func ParseTypeDef(children []*Token) error {
	lno := children[0].LineNum

	if len(children) < 2 {
		return LocErrorf(lno, "bad type def: missing name")
	}
	if len(children) < 3 {
		return LocErrorf(lno, "bad type def: missing type spec")
	}
	if len(children) > 3 {
		return LocErrorf(lno, "bad type def: extra rubbish at end")
	}

	name := children[1]
	spec := children[2]

	if name.Kind != TOK_Name {
		return LocErrorf(lno, "bad type def: name is not an identifier")
	}
	if !ValidDefName(name.Str) {
		return LocErrorf(lno, "bad type def: '%s' is a reserved word", name.Str)
	}
	if user_types[name.Str] != nil {
		return LocErrorf(lno, "bad type def: '%s' is already defined", name.Str)
	}

	kind, err := ParseType(spec)
	if err != nil {
		return LocErrorf(lno, "bad type def: %s", err.Error())
	}

	// create it
	user := kind.Copy()
	user.custom = name.Str

	user_types[name.Str] = user

	/// println("DEFINED", name.Str, "AS", stat.String())

	return Ok
}

//----------------------------------------------------------------------

func HandleAllDefinitions() error {
	for _, tok := range raw_definitions {
		err := HandleDefinition(tok)

		if err != nil {
			return err
		}
	}

	return Ok
}

func HandleDefinition(t *Token) error {
	head := t.Children[0]

	if head.Match("method") {
		return ParseMethodDef(t.Children)
	} else {
		return ParseDefinition(t.Children)
	}
}

func ParseDefinition(children []*Token) error {
	lno := children[0].LineNum

	if len(children) < 2 {
		return LocErrorf(lno, "bad definition: missing name")
	}
	if len(children) < 3 {
		return LocErrorf(lno, "bad definition: missing value")
	}
	if len(children) > 3 {
		return LocErrorf(lno, "bad definition: too many values")
	}

	name := children[1]
	val := children[2]

	if name.Kind != TOK_Name {
		return LocErrorf(lno, "bad definition: name is not an identifier")
	}

	// check for reserved keywords like "def", "if", "else"
	if !ValidDefName(name.Str) {
		return LocErrorf(lno, "bad definition: '%s' is a reserved word", name.Str)
	}

	// check for defining a variable to itself
	if val.Kind == TOK_Name && val.Str == name.Str {
		return LocErrorf(lno, "cannot define '%s' as itself", name.Str)
	}

	// global defs normally cannot be redefined
	_, exist := global_lookup[name.Str]
	if exist {
		return LocErrorf(lno, "global '%s' is already defined", name.Str)
	}

	def := MakeGlobal(name.Str)

	var err error

	// check for a lambda function
	if val.Kind == TOK_Expr && len(val.Children) > 0 &&
		val.Children[0].Match("fun") {

		err = ParseGlobalLambda(name.Str, def.loc, val, nil)
	} else {
		// normal literal
		err = def.loc.ParseLiteral(val)
	}

	if err != nil {
		return LocErrorAt(err, lno)
	}

	if def.loc.kind.base == VAL_String {
		CreateStringDef(def.loc.Str)
	}

	return Ok
}

func ParseMethodDef(children []*Token) error {
	lno := children[0].LineNum

	if len(children) < 2 {
		return LocErrorf(lno, "bad method def: missing type name")
	}
	if len(children) < 3 {
		return LocErrorf(lno, "bad method def: missing method name")
	}
	if len(children) < 4 {
		return LocErrorf(lno, "bad method def: missing function")
	}
	if len(children) > 4 {
		return LocErrorf(lno, "bad method def: extra rubbish at end")
	}

	type_name := children[1]

	if type_name.Kind != TOK_Name {
		return LocErrorf(lno, "bad method def: wanted type name, got %s",
			type_name.String())
	}

	user := user_types[type_name.Str]
	if user == nil {
		return LocErrorf(lno, "bad method def: unknown type '%s'", type_name.Str)
	}

	method_name := children[2]

	if method_name.Kind != TOK_Name {
		return LocErrorf(lno, "bad method def: wanted method name, got %s",
			method_name.String())
	}
	if !method_name.IsField() {
		return LocErrorf(lno, "bad method def, name must begin with a dot")
	}
	if user.base == VAL_Tuple && user.FindParam(method_name.Str) >= 0 {
		return LocErrorf(lno, "bad method def, '%s' is a field name",
			method_name.Str)
	}

	// methods must be unique per type
	if user.methods.Exists(method_name.Str) {
		return LocErrorf(lno, "bad method def, '%s' already defined",
			method_name.Str)
	}

	loc := user.methods.GetLoc(method_name.Str)

	fun_tok := children[3]

	// ensure we have a lambda function
	if !(fun_tok.Kind == TOK_Expr && len(fun_tok.Children) > 0 &&
		fun_tok.Children[0].Match("fun")) {

		return LocErrorf(lno, "bad method def, missing lambda function")
	}

	err := ParseGlobalLambda(method_name.Str, loc, fun_tok, user)
	if err != nil {
		return LocErrorAt(err, lno)
	}

	if loc.kind.base != VAL_Function {
	}

	return Ok
}

func ParseGlobalLambda(name string, loc *Value, tok *Token, method_type *Type) error {
	kind, err := ParseLambdaType(tok, method_type)
	if err != nil {
		return err
	}

	cl := new(Closure)
	cl.uncompiled = tok.Children
	cl.kind = kind
	cl.debug_name = name

	loc.MakeFunction(cl)

	if method_type != nil {
		full_name := method_type.custom + "::" + name

		cl._def = CreateFuncDef(cl, full_name)
	}

	return Ok
}

func ValidDefName(name string) bool {
	switch name {
	case "def", "fun", "method", "type", "macro":
		return false

	case "begin", "let", "set!", "self", "base":
		return false

	case "if", "else", "for", "for-each", "while", "str-for-each":
		return false

	case "int", "str", "array", "tuple":
		return false

	case "NIL", "TRUE", "EOF", "ARGS":
		return false

	case ":", "=", "$", "->", "...":
		return false
	}

	// disallow names of the operators
	_, exist := operators[name]
	if exist {
		return false
	}

	// disallow names of user types
	_, exist = user_types[name]
	if exist {
		return false
	}

	// disallow names of the builtins
	_, exist = generic_lookup[name]
	if exist {
		return false
	}

	g_idx, exist := global_lookup[name]
	if exist {
		loc := globals[g_idx].loc
		if loc.Clos != nil && loc.Clos.builtin != nil {
			return false
		}
	}

	return true
}

//----------------------------------------------------------------------

func CompileAllExpressions() error {
	var err error

	cl := new(Closure)

	cl.instructions = make([]Instruction, 0)
	cl.captures = make([]*LocalVar, 0)
	cl.debug_name = "#Expr#"

	// this is size of the dummy CallFrame, see OutPreamble()
	cl.stack_ofs = 3

	cl.PushLocalGroup()

	for idx, t := range raw_expressions {
		cl.cur_line = t.LineNum

		if idx > 0 {
			cl.EmitBlank()
		}

		if t.Kind != TOK_Expr {
			// we disallow bare literals in a code file
			return LocErrorf(t.LineNum, "expected expr in (), got: %s", t.String())
		}

		_, err = cl.CompileExpr(t, false /* tail */)
		if err != nil {
			return err
		}
	}

	// no PopLocalGroup() here, don't need to hoist captured vars
	// since the process is about to exit.

	OutClosure(cl)

	// no "ret" like normal functions, top-level expressions are just
	// part of one long piece of code which begins at "entry $" and a
	// jump to "exit" code in lib/runtime.asm to finish.

	OutMainReturn()

	return Ok
}

func OutputAllFunctions() error {
	// TODO: mark unused functions, omit from output and maybe
	//       have option to list them to stderr.

	for _, def := range globals {
		name := def.name
		loc := def.loc

		if loc.kind.base == VAL_Function &&
			loc.Clos.builtin == nil {

			err := CompileFunction(loc.Clos, name, nil)
			if err != nil {
				// remove the global (don't leave a dud one)
				DeleteGlobal(name)

				return LocErrorWithFunc(err, name)
			}

			// TODO: this smells like a hack
			if loc.Clos._def == nil {
				loc.Clos._def = CreateFuncDef(loc.Clos, name)
			}

			OutClosure(loc.Clos)
		}
	}

	/* handle all methods */

	for _, ty := range user_types {
		for name, loc := range ty.methods.locs {
			if loc.kind.base == VAL_Function {
				// create a name which cannot clash with a user identifier
				full_name := ty.custom + "::" + name

				err := CompileFunction(loc.Clos, full_name, ty)
				if err != nil {
					// remove the method, don't leave a dud one
					ty.methods.Delete(name)

					return LocErrorWithFunc(err, name)
				}

				OutClosure(loc.Clos)
			}
		}
	}

	/* handle lambda templates */

	for _, cl := range lambda_templates {
		OutClosure(cl)
	}

	return Ok
}

//----------------------------------------------------------------------

type LocError struct {
	Str  string
	Line int    // line # where error occurred, or 0
	File string // filename of error, or ""
	Func string // function which was being compiled, or ""
}

func LocErrorf(line int, format string, a ...interface{}) error {
	format = fmt.Sprintf(format, a...)

	return &LocError{format, line, "", ""}
}

func LocErrorAt(err error, line int) error {
	switch t := err.(type) {
	case *LocError:
		// an existing line number will be more accurate than the new one
		if t.Line == 0 {
			t.Line = line
		}
		return err
	}

	format := fmt.Sprintf("%s", err.Error())

	return &LocError{format, line, "", ""}
}

func LocErrorWithFile(err error, filename string) error {
	switch t := err.(type) {
	case *LocError:
		if t.File == "" {
			t.File = filename
		}
		return err
	}

	format := fmt.Sprintf("%s", err.Error())

	return &LocError{format, 0, filename, ""}
}

func LocErrorWithFunc(err error, funcname string) error {
	switch t := err.(type) {
	case *LocError:
		// don't replace any existing function name
		// [ important due to the compile-on-demand logic ]
		if t.Func == "" {
			t.Func = funcname
		}
		return err
	}

	format := fmt.Sprintf("%s", err.Error())

	return &LocError{format, 0, "", funcname}
}

func (err *LocError) Error() string {
	return err.Str
}

func (err *LocError) Location() string {
	l_part := ""
	if err.Line > 0 {
		l_part = fmt.Sprintf(" near line %d", err.Line)
	}

	f_part := ""
	if err.File != "" {
		f_part = " in " + filepath.Base(err.File)
	}

	g_part := ""
	if err.Func != "" && err.Func[0] == '.' {
		g_part = " in method '" + err.Func + "'"
	} else if err.Func != "" {
		g_part = " in function '" + err.Func + "'"
	}

	return l_part + f_part + g_part
}
