// Copyright 2019 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

import "fmt"
import "strings"

type BaseType int

const (
	VAL_Int BaseType = iota
	VAL_String
	VAL_Array
	VAL_Tuple
	VAL_Function
)

type Type struct {
	base BaseType

	// this is "" for plain types, or a name for custom types
	custom string

	// this is element type for arrays, return type for functions
	sub *Type

	// the parameters of a function, or elements of a tuple
	param []ParamInfo

	methods BindingGroup
}

type ParamInfo struct {
	// name of parameter or custom tuple field
	name string

	kind *Type
}

var int_type *Type
var str_type *Type
var exec_type *Type

var user_types map[string]*Type

//----------------------------------------------------------------------

func SetupTypes() {
	int_type = NewType(VAL_Int)
	str_type = NewType(VAL_String)

	// this is only used internally
	exec_type = NewType(VAL_Function)

	user_types = make(map[string]*Type)
}

func NewType(base BaseType) *Type {
	ty := new(Type)
	ty.base = base
	ty.param = make([]ParamInfo, 0)
	return ty
}

func (base BaseType) String() string {
	switch base {
	case VAL_Int:
		return "<Int>"
	case VAL_String:
		return "<String>"
	case VAL_Array:
		return "<Array>"
	case VAL_Tuple:
		return "<Tuple>"
	case VAL_Function:
		return "<Function>"
	default:
		return "!!!INVALID!!!"
	}
}

func ParseType(t *Token) (*Type, error) {
	if t.Match("int") {
		return int_type, Ok
	}

	if t.Match("str") {
		return str_type, Ok
	}

	// check for user defined types
	if t.Kind == TOK_Name {
		user := user_types[t.Str]

		if user != nil {
			return user, Ok
		}
	}

	if t.Kind == TOK_Expr && len(t.Children) > 0 && t.Children[0].Match("array") {
		if len(t.Children) != 2 {
			return nil, fmt.Errorf("malformed array type")
		}

		sub, err := ParseType(t.Children[1])
		if err != nil {
			return nil, err
		}

		res := NewType(VAL_Array)
		res.sub = sub

		return res, Ok
	}

	if t.Kind == TOK_Expr && len(t.Children) > 0 && t.Children[0].Match("tuple") {
		children := t.Children[1:]

		count := len(children)

		if count < 1 {
			return nil, fmt.Errorf("malformed tuple type")
		}

		// when field names are present, there must be a field name
		// before *every* element.
		has_fields := children[0].IsField()

		if has_fields {
			if count%2 != 0 {
				return nil, fmt.Errorf("malformed tuple type")
			}

			count /= 2
		}

		res := NewType(VAL_Tuple)

		// get types for each parameter
		for i := 0; i < count; i++ {
			var type_tok *Token
			var field string

			if has_fields {
				field_tok := children[i*2]

				if !field_tok.IsNamedField() {
					return nil, fmt.Errorf("bad field name in tuple type: %s", field_tok.Str)
				}

				field = field_tok.Str

				type_tok = children[i*2+1]
			} else {
				type_tok = children[i]
			}

			sub, err := ParseType(type_tok)
			if err != nil {
				return nil, err
			}

			res.AddParam(field, sub)
		}

		return res, Ok
	}

	if t.Kind == TOK_Expr && len(t.Children) > 0 && t.Children[0].Match("lambda") {
		num := len(t.Children)

		if num < 2 {
			return nil, fmt.Errorf("malformed function type")
		}

		// the return type is last
		sub, err := ParseType(t.Children[num-1])
		if err != nil {
			return nil, err
		}

		res := NewType(VAL_Function)
		res.sub = sub

		// get types for each parameter
		for i := 1; i < num-1; i++ {
			kind, err := ParseType(t.Children[i])
			if err != nil {
				return nil, err
			}

			res.AddParam("", kind)
		}

		return res, Ok
	}

	if t.Kind == TOK_Name {
		return nil, fmt.Errorf("unknown type '%s'", t.Str)
	}

	if t.Kind == TOK_Expr && len(t.Children) > 0 && t.Children[0].Kind == TOK_Name {
		return nil, fmt.Errorf("unknown type '(%s ...)'", t.Children[0].Str)
	}

	return nil, fmt.Errorf("malformed type, got: %s", t.String())
}

func ParseLambdaType(t *Token, method_type *Type) (*Type, error) {
	// this parses the full function type from the "(fun ...)" form.
	// the result Type will include the parameter names.
	// 'method_type' is only used for methods, otherwise is nil.

	if len(t.Children) < 2 {
		return nil, fmt.Errorf("missing body in fun")
	}

	kind := NewType(VAL_Function)

	// skip the "fun" and the body
	pars := t.Children[1 : len(t.Children)-1]

	// check for explicit return type
	if len(pars) >= 2 && pars[len(pars)-2].Match("->") {
		sub, err := ParseType(pars[len(pars)-1])
		if err != nil {
			return nil, err
		}

		kind.sub = sub

		pars = pars[0 : len(pars)-2]

	} else {
		// assume 'int' otherwise
		kind.sub = int_type
	}

	/* handle parameters... */

	for len(pars) > 0 {
		tok := pars[0]
		pars = pars[1:]

		if tok.Kind != TOK_Name {
			return nil, fmt.Errorf("bad parameter name: %s", tok.String())
		}

		// allow "self" only in first position
		if method_type != nil && len(kind.param) == 0 {
			if !tok.Match("self") {
				return nil, fmt.Errorf("first parameter of a method must be 'self'")
			}
		} else if !ValidDefName(tok.Str) {
			return nil, fmt.Errorf("bad parameter name: '%s' is a reserved word", tok.Str)
		}

		// check if duplicate
		for _, pi := range kind.param {
			if pi.name == tok.Str {
				return nil, fmt.Errorf("duplicate parameter name '%s'", tok.Str)
			}
		}

		// default type is INT
		param_kind := int_type

		// for methods, the "self" parameter should have the custom type
		if method_type != nil && len(kind.param) == 0 {
			param_kind = method_type
		}

		if len(pars) > 0 && pars[0].Match(":") {
			if len(pars) < 2 {
				return nil, fmt.Errorf("missing type spec for parameter '%s'", tok.Str)
			}

			if tok.Match("self") {
				return nil, fmt.Errorf("self parameter cannot have a type spec")
			}

			var err error
			param_kind, err = ParseType(pars[1])
			if err != nil {
				return nil, fmt.Errorf("parameter: %s", err.Error())
			}

			pars = pars[2:]
		}

		kind.AddParam(tok.Str, param_kind)
	}

	if method_type != nil && len(kind.param) == 0 {
		return nil, fmt.Errorf("missing self paramater for method")
	}

	return kind, Ok
}

func ParseTypeFromString(s string) *Type {
	// NOTE: this is only used for builtin types

	lex := NewLexer(strings.NewReader(s))
	tok := lex.Parse()

	if tok.Kind == TOK_EOF {
		panic("empty type string")
	}
	if tok.Kind == TOK_ERROR {
		panic("bad type string: " + tok.Str)
	}

	ty, err := ParseType(tok)
	if err != nil {
		panic("bad type string: " + err.Error())
	}

	return ty
}

func (ty *Type) String() string {
	if ty == nil {
		// return "!!NIL-TYPE!!"
		panic("nil given to Type.String()")
	}

	if ty.custom != "" {
		return ty.custom
	}

	switch ty.base {
	case VAL_Int:
		return "int"

	case VAL_String:
		return "str"

	case VAL_Array:
		return "(array " + ty.sub.String() + ")"

	case VAL_Tuple:
		s := "(tuple"
		for _, par := range ty.param {
			s = s + " " + par.kind.String()
		}
		return s + ")"

	case VAL_Function:
		s := "(fun "

		for _, par := range ty.param {
			s = s + par.kind.String() + " "
		}

		if ty.sub == nil {
			return s + "!!!UNKNOWN-RETURN!!! )"
		}

		return s + ty.sub.String() + ")"

	default:
		return "!!BAD-TYPE!!"
	}
}

func (ty *Type) Copy() *Type {
	res := NewType(ty.base)

	res.custom = ty.custom
	res.sub = ty.sub

	for i := range ty.param {
		res.AddParam(ty.param[i].name, ty.param[i].kind)
	}

	return res
}

func (ty *Type) AddParam(name string, kind *Type) {
	ty.param = append(ty.param, ParamInfo{name: name, kind: kind})
}

func (ty *Type) FindParam(name string) int {
	for i := range ty.param {
		if ty.param[i].name == name {
			return i
		}
	}

	// not found
	return -1
}

//----------------------------------------------------------------------

func (ty *Type) Equal(other *Type) bool {
	if ty == other {
		return true
	}

	if ty.base != other.base {
		return false
	}

	// if either is a user defined type, require an exact match
	// [ and no need to check sub and/or params ]
	if ty.custom != "" || other.custom != "" {
		return ty.custom == other.custom
	}

	if ty.sub != nil || other.sub != nil {
		if ty.sub == nil || other.sub == nil {
			return false
		}

		if !ty.sub.Equal(other.sub) {
			return false
		}
	}

	// for functions, check all parameters are the same.
	// for tuples, check all sub-types are the same
	if ty.base == VAL_Function || ty.base == VAL_Tuple {
		if len(ty.param) != len(other.param) {
			return false
		}

		for i := range ty.param {
			A := &ty.param[i]
			B := &other.param[i]

			if !A.kind.Equal(B.kind) {
				return false
			}
		}
	}

	return true
}

func (ty *Type) Castable(other *Type) bool {
	if ty == other {
		return true
	}

	if ty.base != other.base {
		return false
	}

	if ty.sub != nil || other.sub != nil {
		if ty.sub == nil || other.sub == nil {
			return false
		}

		if !ty.sub.Castable(other.sub) {
			return false
		}
	}

	// for functions, check all parameters are castable.
	// for tuples, check all sub-types are castable.
	if ty.base == VAL_Function || ty.base == VAL_Tuple {
		if len(ty.param) != len(other.param) {
			return false
		}

		for i := range ty.param {
			A := &ty.param[i]
			B := &other.param[i]

			if !A.kind.Castable(B.kind) {
				return false
			}
		}
	}

	return true
}

func (ty *Type) CastableFromUntyped(lit *Type, tok_kind TokenKind) bool {
	switch tok_kind {
	case TOK_Int, TOK_Float, TOK_Char, TOK_String:
		// ok
	case TOK_Array, TOK_Map:
		// ok
	default:
		return false
	}

	// value must be an untyped (*everywhere*)
	if lit.HasCustom() {
		return false
	}

	// wanted type must be a custom type (*anywhere*)
	if !ty.HasCustom() {
		return false
	}

	return ty.Castable(lit)
}

func (ty *Type) HasCustom() bool {
	if ty.custom != "" {
		return true
	}

	if ty.sub != nil {
		if ty.sub.HasCustom() {
			return true
		}
	}

	if ty.base == VAL_Function || ty.base == VAL_Tuple {
		for i := range ty.param {
			if ty.param[i].kind.HasCustom() {
				return true
			}
		}
	}

	return false
}

//----------------------------------------------------------------------

type CompactElem int

const (
	// these must match the ones in lib/format.asm

	TD_Int CompactElem = iota
	TD_String
	TD_Func
	TD_Array // element type follows
	TD_Tuple // next N words are offsets to sub-type, terminated by -1
)

func (ty *Type) Compactify() []CompactElem {
	res := make([]CompactElem, 1)

	switch ty.base {
	case VAL_Int:
		res[0] = TD_Int

	case VAL_String:
		res[0] = TD_String

	case VAL_Function:
		res[0] = TD_Func

	case VAL_Array:
		res[0] = TD_Array

		sub := ty.sub.Compactify()

		for _, elem := range sub {
			res = append(res, elem)
		}

	case VAL_Tuple:
		res[0] = TD_Tuple

		// allocate room for offsets (use dummy values)
		for range ty.param {
			res = append(res, 777)
		}

		// add terminating element
		res = append(res, -1)

		for idx, child := range ty.param {
			// update the offset value
			res[1+idx] = CompactElem(len(res))

			sub := child.kind.Compactify()

			for _, elem := range sub {
				res = append(res, elem)
			}
		}
	}

	return res
}
