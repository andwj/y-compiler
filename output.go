// Copyright 2019 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

import "fmt"
import "unicode"
import "strings"

type Instruction struct {
	lab, inst, dest, src, comment string

	LineNum int
}

func SetupOutputDefs() {
	string_defs = make([]*StringDef, 0)
	string_lookup = make(map[string]int)

	func_defs = make([]*FuncDef, 0)
	func_lookup = make(map[*Closure]int)

	ctype_defs = make([]*CompactTypeDef, 0)
	ctype_lookup = make(map[string]int)

	array_defs = make([]*ArrayDef, 0)
	array_lookup = make(map[*ArrayImpl]int)

	tuple_defs = make([]*TupleDef, 0)
	tuple_lookup = make(map[*ArrayImpl]int)

	// create some useful defs

	empty_str = CreateStringDef("")
	empty_str.label = "empty_str"

	space_str = CreateStringDef(" ")
	space_str.label = "space_str"
}

//----------------------------------------------------------------------

type StringDef struct {
	str   string
	label string
}

var string_defs []*StringDef
var string_lookup map[string]int

var empty_str *StringDef
var space_str *StringDef

func CreateStringDef(str string) *StringDef {
	idx, exist := string_lookup[str]
	if exist {
		return string_defs[idx]
	}

	def := new(StringDef)
	def.str = str
	def.label = fmt.Sprintf("string.%04d", len(string_defs))

	idx = len(string_defs)
	string_defs = append(string_defs, def)
	string_lookup[str] = idx

	return def
}

func OutStringDefs() {
	for _, def := range string_defs {
		def.Output()
	}
}

func (def *StringDef) Output() {
	OutLabel(def.label)

	// convert to runes
	r := []rune(def.str)

	len_str := fmt.Sprintf("%d", len(r))

	// show the string value on line with length
	safe_str := fmt.Sprintf("%q", def.str)
	if len(safe_str) > 50 {
		safe_str = safe_str[0:50]
		safe_str += "..."
	}

	len_str += " ; " + safe_str

	OutInst("dd", len_str, "", "")

	var sb strings.Builder

	for _, ch := range r {
		if sb.Len() > 60 {
			OutInst("dd", sb.String(), "", "")
			sb.Reset()
		}

		if sb.Len() > 0 {
			sb.WriteByte(',')
		}

		fmt.Fprintf(&sb, "%d", ch)
	}

	if sb.Len() > 0 {
		OutInst("dd", sb.String(), "", "")
	}

	OutLine("")
}

//----------------------------------------------------------------------

type FuncDef struct {
	cl    *Closure
	label string

	// the name of the function
	name     string
	name_def *StringDef

	// the name of the ASM code to implement function
	impl_name string

	// label for storing list of upvalues
	upval_label string
}

var func_defs []*FuncDef
var func_lookup map[*Closure]int

func CreateFuncDef(cl *Closure, name string) *FuncDef {
	idx, exist := func_lookup[cl]
	if exist {
		return func_defs[idx]
	}

	def := new(FuncDef)
	def.cl = cl
	def.label = fmt.Sprintf("funcdef.%04d", len(func_defs))

	def.name = name
	def.name_def = CreateStringDef(name)

	def.impl_name = "_" + EncodeIdent(name) + "_impl"

	def.upval_label = fmt.Sprintf("upvalues.%04d", len(func_defs))

	idx = len(func_defs)
	func_defs = append(func_defs, def)
	func_lookup[cl] = idx

	return def
}

func OutFunctionDefs() {
	for _, def := range func_defs {
		def.Output()
	}
}

func (def *FuncDef) Output() {
	OutLabel(def.label)

	// code pointer
	OutInst("dq", def.impl_name, "", "")

	// name field
	OutInst("dq", def.name_def.label, "", "")

	// upvalues (only used by template lambdas)
	if len(def.cl.captures) == 0 {
		OutInst("dq", "-1", "", "")
	} else {
		OutInst("dq", def.upval_label, "", "")
	}

	OutLine("")

	if len(def.cl.captures) > 0 {
		OutLabel(def.upval_label)
		OutInst("dd", fmt.Sprintf("%d", len(def.cl.captures)), "", "")

		for _, lvar := range def.cl.captures {
			ofs := lvar.offset

			// handle variables captured in a lambda within a lambda.
			// NOTE: we use AddCapture just to find it (it was already added).
			if lvar.owner != def.cl.parent {
				ofs = def.cl.parent.AddCapture(lvar)
				ofs |= 0x80000000
			}

			OutInst("dd", fmt.Sprintf("0x%x", ofs), "", "")
		}

		OutLine("")
	}
}

//----------------------------------------------------------------------

type CompactTypeDef struct {
	ty    *Type
	elems []CompactElem
	label string
}

var ctype_defs []*CompactTypeDef
var ctype_lookup map[string]int

func CreateCompactTypeDef(ty *Type) *CompactTypeDef {
	tspec := ty.String()

	idx, exist := ctype_lookup[tspec]
	if exist {
		return ctype_defs[idx]
	}

	def := new(CompactTypeDef)
	def.ty = ty
	def.elems = ty.Compactify()
	def.label = fmt.Sprintf("ctypedef.%04d", len(ctype_defs))

	idx = len(ctype_defs)
	ctype_defs = append(ctype_defs, def)
	ctype_lookup[tspec] = idx

	return def
}

func OutCompactTypes() {
	for _, def := range ctype_defs {
		def.Output()
	}
}

func (def *CompactTypeDef) Output() {
	OutLabel(def.label)

	// round # of elements so total size is a multiple of 8
	for len(def.elems)%4 != 0 {
		def.elems = append(def.elems, 0)
	}

	var sb strings.Builder

	for _, elem := range def.elems {
		if sb.Len() > 60 {
			OutInst("dw", sb.String(), "", "")
			sb.Reset()
		}

		if sb.Len() > 0 {
			sb.WriteByte(',')
		}

		fmt.Fprintf(&sb, "%d", elem)
	}

	if sb.Len() > 0 {
		OutInst("dw", sb.String(), "", "")
	}

	OutLine("")
}

//----------------------------------------------------------------------

type ArrayDef struct {
	impl  *ArrayImpl
	label string

	data_label string
}

var array_defs []*ArrayDef
var array_lookup map[*ArrayImpl]int

func CreateArrayDef(impl *ArrayImpl) *ArrayDef {
	idx, exist := array_lookup[impl]
	if exist {
		return array_defs[idx]
	}

	def := new(ArrayDef)
	def.impl = impl

	idx = len(array_defs)
	array_defs = append(array_defs, def)
	array_lookup[impl] = idx

	def.label = fmt.Sprintf("array.%04d", idx)
	def.data_label = fmt.Sprintf("elems.%04d", idx)
	return def
}

func OutArrayDefs() {
	for _, def := range array_defs {
		def.Output()
	}
}

func (def *ArrayDef) Output() {
	OutLabel(def.label)

	num_elems := len(def.impl.data)
	lencap := fmt.Sprintf("%d, %d", num_elems, num_elems)

	OutInst("dd", lencap, "", "")

	if len(def.impl.data) == 0 {
		// no element pointer for an empty array
		OutInst("dq", "-1", "", "")

	} else {
		OutInst("dq", def.data_label, "", "")
		OutLine("")

		OutLabel(def.data_label)

		for _, sub := range def.impl.data {
			sub.OutputData()
		}
	}

	OutLine("")
}

//----------------------------------------------------------------------

type TupleDef struct {
	impl  *ArrayImpl
	label string
}

var tuple_defs []*TupleDef
var tuple_lookup map[*ArrayImpl]int

func CreateTupleDef(impl *ArrayImpl) *TupleDef {
	idx, exist := tuple_lookup[impl]
	if exist {
		return tuple_defs[idx]
	}

	def := new(TupleDef)
	def.impl = impl

	idx = len(tuple_defs)
	tuple_defs = append(tuple_defs, def)
	tuple_lookup[impl] = idx

	def.label = fmt.Sprintf("tuple.%04d", idx)
	return def
}

func OutTupleDefs() {
	for _, def := range tuple_defs {
		def.Output()
	}
}

func (def *TupleDef) Output() {
	OutLabel(def.label)

	for _, sub := range def.impl.data {
		sub.OutputData()
	}

	OutLine("")
}

//----------------------------------------------------------------------

func OutGlobalVars() {
	for _, def := range globals {
		def.Output()
	}

	OutLine("")
}

func (def *GlobalDef) Output() {
	v := def.loc

	// skip unused functions
	if v.kind.base == VAL_Function {
		if v.Clos._def == nil {
			return
		}
	}

	label := EncodeIdent(def.name)
	OutLabel(label)

	v.OutputData()
}

//----------------------------------------------------------------------

func OutPreamble() {
	OutLine(";;")
	OutLine(";; %s", Files.source)
	OutLine(";;")
	OutLine(";; (output of the Yewberry compiler)")
	OutLine(";;")
	OutLine("")

	OutLine("include 'lib/yewberry.asm'")
	OutLine("include 'lib/linux_sys64.asm'")
	OutLine("")

	// TODO: this is Linux, support Windows and MacOS X
	OutLine("format ELF64 executable at 0000000100000000h")
	OutLine("")

	OutLine("segment readable executable")
	OutLine("")

	OutLine("entry $")

	OutLine("\t; dummy CallFrame for top level expressions")
	OutInst("xor", "rax", "rax", "")
	OutInst("push", "rax", "", "old %rbp (NULL)")
	OutInst("mov", "rbp", "rsp", "")
	OutInst("push", "rax", "", "info")
	OutInst("push", "rax", "", "code ptr")
	OutInst("push", "rax", "", "upvalues")
	OutLine("")

	OutInst("call", "init", "", "")
	OutLine("")
}

func OutMainReturn() {
	OutInst("jmp", "exit", "", "")
	OutLine("")
}

func OutLibraryCode() {
	OutLine(";")
	OutLine(";  Library code")
	OutLine(";")
	OutLine("include 'lib/io.asm'")
	OutLine("include 'lib/debug.asm'")
	OutLine("include 'lib/memory.asm'")
	OutLine("include 'lib/runtime.asm'")
	OutLine("")
	OutLine("include 'lib/math.asm'")
	OutLine("include 'lib/array.asm'")
	OutLine("include 'lib/string.asm'")
	OutLine("include 'lib/format.asm'")
	OutLine("include 'lib/random.asm'")
	OutLine("include 'lib/sort.asm'")

	OutLine("")
}

func OutImmutableData() {
	OutLine(";")
	OutLine(";  Immutable Data")
	OutLine(";")
	OutLine("segment readable")
	OutLine("")

	OutLine("include 'lib/tables.asm'")
	OutLine("")

	OutStringDefs()
	OutFunctionDefs()
	OutCompactTypes()
}

func OutMutableData() {
	OutLine(";")
	OutLine(";  Mutable Data")
	OutLine(";")
	OutLine("segment readable writable")
	OutLine("")

	OutLine("include 'lib/vars.asm'")
	OutLine("")

	OutArrayDefs()
	OutTupleDefs()
	OutGlobalVars()
}

func OutClosure(cl *Closure) {
	if cl._def != nil {
		OutLabel(cl._def.impl_name)
	}

	for _, ins := range cl.instructions {
		if ins.lab != "" {
			OutLabel(ins.lab)
		}

		if ins.lab == "" && ins.inst == "" {
			OutLine("")
		} else if ins.inst != "" {
			OutInst(ins.inst, ins.dest, ins.src, ins.comment)
		}
	}

	OutLine("")
}

func OutInst(inst, dest, src, comment string) {
	if dest != "" && src != "" {
		dest = dest + "," + src
	}

	if comment != "" {
		dest = dest + "\t; " + comment
	}

	OutLine("\t%s\t%s", inst, dest)
}

func OutLabel(name string) {
	OutLine("%s:", name)
}

func OutLine(format string, a ...interface{}) {
	_, err := fmt.Fprintf(Files.out, format, a...)
	if err != nil {
		// TODO abort compilation?
	}

	fmt.Fprintf(Files.out, "\n")
}

//----------------------------------------------------------------------

func EncodeLocal(offset int) string {
	return fmt.Sprintf("[rbp-%d]", offset*8)
}

func EncodeIdent(name string) string {
	// encode identifiers to be compatible with FASM:
	//   1. prepend an underscore
	//   2. convert '-' to an underscore (except at start)
	//   3. escape non-alphanumeric chars using %XX or %uXXXX

	var sb strings.Builder

	for pos, ch := range []rune(name) {
		if ch > 0xFFFF {
			fmt.Fprintf(&sb, "%%U%08X", ch)
		} else if ch > 0xFF {
			fmt.Fprintf(&sb, "%%u%04X", ch)
		} else if ch == '-' && pos > 0 {
			sb.WriteByte('_')
		} else if ValidIdentChar(ch) {
			sb.WriteByte(byte(ch))
		} else {
			fmt.Fprintf(&sb, "%%%02X", ch)
		}
	}

	return "_" + sb.String()
}

func ValidIdentChar(ch rune) bool {
	// NOTE: this is quite conservative.
	// in particular, the underscore is NOT allowed.

	if unicode.IsLetter(ch) {
		return true
	}

	if unicode.IsDigit(ch) {
		return true
	}

	return false
}
