// Copyright 2019 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

import "fmt"
import "strings"

const NIL = int32(-0x80000000)
const TRUE = int32(0x7fffffff)

type Value struct {
	kind *Type

	Int   int32
	Str   string
	Array *ArrayImpl
	Tuple *ArrayImpl
	Clos  *Closure
}

type ArrayImpl struct {
	data []Value
}

// BindingGroup contains the memory locations where variables are
// stored.  Once created, variables never change their address,
// although variables may cease to exist (e.g. when a let scope
// finishes) and then their memory can be reclaimed and re-used.
type BindingGroup struct {
	// this can stay nil until a binding is added
	locs map[string]*Value
}

//----------------------------------------------------------------------

// Copy stores a direct (bit-for-bit) copy of the other value into
// this Value struct.
func (v *Value) Copy(other *Value) {
	*v = *other
}

func (v *Value) CopyArray() Value {
	v.VerifyType(VAL_Array, "CopyArray")

	// this is a shallow copy: the copy (and its elements) will
	// occupy new memory locations, but the elements themselves
	// can alias the values of the original.

	res := Value{}
	res.MakeArray(len(v.Array.data), v.kind)

	copy(res.Array.data, v.Array.data)

	return res
}

func (v *Value) CopyTuple() Value {
	v.VerifyType(VAL_Tuple, "CopyTuple")

	// this is a shallow copy, the copy (and its elements) will
	// occupy new memory locations, but the elements themselves
	// can alias the values of the original.

	res := Value{}
	res.MakeTuple(v.kind)

	copy(res.Tuple.data, v.Tuple.data)

	return res
}

func (v *Value) MakeInt(num int32) {
	v.kind = int_type

	v.Int = num
	v.Str = ""
	v.Array = nil
	v.Tuple = nil
	v.Clos = nil
}

func (v *Value) MakeNIL() {
	v.kind = int_type

	v.Int = NIL
	v.Str = ""
	v.Array = nil
	v.Tuple = nil
	v.Clos = nil
}

func (v *Value) MakeBool(b bool) {
	v.kind = int_type

	v.Str = ""
	v.Array = nil
	v.Tuple = nil
	v.Clos = nil

	if b {
		v.Int = TRUE
	} else {
		v.Int = NIL
	}
}

func (v *Value) MakeString(s string) {
	v.kind = str_type

	v.Str = s
	v.Array = nil
	v.Tuple = nil
	v.Clos = nil
}

func (v *Value) MakeArray(size int, kind *Type) {
	impl := new(ArrayImpl)
	impl.data = make([]Value, size)

	v.kind = kind

	v.Array = impl
	v.Tuple = nil
	v.Str = ""
	v.Clos = nil
}

func (v *Value) MakeTuple(kind *Type) {
	impl := new(ArrayImpl)
	impl.data = make([]Value, len(kind.param))

	v.kind = kind

	v.Tuple = impl
	v.Array = nil
	v.Str = ""
	v.Clos = nil
}

func (v *Value) MakeFunction(cl *Closure) {
	if cl.builtin != nil {
		v.kind = cl.builtin.kind
	} else {
		v.kind = cl.kind
	}

	v.Clos = cl
	v.Str = ""
	v.Array = nil
	v.Tuple = nil
}

//----------------------------------------------------------------------

func (v *Value) IsNIL() bool {
	return (v.kind.base == VAL_Int) && (v.Int == NIL)
}

func (v *Value) IsTrue() bool {
	// everything except "NIL" is considered true
	if v.kind.base == VAL_Int && v.Int == NIL {
		return false
	}
	return true
}

func (v *Value) BasicEqual(other *Value) bool {
	if v.kind.base != other.kind.base {
		return false
	}

	switch v.kind.base {
	case VAL_Int:
		return v.Int == other.Int

	case VAL_String:
		return v.Str == other.Str

	case VAL_Function:
		return v.Clos == other.Clos

	default:
		// we do not compare arrays or maps
		return false
	}
}

func (v *Value) VerifyType(base BaseType, where string) {
	if v.kind.base != base {
		panic(where + ": verify type failed")
	}
}

//----------------------------------------------------------------------

func (v *Value) GetElem(idx int) Value {
	return v.Array.data[idx]
}

func (v *Value) SetElem(idx int, elem Value) {
	v.Array.data[idx] = elem
}

func (v *Value) SwapElems(i, k int) {
	A := v.Array.data[i]
	B := v.Array.data[k]

	v.Array.data[i] = B
	v.Array.data[k] = A
}

func (v *Value) AppendElem(elem Value) {
	v.Array.data = append(v.Array.data, elem)
}

func (v *Value) InsertElem(idx int, elem Value) {
	// an index <= zero will insert at the beginning (prepend).
	// an index >= length will insert at the end (append).

	if idx < 0 {
		idx = 0
	}
	if idx >= len(v.Array.data) {
		v.AppendElem(elem)
		return
	}

	// resize the array
	v.Array.data = append(v.Array.data, Value{})

	// shift elements up
	copy(v.Array.data[idx+1:], v.Array.data[idx:])

	// store new element
	v.Array.data[idx] = elem
}

func (v *Value) DeleteElem(idx int) {
	size := len(v.Array.data)

	// trying to remove non-existent elements is a no-op
	if idx < 0 || idx >= size {
		return
	}

	// shift elements down, if necessary
	if idx < size {
		copy(v.Array.data[idx:], v.Array.data[idx+1:])
	}

	// shrink the slice
	v.Array.data = v.Array.data[0 : size-1]
}

func (v *Value) ValidIndex(idx int) bool {
	if idx < 0 {
		return false
	}
	if idx >= len(v.Array.data) {
		return false
	}
	return true
}

func (v *Value) Length() int {
	switch v.kind.base {
	case VAL_Array:
		return len(v.Array.data)

	case VAL_Tuple:
		return len(v.Tuple.data)

	case VAL_String:
		return len([]rune(v.Str))

	default:
		return 1
	}
}

//----------------------------------------------------------------------

func (bg *BindingGroup) Exists(name string) bool {
	_, ok := bg.locs[name]
	return ok
}

func (bg *BindingGroup) SetLoc(name string, loc *Value) {
	if bg.locs == nil {
		bg.locs = make(map[string]*Value)
	}

	bg.locs[name] = loc
}

func (bg *BindingGroup) GetLoc(name string) *Value {
	loc, exist := bg.locs[name]
	if exist {
		return loc
	}

	// create a new memory location for the variable
	loc = new(Value)
	loc.MakeNIL()

	bg.SetLoc(name, loc)

	return loc
}

func (bg *BindingGroup) Delete(name string) {
	delete(bg.locs, name)
}

func MakeGlobal(name string) *GlobalDef {
	idx, exist := global_lookup[name]
	if exist {
		return globals[idx]
	}

	// create a new memory location for the variable
	loc := new(Value)
	loc.MakeNIL()

	def := new(GlobalDef)
	def.loc = loc
	def.name = name

	global_lookup[name] = len(globals)
	globals = append(globals, def)

	return def
}

func DeleteGlobal(name string) {
	// this is only for the REPL, used to prevent a dud function
	// hanging around if compiling it failed.

	idx, exist := global_lookup[name]
	if exist {
		globals[idx].loc = new(Value)
		globals[idx].loc.MakeNIL()
	}

	delete(global_lookup, name)
}

//----------------------------------------------------------------------

func (v *Value) ParseLiteral(lit *Token) (err error) {
	switch lit.Kind {
	case TOK_Int:
		err = v.ParseInt(lit.Str)

	case TOK_Float:
		err = v.ParseFloat(lit.Str)

	case TOK_Char:
		err = v.ParseChar(lit.Str)

	case TOK_String:
		err = v.ParseString(lit.Str)

	case TOK_Expr:
		err = v.ParseExpr(lit.Children)

	case TOK_Array:
		err = v.ParseArray(lit.Children)

	case TOK_Map:
		err = v.ParseTuple(lit.Children)

	case TOK_Name:
		err = v.ParseName(lit.Str)

	default:
		err = fmt.Errorf("bad literal value, got: %s", lit.String())
	}

	// ensure error line number is useful with big data structures
	if err != nil {
		err = LocErrorAt(err, lit.LineNum)
	}

	return
}

func (v *Value) ParseInt(s string) error {
	// we want to handle values >= 0x80000000, which would normally
	// overflow an int32.  so we use int64 and check for overflow.

	var long_val int64

	n, _ := fmt.Sscanf(s, "%v", &long_val)
	if n != 1 {
		return fmt.Errorf("bad integer '%s'", s)
	}

	if long_val < -0x80000000 || long_val >= 0xffffffff {
		return fmt.Errorf("integer too large for 32 bits")
	}

	if long_val >= 0x80000000 {
		long_val -= 0x100000000
	}

	v.MakeInt(int32(long_val))

	return Ok
}

func (v *Value) ParseFloat(s string) error {
	return fmt.Errorf("floating point numbers are not supported")
}

func (v *Value) ParseChar(s string) error {
	runes := []rune(s)

	if len(runes) == 0 {
		return fmt.Errorf("malformed character literal")
	}

	v.MakeInt(int32(runes[0]))

	return Ok
}

func (v *Value) ParseString(s string) error {
	// too easy!
	v.MakeString(s)

	return Ok
}

func (v *Value) ParseName(s string) error {
	// look up existing definition
	idx, exist := global_lookup[s]
	if !exist {
		return fmt.Errorf("no such global var '%s'", s)
	}

	def := globals[idx]

	v.Copy(def.loc)

	return Ok
}

func (v *Value) ParseExpr(children []*Token) error {
	if len(children) == 0 {
		return fmt.Errorf("empty expression in ()")
	}

	// see if the expression is a type-cast
	head := children[0]

	if head.Kind == TOK_Name && user_types[head.Str] != nil {
		return v.ParseCast(children, user_types[head.Str])
	}

	// nothing else is acceptable
	return fmt.Errorf("cannot use expression in non-code context")
}

func (v *Value) ParseCast(children []*Token, user *Type) error {
	if len(children) < 2 {
		return fmt.Errorf("missing argument to type cast")
	} else if len(children) > 2 {
		return fmt.Errorf("too many arguments in type cast")
	}

	err := v.ParseLiteral(children[1])
	if err != nil {
		return err
	}

	if !v.kind.Castable(user) {
		return fmt.Errorf("type mismatch in cast, wanted %s, got %s",
			user.String(), v.kind.String())
	}

	v.kind = user

	return Ok
}

func (v *Value) ParseArray(children []*Token) error {
	var elem_type *Type
	var err error

	// check for type specifier
	if len(children) > 0 && children[0].Match(":") {
		if len(children) < 2 {
			return fmt.Errorf("missing type spec in array literal")
		}

		elem_type, err = ParseType(children[1])
		if err != nil {
			return fmt.Errorf("array literal: %s", err.Error())
		}

		children = children[2:]
	}

	if len(children) == 0 {
		if elem_type == nil {
			return fmt.Errorf("empty array literal without type spec")
		}

		arr_type := NewType(VAL_Array)
		arr_type.sub = elem_type

		v.MakeArray(0, arr_type)
		CreateArrayDef(v.Array)

		return Ok
	}

	for i, t := range children {
		var elem Value

		err = elem.ParseLiteral(t)
		if err != nil {
			return err
		}

		if elem_type == nil {
			elem_type = elem.kind

		} else if elem_type.CastableFromUntyped(elem.kind, t.Kind) {
			// ok, allow untyped literals to be casted to a custom type

		} else if !elem_type.Equal(elem.kind) {
			return fmt.Errorf("array literal has different types: %s and %s",
				elem_type.String(), elem.kind.String())
		}

		// create the array on first element
		if i == 0 {
			arr_type := NewType(VAL_Array)
			arr_type.sub = elem_type

			v.MakeArray(0, arr_type)
			CreateArrayDef(v.Array)
		}

		v.AppendElem(elem)

		if elem.kind.base == VAL_String {
			CreateStringDef(elem.Str)
		}
	}

	return Ok
}

func (v *Value) ParseTuple(children []*Token) error {
	if len(children) == 0 {
		return fmt.Errorf("tuple literal must not be empty")
	}

	values := make([]Value, len(children))

	for i, child := range children {
		err := values[i].ParseLiteral(child)
		if err != nil {
			return err
		}

		if values[i].kind.base == VAL_String {
			CreateStringDef(values[i].Str)
		}
	}

	// create the type information
	tup_type := NewType(VAL_Tuple)

	for i := range children {
		tup_type.AddParam("", values[i].kind)
	}

	v.MakeTuple(tup_type)
	v.Tuple.data = values

	CreateTupleDef(v.Tuple)

	return Ok
}

//----------------------------------------------------------------------

func (v *Value) DeepString() string {
	// this keeps track of arrays/tuples which have been seen,
	// to prevent infinite recursion.
	seen := make(map[*Value]bool)

	return v.rawDeepString(seen)
}

func (v *Value) rawDeepString(seen map[*Value]bool) string {
	if v.kind == nil {
		// return "!!UNSET-VALUE!!"
		panic("rawDeepString: value with kind == nil")
	}

	if v.kind.base == VAL_Array || v.kind.base == VAL_Tuple {
		// already seen?
		_, ok := seen[v]
		if ok {
			return "#CyclicData#"
		}
		// mark it now
		seen[v] = true
	}

	switch v.kind.base {
	case VAL_Int:
		return v.IntToString()

	case VAL_String:
		return v.StringToString()

	case VAL_Array:
		return v.ArrayToString(seen)

	case VAL_Tuple:
		return v.TupleToString(seen)

	case VAL_Function:
		return v.FunctionToString()

	default:
		return "!!INVALID!!"
	}
}

func (v *Value) IntToString() string {
	switch v.Int {
	case NIL:
		return "NIL"
	case TRUE:
		return "TRUE"
	}

	return fmt.Sprintf("%d", v.Int)
}

func (v *Value) StringToString() string {
	if v.Str == "\x1A" {
		return "EOF"
	}

	// we need to surround the string with double quotes, and
	// escape any strange characters (including '"' itself).
	// luckily the %q verb in the fmt package does all this.

	return fmt.Sprintf("%q", v.Str)
}

func (v *Value) ArrayToString(seen map[*Value]bool) string {
	var sb strings.Builder

	sb.WriteString("[")

	for i := range v.Array.data {
		if i > 0 {
			sb.WriteString(" ")
		}

		child := v.GetElem(i)

		sb.WriteString(child.rawDeepString(seen))
	}

	sb.WriteString("]")
	return sb.String()
}

func (v *Value) TupleToString(seen map[*Value]bool) string {
	var sb strings.Builder

	sb.WriteString("{")

	for i := range v.Tuple.data {
		if i > 0 {
			sb.WriteString(" ")
		}

		child := v.Tuple.data[i]

		sb.WriteString(child.rawDeepString(seen))
	}

	sb.WriteString("}")
	return sb.String()
}

func (v *Value) FunctionToString() string {
	return "#Function#"
}

//----------------------------------------------------------------------

func (v *Value) OutputData() {
	switch v.kind.base {
	case VAL_Int:
		v.OutputInt()

	case VAL_String:
		v.OutputString()

	case VAL_Function:
		v.OutputFunction()

	case VAL_Array:
		v.OutputArray()

	case VAL_Tuple:
		v.OutputTuple()

	default:
		OutLine("\tdq -1 ; FIXME!! unsupported value kind")
	}
}

func (v *Value) OutputInt() {
	s := fmt.Sprintf("%d", v.Int)
	OutInst("dq", s, "", "")
}

func (v *Value) OutputString() {
	// this def will have been created earlier, we just re-find it
	def := CreateStringDef(v.Str)

	OutInst("dq", def.label, "", "")
}

func (v *Value) OutputFunction() {
	def := v.Clos._def
	if def == nil {
		panic("OutputFunction but no FuncDef")
	}

	OutInst("dq", def.label, "", "")
}

func (v *Value) OutputArray() {
	// this def will have been created earlier, we just re-find it
	def := CreateArrayDef(v.Array)

	OutInst("dq", def.label, "", "")
}

func (v *Value) OutputTuple() {
	// this def will have been created earlier, we just re-find it
	def := CreateTupleDef(v.Tuple)

	OutInst("dq", def.label, "", "")
}
