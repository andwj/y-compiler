
Yewberry Compiler
=================

by Andrew Apted, 2018.


About
-----

Yewberry is a programming language which I wrote from scratch,
partly to see if I could do it (without looking at anybody else's
code or algorithms), partly to explore how minimal a language can
be and still be useful, and partly to serve as a base for future
experiments.

This repository holds a compiler which builds native Linux
executables for the x86-64 CPU architecture.  It requires FASM
to perform the final conversion from assembly language to a
runnable binary (ELF format).

NOTE: there is also a interpreter for this language, see the
repository here: https://gitlab.com/andwj/yewberry


Legalese
--------

Yewberry is Copyright &copy; 2018 Andrew Apted.

Yewberry is Free Software, under the terms of the GNU General
Public License, version 3 or (at your option) any later version.
See the [LICENSE.txt](LICENSE.txt) file for the complete text.

Yewberry comes with NO WARRANTY of any kind, express or implied.
Please read the license for full details.

