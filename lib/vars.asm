;;
;; Variables and buffers
;;

memblock_p:
	dq	0
memblock_free:
	dq	0

;
; various buffers
;
message_buffer:
	dq	1024 dup 0

number_buffer:
	dq	16 dup 0

debug_buffer:
	dq	128 dup 0

;
; random generator state
;
RNG_x:
	dd	1
RNG_y:
	dd	1
RNG_z:
	dd	1
RNG_w:
	dd	1
RNG_c:
	dd	1,0


;;--- editor settings -----
;; vi:ts=8:sw=8:noexpandtab
