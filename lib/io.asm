;;
;; I/O primitives
;;
;; Copyright 2019 Andrew Apted.
;; This code is under the GNU General Public License, version 3
;; or (at your option) any later version.
;;


__print_impl:
	mov	rsi,[rbp-STACKOFS_PARAM1]
	call	print_string

	mov	eax,NIL
	ret


__error_impl:
	call	show_stacktrace

	; show prefix
	mov	rsi,user_error_msg
	call	print_message

	; show actual message
	call	__print_impl

	; exit the process
	mov	edi,98
	mov	eax,sys_exit
	syscall


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


__enable_editor_impl:
	; nothing to do, we do not support line editing
	mov	eax,NIL
	ret


__input_impl:
	push	r12
	push	r13  ; cur length (in message_buffer)

	call	.prompt

	; begin with empty string
	xor	r13,r13
	mov	[message_buffer],r13b

.loop:
	call	input_char

	cmp	eax,NIL
	je	.readerror

	cmp	eax,10  ; '\n'
	je	.finished

	; skip carriage return and NUL
	or	eax,eax
	je	.loop
	cmp	eax,13  ; '\r'
	je	.loop

	mov	rdi,message_buffer
	add	rdi,r13
	mov	[rdi],al
	inc	r13

	xor	eax,eax
	mov	[rdi+1],al

	jmp	.loop

.finished:
	pop	r13
	pop	r12

	; convert to a yewberry string
	mov	rsi,message_buffer
	call	message_to_string

	mov	rax,rdi
	ret

.readerror:
	pop	r13
	pop	r12

	mov	rax,eof_str
	ret

.prompt:
	mov	r12,[rbp-STACKOFS_PARAM1]
	mov	r13d,[r12]

.pr_loop:
	or	r13,r13
	jz	.pr_done

	add	r12,4
	mov	eax,[r12]
	call	print_unicode_char

	dec	r13
	jmp	.pr_loop

.pr_done:
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;
; rsi = string
;
print_string:
	push	r12	; pointer to char
	push	r13	; length remaining

	mov	r12,rsi
	mov	r13d,[r12]
.loop:
	or	r13,r13
	jz	.finished

	add	r12,4
	mov	eax,[r12]
	call	print_unicode_char

	dec	r13
	jmp	.loop

.finished:
	mov	eax,0xA	; newline
	call	print_char

	pop	r13
	pop	r12
	ret

;
; eax = unicode code point
;
print_unicode_char:
	; encode unicode char into UTF-8
	cmp	eax,127
	jbe	print_char

	cmp	eax,0x7FF
	jbe	.pair

	cmp	eax,0xFFFF
	jbe	.triple

	cmp	eax,0x10FFFF
	jbe	.quad

	; just show a question mark
	mov	eax,63
	jmp	print_char

.pair:
	mov	ecx,eax
	shr	eax,6
	or	eax,0xC0
	call	print_char

	mov	eax,ecx
	and	eax,63
	or	eax,0x80
	jmp	print_char

.triple:
	mov	ecx,eax
	shr	eax,12
	or	eax,0xE0
	call	print_char

	mov	eax,ecx
	shr	eax,6
	and	eax,63
	or	eax,0x80
	call	print_char

	mov	eax,ecx
	and	eax,63
	or	eax,0x80
	jmp	print_char

.quad:
	mov	ecx,eax
	shr	eax,18
	or	eax,0xF0
	call	print_char

	mov	eax,ecx
	shr	eax,12
	and	eax,63
	or	eax,0x80
	call	print_char

	mov	eax,ecx
	shr	eax,6
	and	eax,63
	or	eax,0x80
	call	print_char

	mov	eax,ecx
	and	eax,63
	or	eax,0x80
	jmp	print_char

;
; eax = byte to write to STDOUT
;
print_char:
	push	rcx

	; use the stack as our buffer
	push	rax

	mov	edi,STDOUT	; fd
	mov	rsi,rsp		; buf
	mov	edx,1		; count

	mov	eax,sys_write
	syscall

	pop	rax
	pop	rcx
	ret

;
; rsi = message (as asciistr)
;
print_message:
	call	message_len

	mov	edx,eax		; length
	mov	edi,STDOUT	; fd
	mov	eax,sys_write
	syscall

	ret

;
; rsi = message (as asciistr)
; --> rax = length (ignored trailing NUL byte)
;     rsi is saved.
;
message_len:
	push	rsi
	xor	rax,rax
.loop:
	mov	cl,[rsi]
	or	cl,cl
	jz	.finished

	inc	rax
	inc	rsi
	jmp	.loop

.finished:
	pop	rsi
	ret

;
; rsi = source message (as asciistr)
; rdi = destination buffer
;
append_message:
	mov	cl,[rsi]
	or	cl,cl
	jz	.done

	mov	[rdi],cl

	inc	rsi
	inc	rdi
	jmp	append_message
.done:
	ret

;
; rdi = message buffer
;
finish_message:
	mov	byte [rdi],0xA
	inc	rdi
	mov	byte [rdi],0
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;
; (no inputs)
; --> eax = character read from STDIN
;
input_char:
	; use the stack as our buffer
	xor	rax,rax
	push	rax

	mov	edi,STDIN	; fd
	mov	rsi,rsp		; buf
	mov	edx,1		; count

	mov	eax,sys_read
	syscall

	or	rax,rax
	jz	.fail
	js	.fail

	pop	rax
	ret

.fail:
	pop	rax
	mov	eax,NIL
	ret


;;--- editor settings -----
;; vi:ts=8:sw=8:noexpandtab
