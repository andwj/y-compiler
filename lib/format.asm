;;
;; format values to strings
;;
;; Copyright 2019 Andrew Apted.
;; This code is under the GNU General Public License, version 3
;; or (at your option) any later version.
;;

; type descriptor constants
TD_Int		= 0
TD_String	= 1
TD_Func		= 2
TD_Array	= 3
TD_Tuple	= 4


;
; rax = the value to format
; rsi = pointer to type descriptor
; --> rdi is formatted string
;
format_string:
	mov	cx,[rsi]
	cmp	cx,TD_Int
	je	fmt_from_integer

	cmp	cx,TD_String
	je	fmt_quoted_string

	cmp	cx,TD_Func
	je	fmt_from_func

	cmp	cx,TD_Array
	je	fmt_from_array

	cmp	cx,TD_Tuple
	je	fmt_from_tuple

	; should not get here!
	mov	rdi,question_str
	ret


fmt_from_integer:
	; handle two special cases
	cmp	eax,TRUE
	je	.do_TRUE

	cmp	eax,NIL
	je	.do_NIL

	mov	rdi,number_buffer
	call	conv_int32

	; NUL-terminate buffer
	xor	ecx,ecx
	mov	[rdi],cl

	mov	rsi,number_buffer
	call	message_to_string
	ret

.do_TRUE:
	mov	rdi,TRUE_str
	ret

.do_NIL:
	mov	rdi,NIL_str
	ret


fmt_from_func:
	mov	rsi,func_begin_str
	mov	rdi,[rax+FUNCDEF_NAME]
	call	append_string

	mov	rsi,rdi
	mov	rdi,func_end_str
	call	append_string
	ret


fmt_from_array:
	push	r12  ; array / elems
	push	r13  ; current string

	mov	r12,rax
	mov	r13,array_begin_str

	add	rsi,2

	mov	ecx,[r12]   ; length
	mov	r12,[r12+8]

	or	ecx,ecx
	jz	.done

.loop:
	mov	rax,[r12]
	add	r12,8

	push	rcx
	push	rsi
	call	format_string

	mov	rsi,r13
	call	append_string
	mov	r13,rdi

	pop	rsi
	pop	rcx

	dec	ecx
	jz	.done

	; add a space
	push	rcx
	push	rsi

	mov	rsi,r13
	mov	rdi,space_str
	call	append_string
	mov	r13,rdi

	pop	rsi
	pop	rcx

	jmp	.loop

.done:
	mov	rsi,r13
	mov	rdi,array_end_str
	call	append_string

	pop	r13
	pop	r12
	ret


fmt_from_tuple:
	push	r12  ; element ptr
	push	r13  ; current string

	mov	r12,rax
	mov	r13,tuple_begin_str

	xor	rcx,rcx  ; elem index
.loop:
	; get next offset for sub-desc
	mov	rdi,rsi
	mov	rax,rcx
	inc	rax
	shl	rax,1
	add	rdi,rax

	xor	rax,rax
	mov	ax,[rdi]

	; if negative, we are done
	or	ax,ax
	js	.done

	; need a space?
	or	ecx,ecx
	jz	.nospace

	; add a space
	push	rax
	push	rcx
	push	rsi

	mov	rsi,r13
	mov	rdi,space_str
	call	append_string
	mov	r13,rdi

	pop	rsi
	pop	rcx
	pop	rax

.nospace:
	; add offset to base of type-desc
	shl	rax,1
	add	rax,rsi

	; format element and append it
	push	rcx
	push	rsi

	mov	rsi,rax
	mov	rax,[r12]
	add	r12,8
	call	format_string

	mov	rsi,r13
	call	append_string
	mov	r13,rdi

	pop	rsi
	pop	rcx

	inc	rcx
	jmp	.loop

.done:
	mov	rsi,r13
	mov	rdi,tuple_end_str
	call	append_string

	pop	r13
	pop	r12
	ret


fmt_quoted_string:
	; WISH: this is very inefficient, it reallocates the
	;       new string for every character.  Optimise it.

	push	r12  ; source str
	push	r13  ; building string
	push	r14  ; length remaining

	mov	r12,rax
	mov	r13,dblquote_str

	mov	r14d,[r12]
	add	r12,4

	or	r14d,r14d
	jz	.finished

.loop:
	mov	eax,[r12]
	add	r12,4

	call	.add_char

	dec	r14d
	jnz	.loop

.finished:
	mov	rsi,r13
	mov	rdi,dblquote_str
	call	append_string

	pop	r14
	pop	r13
	pop	r12
	ret

.add_char:
	cmp	eax,34  ; '"'
	je	.escape1
	cmp	eax,92  ; '\'
	je	.escape1

	cmp	eax,7   ; '\a'
	je	.bell
	cmp	eax,8   ; '\b'
	je	.backspace
	cmp	eax,9   ; '\t'
	je	.tab
	cmp	eax,10  ; '\n'
	je	.newline
	cmp	eax,11  ; '\v'
	je	.verttab
	cmp	eax,12  ; '\f'
	je	.formfeed
	cmp	eax,13  ; '\r'
	je	.carriage

	cmp	eax,32
	jb	.hex_escape
	cmp	eax,127
	jb	.ok_char
	cmp	eax,160
	jb	.hex_escape

	cmp	eax,0x10FFFF
	ja	.bad_char
	jmp	.ok_char

.bell:
	mov	eax,97  ; 'a'
	jmp	.escape1
.backspace:
	mov	eax,98  ; 'b'
	jmp	.escape1
.tab:
	mov	eax,116  ; 't'
	jmp	.escape1
.verttab:
	mov	eax,118  ; 'v'
	jmp	.escape1
.formfeed:
	mov	eax,102  ; 'f'
	jmp	.escape1
.carriage:
	mov	eax,114  ; 'r'
	jmp	.escape1
.newline:
	mov	eax,110  ; 'n'

.escape1:
	push	rax

	mov	eax,92  ; '\'
	mov	rsi,r13
	call	append_char
	mov	r13,rdi

	pop	rax
.ok_char:
	mov	rsi,r13
	call	append_char
	mov	r13,rdi
	ret

.bad_char:
	mov	rsi,r13
	mov	rdi,question_str
	call	append_string
	mov	r13,rdi
	ret

.hex_escape:
	; add \x prefix
	push	rax
	mov	rsi,r13
	mov	rdi,hex_escape_str
	call	append_string
	mov	r13,rdi
	pop	rax

	; use stack as temp buffer
	xor	rcx,rcx
	push	rcx

	mov	rdi,rsp
	call	conv_hex2

	xor	rax,rax
	mov	al,[rsp]
	mov	rsi,r13
	call	append_char
	mov	r13,rdi

	xor	rax,rax
	mov	al,[rsp+1]
	mov	rsi,r13
	call	append_char
	mov	r13,rdi

	pop	rcx
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;
; eax = number
; rdi = pointer to buffer
;
; NOTE: does not NUL-terminate the result
; WISH: use a better algorithm.
;
conv_int32:
	or	eax,eax
	jns	.positive

	neg	eax

	mov	cl,45  ; '-'
	mov	[rdi],cl
	inc	rdi

.positive:
	xor	edx,edx  ; got digit flag

	mov	ecx,1000000000
	call	.conv_digit
	mov	ecx,100000000
	call	.conv_digit
	mov	ecx,10000000
	call	.conv_digit
	mov	ecx,1000000
	call	.conv_digit
	mov	ecx,100000
	call	.conv_digit
	mov	ecx,10000
	call	.conv_digit
	mov	ecx,1000
	call	.conv_digit
	mov	ecx,100
	call	.conv_digit
	mov	ecx,10
	call	.conv_digit
	mov	ecx,1
	or	edx,0x100

.conv_digit:
	and	edx,0x100

.dig_loop:
	cmp	eax,ecx
	jb	.dig_end

	sub	eax,ecx
	inc	edx
	jmp	.dig_loop

.dig_end:
	or	edx,edx
	jz	.done

	add	dl,48  ; '0'
	mov	[rdi],dl
	inc	rdi

	; set "got digit" flag
	or	edx,0x100
.done:
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;
; rax = number
; rdi = pointer to buffer
; NOTE: does not NUL-terminate the result
;
conv_hex16:
	push	rax
	shr	rax,32
	call	conv_hex8

	pop	rax

conv_hex8:
	mov	edx,eax
	shr	eax,16
	call	conv_hex4

	mov	eax,edx

conv_hex4:
	mov	ecx,eax
	shr	eax,12
	call	conv_hex1

	mov	eax,ecx
	shr	eax,8
	call	conv_hex1

	mov	eax,ecx

conv_hex2:
	mov	ecx,eax
	shr	eax,4
	call	conv_hex1

	mov	eax,ecx

conv_hex1:
	and	eax,15
	add	eax,48	; '0'
	cmp	eax,58
	jb	.store

	add	eax,65-58  ; 'A'
.store:
	mov	[rdi],al
	inc	rdi
	ret


;;--- editor settings -----
;; vi:ts=8:sw=8:noexpandtab
