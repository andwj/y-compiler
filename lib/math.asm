;;
;; math primitives
;;
;; Copyright 2019 Andrew Apted.
;; This code is under the GNU General Public License, version 3
;; or (at your option) any later version.
;;


__int_add_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	mov	ecx,[rbp-STACKOFS_PARAM2]

	add	eax,ecx
	ret


__int_sub_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	mov	ecx,[rbp-STACKOFS_PARAM2]

	sub	eax,ecx
	ret


__int_mul_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	mov	ecx,[rbp-STACKOFS_PARAM2]

	imul	eax,ecx
	ret


__int_div_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	mov	ecx,[rbp-STACKOFS_PARAM2]

	or	ecx,ecx
	jz	division_by_zero

	; calc edx to be signed extension of eax
	movsxd	rdx,eax
	sar	rdx,32

	idiv	ecx
	ret


__int_mod_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	mov	ecx,[rbp-STACKOFS_PARAM2]

	or	ecx,ecx
	jz	division_by_zero

	movsxd	rdx,eax
	sar	rdx,32

	idiv	ecx
	mov	eax,edx
	ret


division_by_zero:
	mov	rsi,div_by_zero_msg
	jmp	abort


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


__int_pow_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	mov	ecx,[rbp-STACKOFS_PARAM2]

	; handle some special cases
	or	ecx,ecx
	js	.negative_exp
	jz	.result_pos1

	cmp	ecx,1
	je	.nochange

	or	eax,eax
	jz	.nochange
	cmp	eax,1
	je	.nochange
	cmp	eax,NIL
	je	.nochange

	cmp	eax,-1
	je	.input_neg1

	; here we have abs(value) >= 2 and exponent >= 2.
	; hence any exponent >= 32 will overflow a signed 32-bit integer.

	cmp	ecx,32
	jae	.overflow

	; sign extend the value to 64 bits
	cdqe
	mov	rsi,rax
	dec	ecx

.loop:
	imul	rsi

	; detect overflow
	cmp	rax,-0x7FFFFFFF
	jl	.overflow

	cmp	rax,0x7FFFFFFF
	jg	.overflow

	dec	ecx
	jnz	.loop

.nochange:
	ret

.input_neg1:
	and	ecx,1
	jnz	.result_neg1

.result_pos1:
	mov	eax,1
	ret

.result_neg1:
	mov	eax,-1
	ret

.overflow:
	mov	eax,NIL
	ret

.negative_exp:
	mov	rsi,neg_exponent_msg
	jmp	abort


__abs_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]

	or	eax,eax
	jns	.done

	neg	eax
.done:
	ret


__min_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	mov	ecx,[rbp-STACKOFS_PARAM2]

	cmp	eax,ecx
	jle	.done

	mov	eax,ecx
.done:
	ret


__max_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	mov	ecx,[rbp-STACKOFS_PARAM2]

	cmp	eax,ecx
	jge	.done

	mov	eax,ecx
.done:
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


__and_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	cmp	eax,NIL
	je	.and_NIL

	mov	eax,[rbp-STACKOFS_PARAM2]
	cmp	eax,NIL
	je	.and_NIL

	mov	eax,TRUE
	ret

.and_NIL:
	mov	eax,NIL
	ret


__or_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	cmp	eax,NIL
	jne	.or_TRUE

	mov	eax,[rbp-STACKOFS_PARAM2]
	cmp	eax,NIL
	jne	.or_TRUE

	mov	eax,NIL
	ret

.or_TRUE:
	mov	eax,TRUE
	ret


__not_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	cmp	eax,NIL
	je	.not_TRUE

	mov	eax,NIL
	ret

.not_TRUE:
	mov	eax,TRUE
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


__bit_and_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	mov	ecx,[rbp-STACKOFS_PARAM2]

	and	eax,ecx
	ret


__bit_or_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	mov	ecx,[rbp-STACKOFS_PARAM2]

	or	eax,ecx
	ret


__bit_xor_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	mov	ecx,[rbp-STACKOFS_PARAM2]

	xor	eax,ecx
	ret


__bit_not_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	not	eax
	ret


; the shift operations are tricky since we need to handle:
;    1. negative shift values --> go opposite way
;    2. large shift values --> produce zero or -1

__shift_left_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	mov	ecx,[rbp-STACKOFS_PARAM2]

	or	ecx,ecx
	jz	shift_nothing
	jns	shift_left

shift_right_neg:
	neg	ecx

shift_right:
	cmp	ecx,32
	jae	shift_POSNEG

	sar	eax,cl
	ret

shift_POSNEG:
	or	eax,eax
	jns	shift_ZERO

	xor	eax,eax
	dec	eax

shift_nothing:
	ret


__shift_right_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	mov	ecx,[rbp-STACKOFS_PARAM2]

	or	ecx,ecx
	jz	shift_nothing
	jns	shift_right

shift_left_neg:
	neg	ecx

shift_left:
	cmp	ecx,32
	jae	shift_ZERO

	sal	eax,cl
	ret

shift_ZERO:
	xor	eax,eax
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


__int_eq%3F_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	mov	ecx,[rbp-STACKOFS_PARAM2]

	xor	eax,ecx

	mov	eax,NIL
	mov	ecx,TRUE

	cmovz	eax,ecx
	ret


__int_ne%3F_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	mov	ecx,[rbp-STACKOFS_PARAM2]

	xor	eax,ecx

	mov	eax,NIL
	mov	ecx,TRUE

	cmovnz	eax,ecx
	ret


__int_lt%3F_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	mov	ecx,[rbp-STACKOFS_PARAM2]

	cmp	eax,ecx

	mov	eax,NIL
	mov	ecx,TRUE

	cmovl	eax,ecx
	ret


__int_le%3F_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	mov	ecx,[rbp-STACKOFS_PARAM2]

	cmp	eax,ecx

	mov	eax,NIL
	mov	ecx,TRUE

	cmovle	eax,ecx
	ret


__int_gt%3F_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	mov	ecx,[rbp-STACKOFS_PARAM2]

	cmp	eax,ecx

	mov	eax,NIL
	mov	ecx,TRUE

	cmovg	eax,ecx
	ret


__int_ge%3F_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	mov	ecx,[rbp-STACKOFS_PARAM2]

	cmp	eax,ecx

	mov	eax,NIL
	mov	ecx,TRUE

	cmovge	eax,ecx
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


__parse_int_impl:
	mov	rsi,[rbp-STACKOFS_PARAM1]
	mov	ecx,[rsi]

	or	ecx,ecx
	jz	.fail

	; skip whitespace
.loop1:
	add	rsi,4

	mov	eax,[rsi]
	cmp	eax,160
	je	.space

	; hack: treat everything from 0-32 as space
	cmp	eax,32
	ja	.solid

.space:
	dec	ecx
	jnz	.loop1

.fail:
	mov	eax,NIL
	ret

.solid:
	; does it match "NIL"?
	cmp	rcx,3
	jb	.not_NIL

	mov	eax,[rsi]
	xor	eax,78	; 'N'
	jne	.not_NIL

	mov	eax,[rsi+4]
	xor	eax,73	; 'I'
	jne	.not_NIL

	mov	eax,[rsi+8]
	xor	eax,76	; 'L'
	jne	.not_NIL

	mov	eax,NIL
	ret

.not_NIL:
	; does it match "TRUE"?
	cmp	rcx,4
	jb	.not_TRUE

	mov	eax,[rsi]
	xor	eax,84	; 'T'
	jne	.not_TRUE

	mov	eax,[rsi+4]
	xor	eax,82	; 'R'
	jne	.not_TRUE

	mov	eax,[rsi+8]
	xor	eax,85	; 'U'
	jne	.not_TRUE

	mov	eax,[rsi+12]
	xor	eax,69	; 'E'
	jne	.not_TRUE

	mov	eax,TRUE
	ret

.not_TRUE:
	; check for character literal
	mov	eax,[rsi]
	cmp	eax,39	; quote
	je	parse_quoted_char

	; check for negative number
	xor	edx,edx

	cmp	eax,45	; '-'
	jne	.not_minus
	cmp	ecx,2
	jb	.fail

	or	edx,1
	add	rsi,4
	dec	ecx

.not_minus:
	; check for hexadecimal and octal
	mov	eax,[rsi]
	cmp	eax,48	; '0'
	jne	.parse_number
	cmp	ecx,3
	jb	.parse_number

	mov	eax,[rsi+4]
	cmp	eax,120	; 'x'
	je	.hexadecimal
	cmp	eax,88	; 'X'
	je	.hexadecimal

.octal:
	or	edx,2
	jmp	.parse_number

.hexadecimal:
	or	edx,4
	add	rsi,8	; skip '0x'
	sub	ecx,2

.parse_number:
	xor	rdi,rdi

.loop2:
	mov	eax,[rsi]
	add	rsi,4

	call	parse_a_digit
	jc	.fail

	dec	ecx
	jnz	.loop2

	; apply minus sign
	test	edx,1
	jz	.positive
	neg	rdi

.positive:
	mov	eax,edi
	ret


;
; eax = character
; edx = flags (2 for octal, 4 for hex)
; rdi = current value
; --> carry set on failure
;
parse_a_digit:
	; convert character to a value 0..35
	sub	eax,48	; '0'
	jb	.bad_char

	cmp	eax,10
	jb	.process

	sub	eax,7	; 'A'
	jb	.bad_char

	cmp	eax,36
	jb	.process

	sub	eax,32	; 'a'
	jb	.bad_char

	cmp	eax,36
	jb	.process

.bad_char:
.overflow:
	stc
	ret

.process:
	; handle decimal, octal or hexadecimal
	test	edx,2
	jnz	.octal
	test	edx,4
	jnz	.hexadecimal

.decimal:
	cmp	eax,10
	jae	.bad_char

	imul	rdi,10

.add_it:
	add	rdi,rax

	; detect overflow
	cmp	rdi,-0x7FFFFFFF
	jl	.overflow

	; we allow values between 0x80000000 and 0xFFFFFFFF
	mov	rax,rdi
	shr	rax,1
	cmp	rax,0x7FFFFFFF
	jg	.overflow

	clc
	ret

.octal:
	cmp	eax,8
	jae	.bad_char

	shl	rdi,3
	jmp	.add_it

.hexadecimal:
	cmp	eax,16
	jae	.bad_char

	shl	rdi,4
	jmp	.add_it


parse_quoted_char:
	cmp	ecx,3
	jb	.fail

	mov	eax,[rsi+4]
	mov	edx,[rsi+8]

	cmp	eax,39	; quote
	je	.fail

	cmp	eax,92	; '\'
	je	.escape

	cmp	edx,39	; quote
	jne	.fail

	ret

.escape:
	; WISH: hex escapes:     \xNN
	; WISH: unicode escapes: \uNNNN
	; WISH: octal escapes:   \377

	cmp	ecx,4
	jb	.fail

	mov	eax,edx
	mov	edx,[rsi+12]
	cmp	edx,39	; quote
	jne	.fail

	cmp	eax,34	; '\"'
	je	.ok
	cmp	eax,39	; '\''
	je	.ok
	cmp	eax,92	; '\\'
	je	.ok

	cmp	eax,97	; '\a'
	je	.bell
	cmp	eax,98	; '\b'
	je	.backspace
	cmp	eax,102	; '\f'
	je	.formfeed
	cmp	eax,110	; '\n'
	je	.newline
	cmp	eax,114	; '\r'
	je	.carriage
	cmp	eax,116	; '\t'
	je	.tab
	cmp	eax,118	; '\v'
	je	.verttab

.fail:
	mov	eax,NIL
.ok:
	ret

.bell:
	mov	eax,7
	ret
.backspace:
	mov	eax,8
	ret
.tab:
	mov	eax,9
	ret
.newline:
	mov	eax,10
	ret
.verttab:
	mov	eax,11
	ret
.formfeed:
	mov	eax,12
	ret
.carriage:
	mov	eax,13
	ret


;;--- editor settings -----
;; vi:ts=8:sw=8:noexpandtab
