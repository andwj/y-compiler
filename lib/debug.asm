;;
;; debugging aids
;;
;; Copyright 2019 Andrew Apted.
;; This code is under the GNU General Public License, version 3
;; or (at your option) any later version.
;;

;
; rsi = message (asciistr)
; --> all registers are preserved
;
debug_print:
	push	rax
	push	rbx
	push	rcx
	push	rdx

	push	rsi
	push	rdi

	push	r8
	push	r9
	push	r10
	push	r11

	call	print_message

	pop	r11
	pop	r10
	pop	r9
	pop	r8

	pop	rdi
	pop	rsi

	pop	rdx
	pop	rcx
	pop	rbx
	pop	rax
	ret

;
; rax = 64-bit value
; --> all registers are preserved
;
debug_hex16:
	push	rax
	push	rbx
	push	rcx
	push	rdx

	push	rsi
	push	rdi

	push	r8
	push	r9
	push	r10
	push	r11

	mov	rdi,debug_buffer
	call	conv_hex16
	call	finish_message

	mov	rsi,debug_buffer
	call	print_message

	pop	r11
	pop	r10
	pop	r9
	pop	r8

	pop	rdi
	pop	rsi

	pop	rdx
	pop	rcx
	pop	rbx
	pop	rax
	ret


;
; eax = 32-bit signed value
; --> all registers are preserved
;
debug_int32:
	push	rax
	push	rbx
	push	rcx
	push	rdx

	push	rsi
	push	rdi

	push	r8
	push	r9
	push	r10
	push	r11

	mov	rdi,debug_buffer
	call	conv_int32
	call	finish_message

	mov	rsi,debug_buffer
	call	print_message

	pop	r11
	pop	r10
	pop	r9
	pop	r8

	pop	rdi
	pop	rsi

	pop	rdx
	pop	rcx
	pop	rbx
	pop	rax
	ret


;;--- editor settings -----
;; vi:ts=8:sw=8:noexpandtab
