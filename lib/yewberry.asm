;;
;; Yewberry basic defs
;;
;; Copyright 2019 Andrew Apted.
;; This code is under the GNU General Public License, version 3
;; or (at your option) any later version.
;;

NIL	= 0x80000000
TRUE	= 0x7FFFFFFF

;
; artificial limits
;
ARRAY_LIMIT	= 0x10000000
STRING_LIMIT	= 0x10000000

;
; stack frame stuff
;
STACKOFS_INFO	= 1*8	; pointer to FuncDef
STACKOFS_CODE	= 2*8	; pointer to code
STACKOFS_UPVAL	= 3*8	; pointer to upvalue list

STACKOFS_PARAM1	= 4*8	; first parameter
STACKOFS_PARAM2	= 5*8	; second parameter
STACKOFS_PARAM3	= 6*8	; third parameter
STACKOFS_PARAM4	= 7*8	; fourth parameter
STACKOFS_PARAM5	= 8*8	; fifth parameter

;
; func-def fields
;
FUNCDEF_CODE	= 0	; code pointer
FUNCDEF_NAME	= 1*8	; pointer to yewberry string
FUNCDEF_UPVALS	= 2*8	; pointer to N ptrs to UPVALUE

;
; upvalue record
;
UPVALUE_OFFSET	= 0	; stack_ofs of var, or -1 if hoisted
UPVALUE_STORAGE	= 1*8	; storage for a hoisted var
UPVALUE_NEXT	= 2*8	; link for CallFrame list


;;--- editor settings -----
;; vi:ts=8:sw=8:noexpandtab
