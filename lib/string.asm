;;
;; string primitives
;;
;; Copyright 2019 Andrew Apted.
;; This code is under the GNU General Public License, version 3
;; or (at your option) any later version.
;;


__str_add_impl:
	mov	rsi,[rbp-STACKOFS_PARAM1]
	mov	rdi,[rbp-STACKOFS_PARAM2]

	call	append_string

	mov	rax,rdi
	ret


__str_len_impl:
	mov	rcx,[rbp-STACKOFS_PARAM1]
	mov	eax,[rcx]
	ret


__str_empty%3F_impl:
	mov	rcx,[rbp-STACKOFS_PARAM1]
	mov	eax,[rcx]
	or	eax,eax

	mov	eax,NIL
	mov	ecx,TRUE
	cmovz	eax,ecx
	ret


__str_subseq_impl:
	mov	rsi,[rbp-STACKOFS_PARAM1]  ; string
	mov	ecx,[rbp-STACKOFS_PARAM2]  ; start
	mov	edx,[rbp-STACKOFS_PARAM3]  ; end

	; clamp start to 0
	xor	eax,eax
	or	ecx,ecx
	cmovs	ecx,eax

	; clamp end to length of string
	mov	eax,[rsi]
	cmp	edx,eax
	cmovg	edx,eax

	; calc new length, will it be empty?
	; [ this handles all weird cases, e.g. start >= length ]
	mov	eax,edx
	sub	eax,ecx
	jle	.empty

	; allocate new string
	push	rcx

	call	make_string

	pop	rax
	push	rdi	; save result ptr

	; offset the source string
	inc	rax
	shl	rax,2
	mov	rsi,[rbp-STACKOFS_PARAM1]
	add	rsi,rax

	add	rdi,4

	or	ecx,ecx
	jz	.done
.loop:
	mov	eax,[rsi]
	add	rsi,4
	mov	[rdi],eax
	add	rdi,4

	dec	ecx
	jnz	.loop

.done:
	pop	rax	; restore result ptr
	ret

.empty:
	mov	rax,empty_str
	ret

;
; rsi = string ptr
; eax = index
; --> eax = character
;
; NOTE: if index is out-of-bounds, an error is raised.
;
index_string:
	or	eax,eax
	js	.outofbounds

	mov	ecx,[rsi]
	cmp	eax,ecx
	jge	.outofbounds

	mov	eax,eax
	shl	rax,2
	add	rsi,rax
	add	rsi,4

	mov	eax,[rsi]
	ret

.outofbounds:
	mov	rsi,out_of_bounds_msg
	jmp	abort


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


__str_eq%3F_impl:
	mov	rsi,[rbp-STACKOFS_PARAM1]
	mov	rdi,[rbp-STACKOFS_PARAM2]

	call	compare_strings
	or	eax,eax

	mov	eax,NIL
	mov	ecx,TRUE
	cmovz	eax,ecx
	ret


__str_ne%3F_impl:
	mov	rsi,[rbp-STACKOFS_PARAM1]
	mov	rdi,[rbp-STACKOFS_PARAM2]

	call	compare_strings
	or	eax,eax

	mov	eax,NIL
	mov	ecx,TRUE
	cmovnz	eax,ecx
	ret


__str_lt%3F_impl:
	mov	rsi,[rbp-STACKOFS_PARAM1]
	mov	rdi,[rbp-STACKOFS_PARAM2]

	call	compare_strings
	or	eax,eax

	mov	eax,NIL
	mov	ecx,TRUE
	cmovs	eax,ecx
	ret


__str_le%3F_impl:
	mov	rsi,[rbp-STACKOFS_PARAM1]
	mov	rdi,[rbp-STACKOFS_PARAM2]

	call	compare_strings
	xor	eax,1

	mov	eax,NIL
	mov	ecx,TRUE
	cmovnz	eax,ecx
	ret


__str_gt%3F_impl:
	mov	rsi,[rbp-STACKOFS_PARAM1]
	mov	rdi,[rbp-STACKOFS_PARAM2]

	call	compare_strings
	xor	eax,1

	mov	eax,NIL
	mov	ecx,TRUE
	cmovz	eax,ecx
	ret


__str_ge%3F_impl:
	mov	rsi,[rbp-STACKOFS_PARAM1]
	mov	rdi,[rbp-STACKOFS_PARAM2]

	call	compare_strings
	or	eax,eax

	mov	eax,NIL
	mov	ecx,TRUE
	cmovns	eax,ecx
	ret


__str_min_impl:
	mov	rsi,[rbp-STACKOFS_PARAM1]
	mov	rdi,[rbp-STACKOFS_PARAM2]

	call	compare_strings
	or	eax,eax
	jns	.second

	mov	rax,[rbp-STACKOFS_PARAM1]
	ret
.second:
	mov	rax,[rbp-STACKOFS_PARAM2]
	ret


__str_max_impl:
	mov	rsi,[rbp-STACKOFS_PARAM1]
	mov	rdi,[rbp-STACKOFS_PARAM2]

	call	compare_strings
	or	eax,eax
	js	.second

	mov	rax,[rbp-STACKOFS_PARAM1]
	ret
.second:
	mov	rax,[rbp-STACKOFS_PARAM2]
	ret


;
; rsi = string A
; rdi = string B
; --> eax = -1/0/+1 (similar to strcmp in C)
;
compare_strings:
	; grab lengths
	mov	ecx,[rsi]
	mov	edx,[rdi]
.loop:
	add	rsi,4
	add	rdi,4

	; reached end of both strings?
	mov	eax,ecx
	or	eax,edx
	jz	.equal

	; reached end of one of them?
	or	ecx,ecx
	jz	.less
	or	edx,edx
	jz	.greater

	; compare characters
	mov	eax,[rsi]
	cmp	eax,[rdi]

	jb	.less
	ja	.greater

	dec	ecx
	dec	edx
	jmp	.loop

.equal:
	xor	eax,eax
	ret
.less:
	xor	eax,eax
	dec	eax
	ret
.greater:
	xor	eax,eax
	inc	eax
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


__char_letter%3F_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	mov	rsi,unicode_letters
	jmp	is_unicode_class


__char_digit%3F_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	mov	rsi,unicode_digits
	jmp	is_unicode_class


__char_symbol%3F_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	mov	rsi,unicode_symbols
	jmp	is_unicode_class


__char_mark%3F_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	mov	rsi,unicode_marks
	jmp	is_unicode_class


__char_control%3F_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	mov	rsi,unicode_control
	jmp	is_unicode_class


__char_space%3F_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	mov	rsi,unicode_whitespace
	jmp	is_unicode_class


;
; eax = char
; rsi = table  (e.g. unicode_letter)
; --> eax = TRUE or NIL
;
; WISH: optimise with a binary search.
;
is_unicode_class:
	mov	ecx,[rsi+4]
	or	ecx,ecx
	jz	.false

	cmp	eax,[rsi]
	jb	.false

	cmp	eax,ecx
	jbe	.true

	add	rsi,8
	jmp	is_unicode_class

.false:
	mov	eax,NIL
	ret
.true:
	mov	eax,TRUE
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


__char_lower_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	mov	rsi,unicode_lowercasing
	jmp	change_char_case


__char_upper_impl:
	mov	eax,[rbp-STACKOFS_PARAM1]
	mov	rsi,unicode_uppercasing
	jmp	change_char_case


;
; eax = char
; esi = table  (e.g. unicode_lowercasing)
; --> eax = possibly changed char
;
; NOTE: could be optimised with a binary search, though for
;       ASCII and Latin1 charsets it would probably be slower.
;
change_char_case:
	mov	ecx,[rsi]
	or	ecx,ecx
	jz	.done

	; in the range?
	cmp	eax,[rsi+4]
	jb	.done

	cmp	eax,[rsi+8]
	ja	.loop

	; yes, but handle pairs
	cmp	cl,1
	je	.change

	; pairs require same odd-ness as start of range
	mov	ecx,[rsi+4]
	xor	ecx,eax
	and	ecx,1
	jz	.change

.loop:
	add	rsi,16
	jmp	change_char_case

.change:
	add	eax,[rsi+12]

.done:
	ret


__str_lower_impl:
	mov	rdi,[rbp-STACKOFS_PARAM1]
	mov	rsi,unicode_lowercasing

	call	change_string_case

	mov	rax,rdi
	ret


__str_upper_impl:
	mov	rdi,[rbp-STACKOFS_PARAM1]
	mov	rsi,unicode_uppercasing

	call	change_string_case

	mov	rax,rdi
	ret


;
; edi = string
; esi = table
; --> rdi = new string
;
change_string_case:
	push	r12	; dest char
	push	r13	; src char
	push	r14	; table

	mov	r13,rdi
	mov	r14,rsi

	; allocate new string
	mov	eax,[r13]
	call	make_string

	mov	r12,rdi
	push	r12	; save result ptr

	mov	[r12],ecx
	or	ecx,ecx
	jz	.done
.loop:
	add	r12,4
	add	r13,4

	mov	eax,[r13]
	mov	rsi,r14  ; table

	push	rcx
	call	change_char_case
	pop	rcx

	mov	[r12],eax
	dec	ecx
	jnz	.loop
.done:
	pop	rdi	; restore result ptr

	pop	r14
	pop	r13
	pop	r12
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


__char%24_impl:
	; allocate string
	mov	rax,1
	call	make_string

	mov	ecx,[rbp-STACKOFS_PARAM1]

	; NIL and negative values become the empty string
	or	ecx,ecx
	js	.empty

	mov	[rdi+4],ecx
	mov	rax,rdi
	ret

.empty:
	; force length to zero
	xor	ecx,ecx
	mov	[rdi],ecx

	mov	rax,rdi
	ret


__hex%24_impl:
	; build result in num buffer (as asciistr)
	mov	rdi,number_buffer

	mov	al,48   ; '0'
	mov	[rdi],al
	inc	rdi
	mov	al,120  ; 'x'
	mov	[rdi],al
	inc	rdi

	mov	rax,[rbp-STACKOFS_PARAM1]
	call	conv_hex8

	xor	eax,eax
	mov	[rdi],al

	; convert to a yewberry string
	mov	rsi,number_buffer
	call	message_to_string

	mov	rax,rdi
	ret


__quote%24_impl:
	mov	rax,[rbp-STACKOFS_PARAM1]
	call	fmt_quoted_string

	mov	rax,rdi
	ret


;;--- editor settings -----
;; vi:ts=8:sw=8:noexpandtab
