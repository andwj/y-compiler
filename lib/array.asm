;;
;; array primitives
;;
;; Copyright 2019 Andrew Apted.
;; This code is under the GNU General Public License, version 3
;; or (at your option) any later version.
;;

__len_impl:
	mov	rcx,[rbp-STACKOFS_PARAM1]
	mov	eax,[rcx]
	ret


__empty%3F_impl:
	mov	rcx,[rbp-STACKOFS_PARAM1]
	mov	eax,[rcx]
	or	eax,eax

	mov	eax,NIL
	mov	ecx,TRUE
	cmovz	eax,ecx
	ret


__same_obj%3F_impl:
	mov	rax,[rbp-STACKOFS_PARAM1]
	mov	rcx,[rbp-STACKOFS_PARAM2]

	xor	rax,rcx

	mov	eax,NIL
	mov	ecx,TRUE

	cmovz	eax,ecx
	ret


__copy_impl:
	mov	rsi,[rbp-STACKOFS_PARAM1]
	xor	rcx,rcx    ; start
	mov	edx,[rsi]  ; end

	call	subsequence_array

	mov	rax,rdi
	ret


__subseq_impl:
	mov	rsi,[rbp-STACKOFS_PARAM1]
	mov	rcx,[rbp-STACKOFS_PARAM2]
	mov	rdx,[rbp-STACKOFS_PARAM3]

	call	subsequence_array

	mov	rax,rdi
	ret


__append%21_impl:
	mov	rsi,[rbp-STACKOFS_PARAM1]
	mov	ecx,1
	call	grow_array

	mov	rsi,[rbp-STACKOFS_PARAM1]
	mov	ecx,[rsi]
	mov	rsi,[rsi+8]

	dec	rcx
	shl	rcx,3
	add	rsi,rcx

	mov	rax,[rbp-STACKOFS_PARAM2]
	mov	[rsi],rax
	ret


__join%21_impl:
	; is second array is empty?
	mov	rsi,[rbp-STACKOFS_PARAM2]
	mov	ecx,[rsi]
	or	ecx,ecx
	jz	.empty

	mov	rsi,[rbp-STACKOFS_PARAM1]
	mov	eax,[rsi]  ; orig length
	push	rax

	call	grow_array

	; get source elements
	mov	rsi,[rbp-STACKOFS_PARAM2]
	mov	ecx,[rsi]
	mov	rsi,[rsi+8]

	; get dest elements
	mov	rdi,[rbp-STACKOFS_PARAM1]
	mov	rdi,[rdi+8]
	pop	rax
	shl	rax,3
	add	rdi,rax

	call	copy_elements

.empty:
	mov	rax,[rbp-STACKOFS_PARAM1]
	ret


__insert%21_impl:
	mov	rsi,[rbp-STACKOFS_PARAM1]
	mov	edx,[rbp-STACKOFS_PARAM2]

	; an index <= zero will insert at the beginning
	or	edx,edx
	jns	.not_neg

	xor	edx,edx

.not_neg:
	; an index >= length will insert at the end
	mov	eax,[rsi]
	cmp	edx,eax
	jl	.not_after

	mov	edx,eax

.not_after:
	; grow it by one
	push	rdx
	mov	ecx,1
	call	grow_array
	pop	rdx

	; shift elements up
	mov	rsi,[rbp-STACKOFS_PARAM1]
	mov	rdi,[rsi+8]

	mov	ecx,[rsi]
	dec	rcx
	mov	rax,rcx
	shl	rax,3
	add	rdi,rax
.loop:
	cmp	ecx,edx
	jle	.finish

	sub	rdi,8
	mov	rax,[rdi]
	mov	[rdi+8],rax

	dec	ecx
	jmp	.loop

.finish:
	; store new element
	mov	rax,[rbp-STACKOFS_PARAM3]
	mov	[rdi],rax

	mov	rax,rsi
	ret


__delete%21_impl:
	mov	rsi,[rbp-STACKOFS_PARAM1]
	mov	ecx,[rsi]
	mov	eax,[rbp-STACKOFS_PARAM2]

	; trying to remove non-existent elements is a no-op
	or	eax,eax
	js	.outofbounds
	cmp	eax,ecx
	jge	.outofbounds

	; update length of array
	dec	ecx
	mov	[rsi],ecx

	; shift elements down
	mov	rdi,[rsi+8]
	mov	edx,[rbp-STACKOFS_PARAM2]
	mov	eax,edx
	shl	rax,3
	add	rdi,rax

	sub	ecx,edx
	jz	.done

.loop:
	mov	rax,[rdi+8]
	mov	[rdi],rax

	add	rdi,8
	dec	ecx
	jnz	.loop

.done:
	; store zero in the last slot
	; [ if we have a garbage collector, this prevents the GC
	;   from holding onto unused values ]
	xor	rax,rax
	mov	[rdi],rax

.outofbounds:
	mov	rax,[rbp-STACKOFS_PARAM1]
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


__find_impl:
	mov	rsi,[rbp-STACKOFS_PARAM1]
	mov	eax,[rsi]
	or	eax,eax
	jz	.not_found

	mov	rdi,[rsi+8]	; cur element
	xor	rdx,rdx		; cur index
.loop:
	mov	rcx,[rdi]
	call	.run_predicate

	; result is "true" ?
	cmp	eax,NIL
	jne	.found

	add	rdi,8
	inc	edx
	cmp	edx,[rsi]
	jl	.loop

.not_found:
	mov	eax,NIL
	ret

.found:
	mov	rax,rdx
	ret

.run_predicate:
	push	rdx
	push	rsi
	push	rdi

	; create CallFrame
	mov	rax,[rbp-STACKOFS_PARAM2]

	push	rbp
	push	rax	; info
	mov	rax,[rax]
	push	rax	; code ptr
	xor	rax,rax
	push	rax	; upvalues
	push	rcx	; param 1
	mov	rbp,rsp
	add	rbp,32
	mov	rax,[rbp-STACKOFS_CODE]
	call	rax
	leave

	pop	rdi
	pop	rsi
	pop	rdx
	ret


__map%21_impl:
	mov	rsi,[rbp-STACKOFS_PARAM1]
	mov	rdi,[rsi+8]	; cur element
	xor	rdx,rdx		; cur index
.loop:
	cmp	edx,[rsi]
	jge	.finished

	mov	rcx,[rdi]
	call	.run_predicate

	; store mapped value
	mov	[rdi],rax

	add	rdi,8
	inc	edx
	jmp	.loop

.finished:
	mov	rax,[rbp-STACKOFS_PARAM1]
	ret

.run_predicate:
	push	rdx
	push	rsi
	push	rdi

	; create CallFrame
	mov	rax,[rbp-STACKOFS_PARAM2]

	push	rbp
	push	rax	; info
	mov	rax,[rax]
	push	rax	; code ptr
	xor	rax,rax
	push	rax	; upvalues
	push	rcx	; param 1
	mov	rbp,rsp
	add	rbp,32
	mov	rax,[rbp-STACKOFS_CODE]
	call	rax
	leave

	pop	rdi
	pop	rsi
	pop	rdx
	ret


__filter%21_impl:
	mov	rsi,[rbp-STACKOFS_PARAM1]
	mov	ecx,[rsi]	; remaining
	or	ecx,ecx
	jz	.empty

	; we iterate UPWARDS through the elements

	mov	rsi,[rsi+8]	; cur source
	mov	rdi,rsi		; cur dest
	xor	rdx,rdx		; new size

.loop:
	mov	rbx,[rsi]
	call	.run_predicate

	cmp	eax,NIL
	je	.drop_it

	mov	rbx,[rsi]
	mov	[rdi],rbx

	add	rdi,8
	inc	rdx

.drop_it:
	add	rsi,8
	dec	ecx
	jnz	.loop

	; zero the remaining slots, for benefit of the GC
	xor	rax,rax

.clear_loop:
	cmp	rdi,rsi
	jae	.finish

	mov	[rdi],rax
	add	rdi,8
	jmp	.clear_loop

.finish:
	mov	rax,[rbp-STACKOFS_PARAM1]
	mov	[rax],rdx
	ret

.empty:
	mov	rax,[rbp-STACKOFS_PARAM1]
	ret

.run_predicate:
	push	rcx
	push	rdx
	push	rsi
	push	rdi

	; create CallFrame
	mov	rax,[rbp-STACKOFS_PARAM2]

	push	rbp
	push	rax	; info
	mov	rax,[rax]
	push	rax	; code ptr
	xor	rax,rax
	push	rax	; upvalues
	push	rbx	; param 1
	mov	rbp,rsp
	add	rbp,32
	mov	rax,[rbp-STACKOFS_CODE]
	call	rax
	leave

	pop	rdi
	pop	rsi
	pop	rdx
	pop	rcx
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;
; rdi = array ptr
; ecx = index
; --> rdi = pointer to value
;     eax = no change (it is preserved)
;
; NOTE: if index is out-of-bounds, an error is raised.
;
index_array:
	or	ecx,ecx
	js	.outofbounds

	cmp	ecx,[rdi]
	jge	.outofbounds

	mov	rdi,[rdi+8]  ; ptr to elements

	mov	ecx,ecx
	shl	rcx,3
	add	rdi,rcx

	ret

.outofbounds:
	mov	rsi,out_of_bounds_msg
	jmp	abort


;
; rsi = array ptr
; ecx = number of new elements (> 0)
;
grow_array:
	mov	eax,[rsi]
	add	eax,ecx
	cmp	eax,ARRAY_LIMIT
	ja	.overflow

	cmp	eax,[rsi+4]  ; capacity
	ja	.realloc

	mov	[rsi],eax
	ret

.realloc:
	; choose new capacity.
	; this is a compromise between wasting too much memory
	; (if cap is too great) and performing too many reallocs
	; (if cap is too small).
	mov	edx,eax
	add	edx,16
	shr	edx,2
	add	edx,eax

	; save old elements ptr
	mov	ecx,[rsi]
	mov	rdi,[rsi+8]

	mov	[rsi],eax
	mov	[rsi+4],edx

	push	rcx
	push	rdi
	push	rsi

	mov	eax,edx
	call	alloc_mem

	pop	rsi
	mov	[rsi+8],rdi

	; copy existing elements to new loc
	pop	rsi
	pop	rcx

	call	copy_elements
	ret

.overflow:
	mov	rsi,array_overflow_msg
	jmp	abort


;
; rsi = array ptr
; ecx = start position
; edx = end position
; --> edi = new array (possibly empty)
;
subsequence_array:
	; clamp start to 0
	xor	eax,eax
	or	ecx,ecx
	cmovs	ecx,eax

	; clamp end to length of array
	mov	eax,[rsi]
	cmp	edx,eax
	cmovg	edx,eax

	; calc new length, will it be empty?
	; [ this handles all weird cases, e.g. start >= length ]
	mov	eax,edx
	sub	eax,ecx
	jle	.empty

	; allocate new array
	push	rcx
	push	rax
	push	rsi

	mov	rcx,rax
	call	make_array

	pop	rsi
	pop	rcx
	pop	rax

	; offset source and copy elements
	shl	rax,3
	mov	rsi,[rsi+8]
	add	rsi,rax

	push	rdi
	mov	rdi,[rdi+8]

	call	copy_elements

	pop	rdi
	ret

.empty:
	xor	ecx,ecx
	call	make_array
	ret


;;--- editor settings -----
;; vi:ts=8:sw=8:noexpandtab
