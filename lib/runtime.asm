;;
;; Runtime support code
;;
;; Copyright 2019 Andrew Apted.
;; This code is under the GNU General Public License, version 3
;; or (at your option) any later version.
;;


init:
	call	populate_ARGS
	call	populate_ENVS

	ret


exit:
	mov	edi,[_EXIT_STATUS]
	mov	eax,sys_exit
	syscall


;
; rsi = message (as asciistr)
;
abort:
	push	rsi
	call	show_stacktrace
	pop	rsi

	; show message
	call	message_len

	mov	edx,eax		; length
	mov	edi,STDERR	; fd
	mov	eax,sys_write
	syscall

	; exit the process
	mov	edi,99
	mov	eax,sys_exit
	syscall


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


populate_ARGS:
	; in Linux (other Unixes too), the arguments are
	; placed onto the stack before the executable is run.
	; here we rely on %rbp being setup as a dummy CallFrame.

	push	r12
	push	r14

	; get count
	mov	rcx,[rbp+8]
	or	rcx,rcx
	jz	.done

	; grow the ARGS array
	mov	rsi,[_ARGS]
	call	grow_array

	mov	r12,[_ARGS]
	mov	r12,[r12+8]
	mov	rcx,[rbp+8]

	mov	r14,rbp
	add	r14,8*2

.loop:
	mov	rsi,[r14]
	add	r14,8

	push	rcx
	call	message_to_string
	pop	rcx

	mov	[r12],rdi
	add	r12,8

	dec	rcx
	jnz	.loop

.done:
	pop	r14
	pop	r12
	ret


populate_ENVS:
	; in Linux (other Unixes too), the environment strings
	; are placed onto the stack before the executable is run.
	; here we rely on %rbp being setup as a dummy CallFrame.

	push	r12
	push	r13
	push	r14
	push	r15

	; skip the arguments (and their trailing NULL)
	mov	rcx,[rbp+8]
	add	rcx,3
	shl	rcx,3

	mov	r12,rbp
	add	r12,rcx

	; count them
	mov	rdi,r12
	xor	rcx,rcx

.cnt_loop:
	mov	rax,[rdi]
	or	rax,rax
	jz	.counted

	inc	rcx
	add	rdi,8
	jmp	.cnt_loop

.counted:
	or	rcx,rcx
	jz	.done

	push	rcx

	; allocate memory for tuples --> r15
	mov	rax,rcx
	shl	rax,1
	call	alloc_mem
	mov	r15,rdi

	; grow the array, element ptr --> r14
	pop	rcx

	mov	rsi,[_ENVS]
	call	grow_array

	mov	rsi,[_ENVS]
	mov	r14,[rsi+8]

	; split the env strings
.loop:
	mov	rsi,[r12]
	add	r12,8
	or	rsi,rsi
	jz	.done

	call	.split_env

	mov	[r14],r15
	add	r14,8

	mov	[r15],rsi
	mov	[r15+8],rdi
	add	r15,16

	jmp	.loop

.done:
	pop	r15
	pop	r14
	pop	r13
	pop	r12
	ret

.split_env:
	; find the '=' between key and value
	mov	rdi,rsi

.spl_loop:
	mov	al,[rdi]
	inc	rdi

	or	al,al
	jz	.spl_fail

	cmp	al,61  ; '='
	jne	.spl_loop

	push	rdi

	; process key string
	mov	rax,rdi
	sub	rax,rsi
	dec	rax
	jz	.spl_fail

	call	message_to_string.with_length

	; save the key string, process value string
	mov	r13,rdi

	pop	rsi
	call	message_to_string

	mov	rsi,r13
	ret

.spl_fail:
	mov	rsi,empty_str
	mov	rdi,empty_str
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


show_stacktrace:
	push	r15
	mov	r15,rbp

	mov	rsi,stacktrace_msg
	call	print_message

.loop:
	; at the last frame?
	mov	rax,[r15]
	or	rax,rax
	jz	.finished

	call	.show_frame

	mov	r15,[r15]
	jmp	.loop

.finished:
	pop	r15
	ret

.show_frame:
	mov	eax,32  ; space
	call	print_char
	mov	eax,32
	call	print_char
	mov	eax,32
	call	print_char

	mov	rsi,[r15-STACKOFS_INFO]
	mov	rsi,[rsi+FUNCDEF_NAME]
	call	print_string

	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;
; rsi = template funcdef
; --> rdi = new function
;
make_lambda:
	push	r12	; source (template upvalue list)
	push	r13	; destination (instantiated upvalue table)
	push	r14	; num remaining

	; allocate the funcdef
	mov	eax,3
	push	rsi
	call	alloc_mem
	pop	rsi

	mov	rax,[rsi+FUNCDEF_CODE]
	mov	[rdi+FUNCDEF_CODE],rax

	mov	rax,[rsi+FUNCDEF_NAME]
	mov	[rdi+FUNCDEF_NAME],rax

	; create the upvalue table
	mov	r12,[rsi+FUNCDEF_UPVALS]
	or	r12,r12
	js	.no_upvalues

	; save result
	push	rdi

	mov 	eax,[r12]  ; count (always > 0)
	call	alloc_mem
	mov	r13,rdi

	mov	rdi,[rsp]
	mov	[rdi+FUNCDEF_UPVALS],r13

	mov	r14d,[r12]  ; count
	add	r12,4

	; create/find each UPVALUE record
.loop:
	mov	ecx,[r12]  ; stack_ofs
	add	r12,4

	call	capture_variable

	; store it
	mov	[r13],rdi
	add	r13,8

	dec	r14d
	jne	.loop

	; restore result
	pop	rdi

	pop	r14
	pop	r13
	pop	r12
	ret

.no_upvalues:
	; value here will not matter
	mov	[rdi+FUNCDEF_UPVALS],r12

	pop	r14
	pop	r13
	pop	r12
	ret

;
; ecx = if bit 31 is 0, stack_ofs  (4, 5, 6, etc...)
;       if bit 31 is 1, index into parent's upvalue table
; --> rdi = pointer to UPVALUE
;
capture_variable:
	or	ecx,ecx
	js	.from_parent

	; see if already captured
	mov	rdi,[rbp-STACKOFS_UPVAL]
.loop:
	or	rdi,rdi
	jz	.is_new

	cmp	ecx,[rdi+UPVALUE_OFFSET]
	je	.found_it

	; visit next in list
	mov	rdi,[rdi+UPVALUE_NEXT]
	jmp	.loop

.is_new:
	call	make_upvalue

	; link the fresh record into current CallFrame
	mov	rax,[rbp-STACKOFS_UPVAL]
	mov	[rdi+UPVALUE_NEXT],rax
	mov	[rbp-STACKOFS_UPVAL],rdi

.found_it:
	ret

.from_parent:
	and	ecx,0x7FFFFFFF

	mov	rdi,[rbp-STACKOFS_INFO]
	mov	rdi,[rdi+FUNCDEF_UPVALS]

	shl	rcx,3
	mov	rdi,[rdi+rcx]
	ret

;
; ecx = stack_ofs
; --> rdi = UPVALUE record
;
make_upvalue:
	push	rcx

	mov	eax,3
	call	alloc_mem

	pop	rax
	mov	[rdi+UPVALUE_OFFSET],rax

	; store pointer to value on stack
	shl     rax,3
	neg     rax
	add     rax,rbp
	mov	[rdi+UPVALUE_STORAGE],rax

	xor	rax,rax
	mov	[rdi+UPVALUE_NEXT],rax
	ret

;
; rcx = upvalue index
; --> rdi = pointer to value
;     eax = no change (it is preserved)
;
index_upvalue:
	mov	rdi,[rbp-STACKOFS_INFO]
	mov	rdi,[rdi+FUNCDEF_UPVALS]

	; get UPVALUE record
	shl	rcx,3
	mov	rdi,[rdi+rcx]

	mov	ecx,[rdi+UPVALUE_OFFSET]
	or	ecx,ecx
	js	.hoisted

.local:
	mov	rdi,[rdi+UPVALUE_STORAGE]
	ret

.hoisted:
	add	rdi,UPVALUE_STORAGE
	ret

;
; ecx = minimum stack_ofs.
;       all upvalues with offset >= %ecx get hoisted.
;       use zero for all remaining ones (for tail calls).
;
; --> eax = no change (it is preserved)
;
hoist_upvalues:
	push	rax
	push	r12	; current upvalue ptr
	push	r13	; minimum stack_ofs

	mov	r12,[rbp-STACKOFS_UPVAL]
	mov	r13,rcx
.loop:
	or	r12,r12
	jz	.finished

	; already hoisted?
	mov	eax,[r12+UPVALUE_OFFSET]
	or	eax,eax
	js	.no_match

	; in scope?
	cmp	eax,r13d
	jl	.no_match

	call	.hoist_var

.no_match:
	mov	r12,[r12+UPVALUE_NEXT]
	jmp	.loop

.finished:
	pop	r13
	pop	r12
	pop	rax
	ret

.hoist_var:
	; compute -8 * offset
	mov	ecx,[r12+UPVALUE_OFFSET]
	shl	rcx,3
	neg	rcx

	; transfer existing value
	mov	rax,[rbp+rcx]
	mov	[r12+UPVALUE_STORAGE],rax

	; mark it as hoisted
	mov	eax,-1
	mov	[r12+UPVALUE_OFFSET],eax
	ret


;;--- editor settings -----
;; vi:ts=8:sw=8:noexpandtab
