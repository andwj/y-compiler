;;
;; random numbers
;;
;; Copyright 2019 Andrew Apted.
;; This code is under the GNU General Public License, version 3
;; or (at your option) any later version.
;;


__rand_seed_impl:
	xor	eax,eax
	mov	[RNG_x],eax
	mov	[RNG_c],eax

	mov	eax,12345678
	mov	[RNG_w],eax

	mov	eax,[rbp-STACKOFS_PARAM1]
	and	eax,0xCCCCCCCC
	or	eax,1  ; ensure 'y' is never zero
	mov	[RNG_y],eax

	mov	eax,[rbp-STACKOFS_PARAM1]
	and	eax,0x33333333
	mov	[RNG_z],eax

	; warm up the generator (discard initial values)
	mov	esi,100
.loop:
	call	generate_random

	dec	esi
	jnz	.loop

	mov	eax,NIL
	ret


__rand_int_impl:
	call	generate_random

	; we require: 0 <= result < parameter

	mov	ecx,[rbp-STACKOFS_PARAM1]
	cmp	ecx,1
	jl	.fail
	je	.zero

	xor	edx,edx
	idiv	ecx

	mov	eax,edx
	ret

.zero:
	xor	eax,eax
	ret

.fail:
	; TODO raise a run-time error
	mov	eax,NIL
	ret


__rand_range_impl:
	call	generate_random

	; we require: low <= result <= high

	movsxd	rdx,[rbp-STACKOFS_PARAM1]
	movsxd	rcx,[rbp-STACKOFS_PARAM2]

	sub	rcx,rdx  ; high - low
	jl	.fail
	je	.same

	inc	rcx

	mov	eax,eax
	xor	rdx,rdx

	idiv	rcx

	mov	eax,edx
	add	eax,[rbp-STACKOFS_PARAM1]
	ret

.zero:
	xor	eax,eax
	ret

.same:
	mov	eax,edx
	ret

.fail:
	; TODO raise a run-time error
	mov	eax,NIL
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;
; The RNG algorithm used is 'JKISS32' by (I presume) George Marsaglia,
; described in David Jones's paper titled "Good Practice in (Pseudo)
; Random Number Generation for Bioinformatics Applications".
;

generate_random:
	; x is a simple accumulator
	mov	eax,[RNG_x]
	add	eax,1411392427
	mov	[RNG_x],eax

	; y is an 'xorshift' generator, y must never be 0
	mov	ecx,[RNG_y]

	mov	eax,ecx
	shl	eax,5
	xor	ecx,eax

	mov	eax,ecx
	shr	eax,7
	xor	ecx,eax

	mov	eax,ecx
	shl	eax,22
	xor	ecx,eax

	mov	[RNG_y],ecx

	; w is an 'add-with-carry' (AWC) generator
	mov	eax,[RNG_z]
	add	eax,[RNG_w]
	add	eax,[RNG_c]

	mov	ecx,[RNG_w]
	mov	[RNG_z],ecx

	mov	ecx,eax
	shr	ecx,31
	mov	[RNG_c],ecx

	and	eax,0x7FFFFFFF
	mov	[RNG_w],eax

	; combine them
	add	eax,[RNG_x]
	add	eax,[RNG_y]
	ret


;;--- editor settings -----
;; vi:ts=8:sw=8:noexpandtab
