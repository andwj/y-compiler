;;
;; Memory allocation
;;
;; Copyright 2019 Andrew Apted.
;; This code is under the GNU General Public License, version 3
;; or (at your option) any later version.
;;


;
; ecx = number of elements (can be zero)
; --> rdi = pointer to allocated array
;
make_array:
	push	rcx

	or	ecx,ecx
	jz	.empty

	mov	rax,rcx
	call	alloc_mem

	push	rdi
	jmp	.alloc_base

.empty:
	; dummy pointer
	mov	rdi,-1
	push	rdi

.alloc_base:
	mov	eax,2
	call	alloc_mem

	pop	rsi
	mov	[rdi+8],rsi	; elements

	pop	rcx
	mov	[rdi],ecx	; length
	mov	[rdi+4],ecx	; capacity

	ret

;
; ecx = number of elements
; --> rdi = pointer freshly allocated array
;
make_tuple:
	mov	eax,ecx
	call	alloc_mem
	ret

;
; eax = length of string
; --> rdi = pointer to allocated string
;     ecx = length of string
;
; NOTE: the new string has the length already stored.
;
make_string:
	mov	eax,eax
	push	rax

	; add room for length
	inc	rax

	; round up to quadwords
	inc	rax
	shr	rax,1

	call	alloc_mem

	pop	rcx
	mov	[rdi],ecx
	ret

;
; rsi = source message (as asciistr)
; --> rdi = newly allocated and converted string
;
message_to_string:
	call	message_len

.with_length:
	push	rsi
	call	make_string
	pop	rsi

	mov	rdx,rdi
	or	ecx,ecx
	jz	.finished

.loop:
	add	rdx,4

	xor	eax,eax
	mov	al,[rsi]
	inc	rsi

	; disallow values >= 0x80
	cmp	eax,0x80
	jb	.ok

	mov	eax,63  ; '?'
.ok:
	mov	[rdx],eax

	dec	ecx
	jnz	.loop

.finished:
	ret

;
; rsi = string
; eax = unicode char
; --> rdi = new string
;
append_char:
	; use stack as a temp buffer
	xor	rcx,rcx
	push	rcx

	inc	ecx
	mov	[rsp],ecx
	mov	[rsp+4],eax

	mov	rdi,rsp
	call	append_string

	pop	rcx
	ret

;
; rsi = string A
; rdi = string B
; --> rdi = new string (A + B)
;
append_string:
	push	r12
	push	r13
	push	r14

	mov	r12,rsi
	mov	r13,rdi

	; calc new length (in runes)
	mov	eax,[r12]
	add	eax,[r13]
	mov	r14,rax

	cmp	r14,STRING_LIMIT
	ja	.overflow

	; allocate it
	call	make_string

	; save result string ptr
	push	rdi

	; copy first half
	add	rdi,4

	mov	rsi,r12
	mov	ecx,[rsi]
	shl	ecx,2
	add	rsi,4
	call	copy_memory

	; copy second half
	mov	rsi,r13
	mov	ecx,[rsi]
	shl	ecx,2
	add	rsi,4
	call	copy_memory

	; restore result string ptr
	pop	rdi

	pop	r14
	pop	r13
	pop	r12
	ret

.overflow:
	mov	rsi,string_overflow_msg
	jmp	abort


;
; rax = pointer to tuple
; ecx = number of elements
; --> rdi = the duplicated tuple
;
duplicate_tuple:
	push	rax
	push	rcx

	call	make_tuple

	pop	rcx
	pop	rsi

	push	rdi
	call	copy_elements
	pop	rdi

	ret

;
; rsi = source
; rdi = destination
; rcx = size (in quadwords), can be zero
;
; NOTE: memory ranges must not overlap!
;
copy_elements:
	or	rcx,rcx
	jz	.done
.loop:
	mov	rax,[rsi]
	add	rsi,8

	mov	[rdi],rax
	add	rdi,8

	dec	rcx
	jnz	.loop
.done:
	ret


;
; rsi = source
; rdi = destination
; rcx = size (in bytes), can be zero
;
; NOTE: memory ranges must not overlap!
;
copy_memory:
	or	rcx,rcx
	jz	.done
.loop:
	mov	al,[rsi]
	inc	rsi

	mov	[rdi],al
	inc	rdi

	dec	rcx
	jnz	.loop
.done:
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;
; rax = quadwords to allocate
; --> rdi = pointer to memory
;
; NOTE:
;   this is extremely basic, we have a current block and we attempt
;   to use free space at the that block.  If that fails, we allocate
;   a new block from the kernel.  No memory is ever freed.
;
; TODO: if size is huge, just round up to NNN and call alloc_OS
;
alloc_mem:
	; multiply by 8
	shl	rax,3

	; no block yet?
	mov	rcx,[memblock_p]
	or	rcx,rcx
	jz	.newblock

	; not enough room?
	mov	rcx,[memblock_free]
	cmp	rax,rcx
	jbe	.takeslice

.newblock:
	push	rax

	; round the size upto a multiple of 32k
	or	rax,32767
	inc	rax
	add	rax,32768
	mov	[memblock_free],rax

	call	alloc_from_OS

	mov	[memblock_p],rdi
	mov	rcx,[memblock_free]
	pop	rax	; get original size

.takeslice:
	; re-use end of existing block
	sub	rcx,rax
	mov	[memblock_free],rcx

	mov	rcx,[memblock_p]
	mov	rdi,rcx

	; bump pointer to free space
	add	rcx,rax
	mov	[memblock_p],rcx

	ret


;
; rax = size in bytes, non-zero, multiple of page size
; --> rdi = pointer to memory
;
alloc_from_OS:
	xor	rdi,rdi				; addr (NULL)
	mov	rsi,rax				; length
	mov	edx,PROT_READ+PROT_WRITE	; prot
	mov	r10,MAP_ANONYMOUS+MAP_PRIVATE	; flags
	mov	r8,-1				; fd
	xor	r9,r9				; offset

	mov	eax,sys_mmap
	syscall

	; negative value means an error
	or	rax,rax
	js	.failed

.ok:
	mov	rdi,rax
	ret

.failed:
	mov	rsi,out_of_memory_msg
	jmp	abort


;;--- editor settings -----
;; vi:ts=8:sw=8:noexpandtab
