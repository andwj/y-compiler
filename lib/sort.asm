;;
;; sorting primitives
;;
;; Copyright 2019 Andrew Apted.
;; This code is under the GNU General Public License, version 3
;; or (at your option) any later version.
;;


__sort%21_impl:
	push	r12
	push	r14

	; less than two elements?
	mov	rsi,[rbp-STACKOFS_PARAM1]
	mov	ecx,[rsi]
	cmp	ecx,2
	jl	.finished

	mov	r12,[rsi+8]	; start element

	mov	r14d,ecx
	dec	r14
	shl	r14,3
	add	r14,r12		; end element

	call	.safe_sort

.finished:
	mov	rax,[rbp-STACKOFS_PARAM1]

	pop	r14
	pop	r12
	ret



.safe_sort:
	; this is a Quick-sort algorithm, but crafted so that the
	; comparison function may return complete garbage and this
	; code will not crash (e.g. access out-of-bound elements).
	;
	; r12 = start element
	; r14 = end element
	; [ comparator predicate is in stack frame ]

	cmp	r12,r14
	jae	.safe_done

	; check for the two element case
	mov	r10,r14
	sub	r10,r12
	cmp	r10,8
	je	.only_two_elems

	; choosing a pivot in the middle should avoid the worst-case
	; behavior that otherwise occurs when pivot is first or last
	; element and the array is already sorted.
	shr	r10,4
	shl	r10,3
	add	r10,r12

	call	.partition

	; handle degenerate case 1: mid <= s
	cmp	r10,r12
	jbe	.degen1

	; handle degenerate case 2: mid+1 >= e
	mov	rax,r10
	add	rax,8
	cmp	rax,r14
	jae	.degen2

	; recursively sort each sub-group.
	; [ but only use recursion on the smallest group, since
	;   it helps to limit stack usage ]
	mov	rax,r10
	sub	rax,r12

	mov	rbx,r14
	sub	rbx,r10

	cmp	rbx,rax
	jb	.second_smaller

.first_smaller:
	push	r10
	push	r14

	; sort s ... mid
	mov	r14,r10
	call	.safe_sort

	; sort mid+1 ... e
	pop	r14
	pop	r12
	add	r12,8
	jmp	.safe_sort

.second_smaller:
	; sort mid+1 ... e
	push	r12
	push	r10

	mov	r12,r10
	add	r12,8
	call	.safe_sort

	; sort s ... mid
	pop	r14
	pop	r12
	jmp	.safe_sort

.degen1:
	add	r12,8
	jmp	.safe_sort

.degen2:
	sub	r14,8
	jmp	.safe_sort

.only_two_elems:
	; check if *e < *s
	mov	rbx,[r14]
	mov	rcx,[r12]

	call	.run_predicate

	cmp	eax,NIL
	je	.safe_done

	; swap!
	mov	[r12],rbx
	mov	[r14],rcx

.safe_done:
	ret



.partition:
	; this is Hoare's algorithm
	;
	; r12 = lo element (preserved)
	; r14 = hi element (preserved)
	; r10 = pivot element
	; --> r10 = mid element (i.e. where pivot ended up)

	mov	rsi,r12  ; s
	mov	rdi,r14  ; e

.p_loop1:
	cmp	rsi,rdi
	ja	.p1

	; *s < *pivot ?
	mov	rbx,[rsi]
	mov	rcx,[r10]

	call	.run_predicate

	cmp	eax,NIL
	je	.p1

	add	rsi,8
	jmp	.p_loop1

.p1:
	; s > hi ?
	cmp	rsi,r14
	ja	.p_less

.p_loop2:
	cmp	rdi,rsi
	jb	.p2

	; *e >= *pivot ?
	mov	rbx,[rdi]
	mov	rcx,[r10]
	call	.run_predicate

	cmp	eax,NIL
	jne	.p2

	sub	rdi,8
	jmp	.p_loop2

.p2:
	; e < lo ?
	cmp	rdi,r12
	jb	.p_high

	; s < e ?
	cmp	rsi,rdi
	jb	.p3

	mov	r10,rsi
	sub	r10,8
	ret

.p3:
	; swap!
	mov	rbx,[rsi]
	mov	rcx,[rdi]
	mov	[rsi],rcx
	mov	[rdi],rbx

	; if pivot ptr matched s or e, move it too
	cmp	r10,rsi
	jne	.p4
	mov	r10,rdi
	jmp	.p5
.p4:
	cmp	r10,rdi
	jne	.p5
	mov	r10,rsi
.p5:
	add	rsi,8
	sub	rdi,8
	jmp	.p_loop1

.p_less:
	; all values were < pivot, including the pivot itself!

	cmp	r10,r14
	je	.p_less2

	; swap pivot and hi
	mov	rbx,[r10]
	mov	rcx,[r14]
	mov	[r10],rcx
	mov	[r14],rbx

.p_less2:
	mov	r10,r14
	sub	r10,8
	ret

.p_high:
	; all values were >= pivot

	cmp	r10,r12
	je	.p_high2

	; swap pivot and lo
	mov	rbx,[r10]
	mov	rcx,[r12]
	mov	[r10],rcx
	mov	[r12],rbx

.p_high2:
	mov	r10,r12
	ret



.run_predicate:
	push	rbx
	push	rcx
	push	rsi
	push	rdi

	; create CallFrame
	mov	rax,[rbp-STACKOFS_PARAM2]

	push	rbp
	push	rax	; info
	mov	rax,[rax]
	push	rax	; code ptr
	xor	rax,rax
	push	rax	; upvalues
	push	rbx	; param 1
	push	rcx	; param 2
	mov	rbp,rsp
	add	rbp,40
	mov	rax,[rbp-STACKOFS_CODE]
	call	rax
	leave

	pop	rdi
	pop	rsi
	pop	rcx
	pop	rbx
	ret


;;--- editor settings -----
;; vi:ts=8:sw=8:noexpandtab
