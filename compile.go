// Copyright 2019 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

import "fmt"

type Closure struct {
	// when this is non-nil, this is a built-in function, and ALL the
	// other fields in this struct are meaningless.
	builtin *Builtin

	_def *FuncDef

	// the uncompiled lambda expression, i.e. (fun ... body)
	// _OR_ an uncompiled top-level expression.
	uncompiled []*Token

	// the static type of this function (once fully compiled).
	// this also contains the parameter names.
	kind *Type

	// the compiled form is a sequence of instructions
	instructions []Instruction

	// all the constant values used in the function
	constants []Value

	// locals created in this closure (such as parameters and let bindings).
	locals *LocalGroup

	// the parent function which contains this one.  Will be nil for
	// global functions.  Used to determine the scope of local vars,
	// and to determine which local vars are upvalues.
	parent *Closure

	// this is only used by a *template* lambda function.
	// it references variables which need to be captured when this
	// template is instantiated into a real function value.
	// indexes into this array are used to read/write such vars.
	//
	// NOTE: this may include variables which are not referenced
	//       directly by this lambda, only indirectly in a sub-lambda
	//       (or sub-sub-lambda, etc).  They need to be here so that
	//       they actually get captured.
	captures []*LocalVar

	// this keeps track of the offset from %rbp to the current
	// stack pointer in %rsp.  let statements (etc) increase it
	// for each variable, and decrease it afterwards.
	stack_ofs int

	// > 0 when a type cast is active (and hence implicit casting of
	// untyped literals should be inhibited).
	in_type_cast int

	debug_name string

	cur_line int

	// this allows dot labels to have a numeric suffix to ensure they
	// are unique within the function (in ASM).
	lab_counter int

	lambda_counter int

	ret_addr_ofs int
}

var lambda_templates []*Closure

type LocalGroup struct {
	vars map[string]*LocalVar

	parent *LocalGroup

	min_stack int
}

type LocalVar struct {
	name      string
	kind      *Type
	offset    int
	captured  bool
	immutable bool
	owner     *Closure
}

func CompileFunction(cl *Closure, debug_name string, method_type *Type) error {
	cl.instructions = make([]Instruction, 0)
	cl.captures = make([]*LocalVar, 0)
	cl.debug_name = debug_name

	if cl.kind == nil || cl.kind.base != VAL_Function {
		panic("CompileFunction with bad kind")
	}

	// this is size of the CallFrame, which sits immediately below %rbp
	// (and is setup by the caller of a function).
	cl.stack_ofs = 3

	cl.PushLocalGroup()

	if len(cl.uncompiled) < 2 {
		return ParsingError(cl.uncompiled[0], "missing body in fun")
	}

	cl.cur_line = cl.uncompiled[0].LineNum

	uncompiled := cl.uncompiled[1:]

	body := uncompiled[len(uncompiled)-1]

	// add the parameters (they have already been parsed)
	for _, par := range cl.kind.param {
		cl.stack_ofs += 1
		cl.AddLocal(par.name, par.kind, cl.stack_ofs, false)
	}

	// the next stack slot holds the caller return address
	// (the CPU pushes it when executing the "call" instruction).
	cl.stack_ofs += 1
	cl.ret_addr_ofs = cl.stack_ofs

	// compile the body
	kind, err := cl.CompileToken(body, true /* tail */)
	if err != nil {
		return err
	}

	// check return type of function
	// NOTE: untyped literal casting is handled elsewhere.
	if !cl.kind.sub.Equal(kind) {
		return ParsingError(cl.uncompiled[0],
			"type mismatch on fun result, specified %s, body is %s",
			cl.kind.sub.String(), kind.String())
	}

	cl.PopLocalGroup()

	cl.EmitInst("ret", "", "", "")

	return Ok
}

//----------------------------------------------------------------------

func (cl *Closure) EmitLabel(lab string) {
	ins := Instruction{lab, "", "", "", "", cl.cur_line}
	cl.instructions = append(cl.instructions, ins)
}

func (cl *Closure) EmitInst(inst, dest, src, comment string) {
	ins := Instruction{"", inst, dest, src, comment, cl.cur_line}
	cl.instructions = append(cl.instructions, ins)
}

func (cl *Closure) EmitBlank() {
	ins := Instruction{"", "", "", "", "", cl.cur_line}
	cl.instructions = append(cl.instructions, ins)
}

func (cl *Closure) AddConstant(v Value) int16 {
	if cl.constants == nil {
		cl.constants = make([]Value, 0)
	}

	// see if we can re-use an existing constant
	for idx, other := range cl.constants {
		if v.BasicEqual(&other) {
			return int16(-1 - idx)
		}
	}

	// nope, so add a new one
	res := int16(-1 - len(cl.constants))

	cl.constants = append(cl.constants, v)

	return res
}

func (cl *Closure) PushLocalGroup() {
	group := new(LocalGroup)
	group.vars = make(map[string]*LocalVar)
	group.parent = cl.locals
	group.min_stack = cl.stack_ofs + 1

	cl.locals = group
}

func (cl *Closure) PopLocalGroup() {
	if cl.locals == nil {
		panic("PopLocalGroup with empty locals")
	}

	// add code to hoist any captured vars
	has_capture := false

	for _, lvar := range cl.locals.vars {
		if lvar.captured {
			has_capture = true
			break
		}
	}

	if has_capture {
		// NOTE: hoist_upvalues will not modify %rax
		cl.EmitInst("mov", "ecx", fmt.Sprintf("%d", cl.locals.min_stack), "")
		cl.EmitInst("call", "hoist_upvalues", "", "")
	}

	cl.locals = cl.locals.parent
}

func (cl *Closure) PopLocalVars(count int) {
	// NOTE: parameters do NOT count as locals for this

	if count > 0 {
		cl.EmitInst("add", "rsp", fmt.Sprintf("%d", count*8), "pop locals")
		cl.stack_ofs -= count
	}
}

func (cl *Closure) AddLocal(name string, kind *Type, ofs int, immutable bool) {
	if cl.locals == nil {
		panic("AddLocal when closure has no LocalGroup")
	}

	lvar := new(LocalVar)
	lvar.name = name
	lvar.kind = kind
	lvar.offset = ofs
	lvar.immutable = immutable
	lvar.owner = cl

	cl.locals.vars[name] = lvar
}

func (cl *Closure) LookupLocal(name string) *LocalVar {
	for p := cl; p != nil; p = p.parent {
		for group := p.locals; group != nil; group = group.parent {
			lvar := group.vars[name]

			if lvar != nil {
				// found it
				return lvar
			}
		}
	}

	// not found
	return nil
}

func (cl *Closure) CaptureLocal(lvar *LocalVar) int {
	lvar.captured = true

	idx := cl.AddCapture(lvar)

	// if this closure is a sub-lambda inside a lambda
	// (or a sub-sub-lambda, etc) then we rely on all lambdas
	// above us to perform the capture, since the variable may
	// cease to exist on the stack by the time this closure
	// gets instantiated.

	// since the owner *must* be higher up the chain, we know
	// cl.parent != nil and 'p' *will* eventually reach the
	// owner in this loop.

	for p := cl.parent; p != lvar.owner; p = p.parent {
		p.AddCapture(lvar)
	}

	return idx
}

func (cl *Closure) AddCapture(lvar *LocalVar) int {
	// check if already in the list...
	for idx, ptr := range cl.captures {
		if ptr == lvar {
			return idx
		}
	}

	idx := len(cl.captures)
	cl.captures = append(cl.captures, lvar)

	return idx
}

//----------------------------------------------------------------------

func (cl *Closure) CompileToken(t *Token, tail bool) (*Type, error) {
	cl.cur_line = t.LineNum

	switch t.Kind {
	case TOK_Expr:
		return cl.CompileExpr(t, tail)

	case TOK_Array:
		return cl.CompileArray(t, tail)

	case TOK_Map:
		return cl.CompileTuple(t, tail)

	case TOK_Name:
		return cl.CompileName(t)

	default:
		return cl.CompileImmediate(t, tail)
	}
}

func (cl *Closure) CompileName(t *Token) (*Type, error) {
	name := t.Str

	// find scope of the name
	lvar := cl.LookupLocal(name)

	if lvar == nil {
		// identifier is not a local, try a global
		g_idx, exist := global_lookup[name]
		if !exist {
			return nil, ParsingError(t, "unknown binding: '%s'", name)
		}

		g_name := EncodeIdent(name)

		cl.EmitInst("mov", "rax", "["+g_name+"]", "")

		loc := globals[g_idx].loc

		if loc.kind.base == VAL_Function {
			loc.Clos._def = CreateFuncDef(loc.Clos, name)
		}

		return loc.kind, Ok
	}

	if lvar.owner == cl {
		// local identifiers exist in current stack frame
		access := fmt.Sprintf("[rbp-%d]", lvar.offset*8)
		cl.EmitInst("mov", "rax", access, "local "+name)

	} else {
		// variable is local to a parent -- an upvalue
		upv_idx := cl.CaptureLocal(lvar)

		cl.EmitInst("mov", "rcx", fmt.Sprintf("%d", upv_idx), "")
		cl.EmitInst("call", "index_upvalue", "", "")
		cl.EmitInst("mov", "rax", "[rdi]", "")
	}

	return lvar.kind, Ok
}

func (cl *Closure) CompileImmediate(t *Token, tail bool) (*Type, error) {
	// this handles simple values: integers and strings
	var lit Value

	err := lit.ParseLiteral(t)
	if err != nil {
		return nil, err
	}

	// does function have an explicit return type?
	// if so, check if we can implicitly cast an untyped literal.
	if cl.kind != nil && cl.kind.sub != nil && tail && cl.in_type_cast == 0 {
		ret_type := cl.kind.sub

		if ret_type.CastableFromUntyped(lit.kind, t.Kind) {
			lit.kind = ret_type
		}
	}

	switch lit.kind.base {
	case VAL_Int:
		s := fmt.Sprintf("%d", lit.Int)
		cl.EmitInst("mov", "rax", s, "")

	case VAL_String:
		def := CreateStringDef(lit.Str)
		cl.EmitInst("mov", "rax", def.label, "")

	default:
		panic("UNSUPPORTED LITERAL VALUE")
	}

	return lit.kind, Ok
}

func (cl *Closure) CompileArray(t *Token, tail bool) (*Type, error) {
	children := t.Children

	var elem_type *Type

	if len(children) > 0 && children[0].Match(":") {
		if len(children) < 2 {
			return nil, ParsingError(t, "missing type spec in array literal")
		}

		kind, err := ParseType(children[1])
		if err != nil {
			return nil, ParsingError(t, "array literal: %s", err.Error())
		}

		elem_type = kind

		children = children[2:]
	}

	// create a new array (at final size)
	cl.EmitInst("mov", "ecx", fmt.Sprintf("%d", len(children)), "")
	cl.EmitInst("call", "make_array", "", "")
	cl.EmitInst("push", "rdi", "", "")

	cl.stack_ofs += 1

	// the S field gets updated afterwards with type of array.
	//cl.Emit3(OP_MAKE_ARRAY, NO_PART, arr_ofs)

	// process each element
	for idx, tok := range children {
		kind, err := cl.CompileToken(tok, false)
		if err != nil {
			return nil, err
		}

		if elem_type == nil {
			elem_type = kind

		} else if elem_type.Equal(kind) {
			// ok

		} else if elem_type.CastableFromUntyped(kind, tok.Kind) {
			// ok, allow untyped literals to be casted to a custom type

		} else {
			return nil, ParsingError(tok, "array literal has different types: %s and %s",
				elem_type.String(), kind.String())
		}

		// store this element
		cl.EmitInst("mov", "rsi", "[rsp]", "")
		cl.EmitInst("mov", "rsi", "[rsi+8]", "")

		dest := fmt.Sprintf("[rsi+%d]", 8*idx)

		cl.EmitInst("mov", dest, "rax", "")
	}

	cl.EmitInst("pop", "rax", "", "")
	cl.stack_ofs -= 1

	if elem_type == nil {
		return nil, ParsingError(t, "empty array literal without type spec")
	}

	arr_type := NewType(VAL_Array)
	arr_type.sub = elem_type

	// check if we can implicitly cast an untyped literal...
	if cl.kind != nil && cl.kind.sub != nil && tail && cl.in_type_cast == 0 {
		ret_type := cl.kind.sub

		if ret_type.CastableFromUntyped(arr_type, TOK_Array) {
			arr_type = ret_type
		}
	}

	return arr_type, Ok
}

func (cl *Closure) CompileTuple(t *Token, tail bool) (*Type, error) {
	children := t.Children

	if len(children) == 0 {
		return nil, ParsingError(t, "tuple literal must not be empty")
	}

	// create a new empty tuple.
	cl.EmitInst("mov", "ecx", fmt.Sprintf("%d", len(children)), "")
	cl.EmitInst("call", "make_tuple", "", "")
	cl.EmitInst("push", "rdi", "", "")

	cl.stack_ofs += 1

	tup_type := NewType(VAL_Tuple)

	for idx, child := range children {
		elem_type, err := cl.CompileToken(child, false)
		if err != nil {
			return nil, err
		}

		tup_type.AddParam("", elem_type)

		// store this element
		cl.EmitInst("mov", "rsi", "[rsp]", "")

		dest := fmt.Sprintf("[rsi+%d]", 8*idx)

		cl.EmitInst("mov", dest, "rax", "")
	}

	cl.EmitInst("pop", "rax", "", "")
	cl.stack_ofs -= 1

	// check if we can implicitly cast an untyped literal...
	if cl.kind != nil && cl.kind.sub != nil && tail && cl.in_type_cast == 0 {
		ret_type := cl.kind.sub

		if ret_type.CastableFromUntyped(tup_type, TOK_Map) {
			tup_type = ret_type
		}
	}

	return tup_type, Ok
}

//----------------------------------------------------------------------

func (cl *Closure) CompileExpr(t *Token, tail bool) (*Type, error) {
	if len(t.Children) == 0 {
		return nil, ParsingError(t, "empty expression in ()")
	}

	head := t.Children[0]

	// check for special forms...
	switch {
	case head.Match("$"):
		return cl.CompileConcat(t)

	case head.Match("begin"):
		return cl.CompileBegin(t, tail)

	case head.Match("let"):
		return cl.CompileLet(t, tail)

	case head.Match("fun"):
		return cl.CompileLambda(t)

	case head.Match("set!"):
		return cl.CompileAssign(t)

	case head.Match("base"):
		return cl.CompileTypeBase(t, tail)

	case head.Match("dup"):
		return cl.CompileDup(t)

	case head.Match("if"):
		return cl.CompileIf(t, tail)

	case head.Match("while"):
		return cl.CompileWhile(t)

	case head.Match("for"):
		return cl.CompileForRange(t)

	case head.Match("for-each"):
		return cl.CompileForEach(t, VAL_Array)

	case head.Match("str-for-each"):
		return cl.CompileForEach(t, VAL_String)

	case head.Match("def"), head.Match("macro"):
		return nil, ParsingError(t, "cannot use '%s' within code", head.Str)
	}

	// a math expression?
	if HasOperator(t) {
		return cl.CompileMathExpr(t)
	}

	// type cast?
	if head.Kind == TOK_Name {
		user := user_types[head.Str]
		if user != nil {
			return cl.CompileTypeCast(t, user, tail)
		}
	}

	// a generic builtin?
	if head.Kind == TOK_Name {
		idx, exist := generic_lookup[head.Str]
		if exist {
			gen := generic_builtins[idx]
			return cl.CompileFuncCall(t, gen, false)
		}
	}

	return cl.CompileFuncCall(t, nil, tail)
}

func (cl *Closure) CompileFuncCall(t *Token, gen *GenericBuiltin, tail bool) (*Type, error) {
	children := t.Children

	leave_ofs := cl.stack_ofs

	num_params := len(children) - 1

	// generic builtins have no fixed type, so this is nil when gen != nil
	var fun_type *Type

	if gen == nil {
		// compile the function itself, so we know what parameters it needs
		// or whether it is a container access or method call.
		var err error
		fun_type, err = cl.CompileToken(children[0], false)
		if err != nil {
			return nil, err
		}
	} else {
		// make a funcdef (mark it as used)
		gen.cl._def = CreateFuncDef(gen.cl, gen.name)

		cl.EmitInst("mov", "rax", gen.cl._def.label, "")
	}

	// is it actually a method call?
	is_method := false

	var method_name *Token
	if len(children) >= 2 {
		method_name = children[1]
	}

	if gen != nil {
		// a generic builtin

	} else if fun_type.custom != "" && method_name != nil &&
		method_name.Kind == TOK_Name &&
		fun_type.methods.Exists(method_name.Str) {

		is_method = true

	} else if fun_type.base == VAL_Array ||
		fun_type.base == VAL_Tuple ||
		fun_type.base == VAL_String {

		kind, err := cl.CompileReadElement(fun_type, children)

		return kind, err
	}

	// normal func call or method call

	if is_method {
		fun_loc := fun_type.methods.GetLoc(method_name.Str)
		fun_type = fun_loc.kind

		// grab the actual FuncDef
		cl.EmitInst("mov", "rcx", "rax", "")
		cl.EmitInst("mov", "rax", fun_loc.Clos._def.label, "")

	}

	// construct the CallFrame
	if !tail {
		cl.EmitInst("push", "rbp", "", "")
		cl.stack_ofs += 1
	}

	cl.EmitInst("push", "rax", "", "info")

	cl.EmitInst("mov", "rax", "[rax]", "")
	cl.EmitInst("push", "rax", "", "code ptr")

	cl.EmitInst("xor", "rax", "rax", "")
	cl.EmitInst("push", "rax", "", "upvalues")

	cl.stack_ofs += 3

	if gen == nil {
		if fun_type.base != VAL_Function {
			return nil, ParsingError(t, "Cannot call non-function, got %s", fun_type.base.String())
		}

		if num_params != len(fun_type.param) {
			return nil, ParsingError(t, "wrong number of parameters, wanted %d, got %d",
				len(fun_type.param), len(children))
		}
	} else {
		if gen.args >= 0 && num_params != gen.args {
			return nil, ParsingError(t, "wrong number of parameters, wanted %d, got %d",
				gen.args, num_params)
		}
	}

	child_types := make([]*Type, 0)

	// self parameter for methods
	if is_method {
		cl.EmitInst("push", "rcx", "", "self param")
		cl.stack_ofs += 1
	}

	// compile each parameter
	for idx := 0; idx < num_params; idx++ {
		// for methods, skip "self" as we have done it already
		if is_method && idx == 0 {
			continue
		}

		sub := children[1+idx]

		sub_kind, err := cl.CompileToken(sub, false)
		if err != nil {
			return nil, err
		}

		child_types = append(child_types, sub_kind)

		// generic builtins have their own type checkers
		if gen == nil {
			PT := fun_type.param[idx].kind

			if PT.Equal(sub_kind) {
				// ok

			} else if PT.CastableFromUntyped(sub_kind, sub.Kind) {
				// ok, allow untyped literal to be implicitly casted

			} else {
				return nil, ParsingError(sub, "parameter is wrong type, wanted %s, got %s",
					PT.String(), sub_kind.String())
			}
		}

		comment := fmt.Sprintf("param %d", idx+1)

		cl.EmitInst("push", "rax", "", comment)
		cl.stack_ofs += 1
	}

	// call validator of the builtin
	if gen != nil {
		err := gen.validator(child_types)
		if err != nil {
			return nil, ParsingError(t, "%s", err.Error())
		}
	}

	if !tail {
		// setup the new %rbp value
		cl.EmitInst("mov", "rbp", "rsp", "")
		cl.EmitInst("add", "rbp", fmt.Sprintf("%d", (num_params+3)*8), "")

		// get code pointer and call it
		cl.EmitInst("mov", "rax", "[rbp-STACKOFS_CODE]", "")
		cl.EmitInst("call", "rax", "", "")

		// pop everything
		cl.EmitInst("leave", "", "", "")
		cl.stack_ofs = leave_ofs

	} else {
		// hoist captured vars to the heap.
		// [ as the normal code (from "let" etc) won't execute now ]
		cl.EmitInst("mov", "ecx", "1", "")
		cl.EmitInst("call", "hoist_upvalues", "", "")

		// push a copy of the return address
		cl.EmitInst("mov", "rax", EncodeLocal(cl.ret_addr_ofs), "")
		cl.EmitInst("push", "rax", "", "copy ret addr")
		cl.stack_ofs += 1

		bottom := cl.stack_ofs

		// copy new frame over the old frame
		depth := 4 + num_params
		cl.MoveStackElems(bottom, depth)

		// update %rsp to point at copy of return address
		cl.EmitInst("mov", "rax", "rbp", "")
		cl.EmitInst("sub", "rax", fmt.Sprintf("%d", depth*8), "")
		cl.EmitInst("mov", "rsp", "rax", "")

		// get code pointer and JUMP to it
		cl.EmitInst("mov", "rax", "[rbp-STACKOFS_CODE]", "")
		cl.EmitInst("jmp", "rax", "", "tail call")
		cl.EmitBlank()

		cl.stack_ofs = leave_ofs
	}

	if gen != nil {
		// get the result type
		var res_type *Type

		switch gen.result {
		case VAL_Int:
			res_type = int_type

		case VAL_String:
			res_type = str_type

		case BVAL_First:
			res_type = child_types[0]

		default:
			panic("Bad generic builtin result")
		}

		return res_type, Ok
	}

	return fun_type.sub, Ok
}

func (cl *Closure) MoveStackElems(bottom, count int) {
	for i := 0; i < count; i++ {
		cl.EmitInst("mov", "rax", EncodeLocal(bottom-count+1+i), "")
		cl.EmitInst("mov", EncodeLocal(1+i), "rax", "")
	}
}

type MathNode struct {
	// this is non-nil for a term
	term *Token

	// if not a term, this is non-nil for the operator
	op *OperatorInfo

	// arguments to operator (R is nil for unary ops)
	L *MathNode
	R *MathNode
}

func (cl *Closure) CompileMathExpr(t *Token) (*Type, error) {
	/* this is Dijkstra's shunting-yard algorithm */

	op_stack := make([]*OperatorInfo, 0)
	term_stack := make([]*MathNode, 0)

	shunt := func() error {
		// the op_stack is never empty when this is called
		op := op_stack[len(op_stack)-1]
		op_stack = op_stack[0 : len(op_stack)-1]

		// possible??
		if len(term_stack) < 2 {
			return ParsingError(t, "FAILURE AT THE SHUNTING YARD")
		}

		L := term_stack[len(term_stack)-2]
		R := term_stack[len(term_stack)-1]
		term_stack = term_stack[0 : len(term_stack)-2]

		node := new(MathNode)
		node.op = op
		node.L = L
		node.R = R

		term_stack = append(term_stack, node)
		return Ok
	}

	// saved unary ops for the next (unseen) term
	un_ops := make([]*OperatorInfo, 0)

	seen_term := false

	for _, tok := range t.Children {
		var op *OperatorInfo

		if tok.Kind == TOK_Name {
			op = operators[tok.Str]
		}

		// if not an operator, it must be a term
		if op == nil {
			if seen_term {
				return nil, ParsingError(tok,
					"bad math expression, missing operator between terms")
			}
			seen_term = true

			term := new(MathNode)
			term.term = tok

			// apply any saved unary operators
			for len(un_ops) > 0 {
				op = un_ops[len(un_ops)-1]
				un_ops = un_ops[0 : len(un_ops)-1]

				node := new(MathNode)
				node.op = op
				node.L = term

				term = node
			}

			term_stack = append(term_stack, term)
			continue
		}

		if op.unary {
			if seen_term {
				return nil, ParsingError(tok,
					"bad math expression, unexpected %s operator", tok.Str)
			}
			un_ops = append(un_ops, op)
			continue
		}

		if !seen_term || len(un_ops) > 0 {
			return nil, ParsingError(tok,
				"bad math expression, unexpected %s operator", tok.Str)
		}

		// shunt existing operators if they have greater precedence
		for len(op_stack) > 0 {
			top := op_stack[len(op_stack)-1]

			if top.precedence > op.precedence ||
				(top.precedence == op.precedence && !top.right_assoc) {

				err := shunt()
				if err != nil {
					return nil, err
				}
				continue
			}

			break
		}

		op_stack = append(op_stack, op)
		seen_term = false
	}

	if !seen_term || len(un_ops) > 0 {
		return nil, ParsingError(t.Children[len(t.Children)-1],
			"bad math expression, missing term after last operator")
	}

	// handle the remaining operators on stack
	for len(op_stack) > 0 {
		err := shunt()
		if err != nil {
			return nil, err
		}
	}

	if len(term_stack) != 1 {
		return nil, ParsingError(t, "PARSE MATH FAIlURE")
	}

	return cl.CompileMathNode(term_stack[0], t)
}

func (cl *Closure) CompileMathNode(nd *MathNode, lnum *Token) (*Type, error) {
	if nd.term != nil {
		return cl.CompileToken(nd.term, false)
	}

	/* this is analogous to a function call (see CompileExpr code) */

	leave_ofs := cl.stack_ofs

	cl.EmitInst("push", "rbp", "", "")
	cl.stack_ofs += 1

	// make a bare CallFrame, fields are set later...
	cl.EmitInst("xor", "rax", "rax", "")
	cl.EmitInst("push", "rax", "", "info")
	cl.EmitInst("push", "rax", "", "code ptr")
	cl.EmitInst("push", "rax", "", "upvalues")

	cl.stack_ofs += 3

	num_params := 1
	if !nd.op.unary {
		num_params = 2
	}

	var child_types [2]*Type

	// evaluate the arguments
	for arg := 0; arg < 2; arg++ {
		child := nd.L
		if arg == 1 {
			child = nd.R
		}
		if child == nil {
			continue
		}

		var err error

		child_types[arg], err = cl.CompileMathNode(child, lnum)
		if err != nil {
			return nil, err
		}

		cl.EmitInst("push", "rax", "", "")
		cl.stack_ofs += 1
	}

	// check types
	if num_params == 2 && !child_types[0].Equal(child_types[1]) {
		return nil, ParsingError(lnum, "incompatible types to %s operator: %s and %s",
			nd.op.name, child_types[0].String(), child_types[1].String())
	}

	fun_def := nd.op.GetDef(child_types[0].base)

	if fun_def == nil {
		return nil, ParsingError(lnum, "wrong type for %s operator, got %s",
			nd.op.name, child_types[0].String())
	}

	if fun_def.loc.Clos._def == nil {
		fun_def.loc.Clos._def = CreateFuncDef(fun_def.loc.Clos, fun_def.name)
	}

	// prevent using operators with custom types.
	// BUT allow the comparison operators (especially equality).
	if child_types[0].custom != "" && !nd.op.IsComparison() {
		return nil, ParsingError(lnum, "wrong type for %s operator, got %s",
			nd.op.name, child_types[0].String())
	}

	// setup the new %rbp value
	cl.EmitInst("mov", "rbp", "rsp", "")
	cl.EmitInst("add", "rbp", fmt.Sprintf("%d", (num_params+3)*8), "")

	// get function info, update the CallFrame
	cl.EmitInst("mov", "rax", "["+EncodeIdent(fun_def.name)+"]", "")
	cl.EmitInst("mov", "[rbp-STACKOFS_INFO]", "rax", "")
	cl.EmitInst("mov", "rax", "[rax]", "")
	cl.EmitInst("mov", "[rbp-STACKOFS_CODE]", "rax", "")

	// get code pointer and call it
	cl.EmitInst("call", "rax", "", "")

	// pop everything
	cl.EmitInst("leave", "", "", "")

	cl.stack_ofs = leave_ofs

	fun_type := fun_def.loc.Clos.kind

	return fun_type.sub, Ok
}

func (cl *Closure) CompileTypeCast(t *Token, user *Type, tail bool) (*Type, error) {
	if len(t.Children) < 2 {
		return nil, ParsingError(t, "missing argument to type cast")
	} else if len(t.Children) > 2 {
		return nil, ParsingError(t, "too many arguments in type cast")
	}

	cl.in_type_cast += 1

	val_type, err := cl.CompileToken(t.Children[1], tail)
	if err != nil {
		return nil, err
	}

	// check that the result is compatible
	if !val_type.Castable(user) {
		return nil, ParsingError(t, "cannot cast type %s to %s",
			val_type.String(), user.String())
	}

	cl.in_type_cast -= 1

	return user, Ok
}

func (cl *Closure) CompileTypeBase(t *Token, tail bool) (*Type, error) {
	children := t.Children

	if len(children) < 2 {
		return nil, ParsingError(t, "missing argument to type base")
	} else if len(children) > 2 {
		return nil, ParsingError(t, "too many arguments in type base")
	}

	cl.in_type_cast += 1

	kind, err := cl.CompileToken(t.Children[1], tail)
	if err != nil {
		return nil, err
	}

	base_kind := kind.Copy()
	base_kind.custom = ""

	cl.in_type_cast -= 1

	return base_kind, Ok
}

func (cl *Closure) CompileReadElement(arr_type *Type, children []*Token) (*Type, error) {
	// the array/string/tuple object is in %rax

	if len(children) < 1 {
		return nil, ParsingError(children[0], "missing index for array/tuple access")
	}

	indexes := children[1:]

	for _, tok := range indexes {
		idx_type, err := cl.CompileAccess(arr_type, tok, false /* is_write */)
		if err != nil {
			return nil, err
		}

		arr_type = idx_type
	}

	return arr_type, Ok
}

func (cl *Closure) CompileAssign(t *Token) (*Type, error) {
	if len(t.Children) < 3 {
		return nil, ParsingError(t, "bad assignment syntax")
	}

	targ := t.Children[1]
	nval := t.Children[len(t.Children)-1]
	indexes := t.Children[2 : len(t.Children)-1]

	if len(indexes) > 0 {
		return cl.CompileWriteElement(targ, nval, indexes)
	}

	if targ.Kind != TOK_Name {
		return nil, ParsingError(t, "expected var name, got: %s", targ.String())
	}

	// compute the value
	val_type, err := cl.CompileToken(nval, false)
	if err != nil {
		return nil, err
	}

	name := targ.Str
	lvar := cl.LookupLocal(name)

	if lvar != nil && lvar.immutable {
		return nil, ParsingError(t, "loop variable '%s' is read-only", name)
	}

	var dest_type *Type

	if lvar == nil {
		// identifier is not a local, try a global
		g_idx, exist := global_lookup[name]
		if !exist {
			return nil, ParsingError(t, "unknown binding: '%s'", name)
		}

		g_name := EncodeIdent(name)

		cl.EmitInst("mov", "["+g_name+"]", "rax", "")

		dest_type = globals[g_idx].loc.kind

	} else if lvar.owner == cl {
		// var is local to this function
		access := fmt.Sprintf("[rbp-%d]", lvar.offset*8)
		cl.EmitInst("mov", access, "rax", "local "+name)

		dest_type = lvar.kind

	} else {
		// var is local to a parent -- an upvalue
		upv_idx := cl.CaptureLocal(lvar)

		// NOTE: index_upvalue will not modify %rax
		cl.EmitInst("mov", "rcx", fmt.Sprintf("%d", upv_idx), "")
		cl.EmitInst("call", "index_upvalue", "", "")
		cl.EmitInst("mov", "[rdi]", "rax", "")

		dest_type = lvar.kind
	}

	// perform type check
	if dest_type.Equal(val_type) {
		// ok
	} else if dest_type.CastableFromUntyped(val_type, nval.Kind) {
		// ok
	} else {
		return nil, ParsingError(t, "type mismatch in assignment, wanted %s, got %s",
			dest_type.String(), val_type.String())
	}

	// ensure the result is always NIL
	cl.EmitInst("mov", "rax", "NIL", "")

	return int_type, Ok
}

func (cl *Closure) CompileWriteElement(targ, nval *Token, indexes []*Token) (*Type, error) {
	// must do the value first
	val_type, err := cl.CompileToken(nval, false)
	if err != nil {
		return nil, err
	}

	cl.EmitInst("push", "rax", "", "")
	cl.stack_ofs += 1

	// the array/tuple itself
	arr_type, err := cl.CompileToken(targ, false)
	if err != nil {
		return nil, err
	}

	// apply all indices *except* the last one
	for i := 0; i < len(indexes)-1; i++ {
		idx_type, err := cl.CompileAccess(arr_type, indexes[i], false /* is_write */)
		if err != nil {
			return nil, err
		}

		arr_type = idx_type
	}

	// this will pop the value and restore cl.stack_ofs
	dest_type, err := cl.CompileAccess(arr_type, indexes[len(indexes)-1], true /* is_write */)
	if err != nil {
		return nil, err
	}

	// perform type check
	if dest_type.Equal(val_type) {
		// ok
	} else if dest_type.CastableFromUntyped(val_type, nval.Kind) {
		// ok
	} else {
		return nil, ParsingError(nval, "type mismatch in assignment, wanted %s, got %s",
			dest_type.String(), val_type.String())
	}

	// ensure the result is always NIL
	cl.EmitInst("mov", "rax", "NIL", "")

	return int_type, Ok
}

func (cl *Closure) CompileAccess(arr_type *Type, t *Token, is_write bool) (*Type, error) {
	// %rax contains the object to access (arr_type is its type).
	// 't' is the token for the key/field/index.
	// for writing, value is on the stack (at the bottom).
	// for reading, %rax will contain the result.

	if arr_type.base == VAL_Tuple {
		// check if key is an identifier like ".0", ".1", ".2"
		if t.Kind != TOK_Name {
			return nil, ParsingError(t, "bad tuple key, wanted identifier, got %s", t.String())
		}
		if !t.IsField() {
			return nil, ParsingError(t, "bad tuple key, identifier must begin with a dot")
		}

		var tuple_idx int
		n, _ := fmt.Sscanf(t.Str[1:], "%d", &tuple_idx)
		if n != 1 || tuple_idx < 0 {
			// support named tuple fields
			tuple_idx = -1

			if arr_type.custom != "" && t.IsNamedField() {
				tuple_idx = arr_type.FindParam(t.Str)
			}

			if tuple_idx < 0 {
				return nil, ParsingError(t, "bad or unknown tuple key: %s", t.Str)
			}
		}

		// check if tuple_idx is valid, and get its type.
		if tuple_idx >= len(arr_type.param) {
			return nil, ParsingError(t, "bad tuple key, out-of-range (.%d > %d)",
				tuple_idx, len(arr_type.param)-1)
		}

		elem_type := arr_type.param[tuple_idx].kind

		cl.EmitInst("mov", "rdi", "rax", "")

		addr := fmt.Sprintf("[rdi+%d]", tuple_idx*8)

		if is_write {
			cl.EmitInst("pop", "rax", "", "")
			cl.EmitInst("mov", addr, "rax", "")

			cl.stack_ofs -= 1
		} else {
			cl.EmitInst("mov", "rax", addr, "")
		}

		return elem_type, Ok
	}

	/* arrays and string */

	cl.EmitInst("push", "rax", "", "")
	cl.stack_ofs += 1

	// compile the index expression
	idx_type, err := cl.CompileToken(t, false)
	if err != nil {
		return nil, err
	}

	// check type of indexor
	if idx_type.base != VAL_Int {
		return nil, ParsingError(t, "bad array indexor, wanted int, got %s",
			idx_type.String())
	}

	switch arr_type.base {
	case VAL_Array:
		// NOTE: index_array will not modify %rax
		cl.EmitInst("pop", "rdi", "", "")
		cl.EmitInst("mov", "rcx", "rax", "")
		cl.EmitInst("call", "index_array", "", "")

		cl.stack_ofs -= 1

		if is_write {
			cl.EmitInst("pop", "rax", "", "")
			cl.EmitInst("mov", "[rdi]", "rax", "")

			cl.stack_ofs -= 1
		} else {
			cl.EmitInst("mov", "rax", "[rdi]", "")
		}

		return arr_type.sub, Ok

	case VAL_String:
		if is_write {
			return nil, ParsingError(t, "bad assignment, cannot write to a string")
		}

		cl.EmitInst("pop", "rsi", "", "")
		cl.EmitInst("call", "index_string", "", "")

		cl.stack_ofs -= 1

		return int_type, Ok
	}

	if is_write {
		return nil, ParsingError(t, "bad assignment, expected array/tuple, got %s",
			arr_type.base.String())
	}

	return nil, ParsingError(t, "bad access, expected array/tuple, got %s",
		arr_type.base.String())
}

func (cl *Closure) CompileBegin(t *Token, tail bool) (*Type, error) {
	children := t.Children[1:]

	// no elements?
	if len(children) == 0 {
		cl.EmitInst("mov", "rax", "NIL", "")
		return int_type, Ok
	}

	// compile each child expression.
	// the last one will provide the final "result" without having
	// to do anything special.

	var last_kind *Type

	for idx, tok := range children {
		if idx > 0 {
			cl.EmitBlank()
		}

		sub_tail := tail && (idx == len(children)-1)

		sub_kind, err := cl.CompileToken(tok, sub_tail)
		if err != nil {
			return nil, err
		}

		last_kind = sub_kind
	}

	return last_kind, Ok
}

func (cl *Closure) CompileConcat(t *Token) (*Type, error) {
	children := t.Children[1:]

	// begin with an empty string
	cl.EmitInst("mov", "rax", "empty_str", "start $")
	cl.EmitInst("push", "rax", "", "")
	cl.stack_ofs += 1

	build_ofs := cl.stack_ofs
	build_v := fmt.Sprintf("[rbp-%d]", 8*build_ofs)

	for idx, sub := range children {
		if idx > 0 {
			cl.EmitInst("mov", "rsi", build_v, "")
			cl.EmitInst("mov", "rdi", "space_str", "")
			cl.EmitInst("call", "append_string", "", "")
			cl.EmitInst("mov", build_v, "rdi", "")
		}

		sub_type, err := cl.CompileToken(sub, false)
		if err != nil {
			return nil, err
		}

		if sub_type.base == VAL_String {
			cl.EmitInst("mov", "rdi", "rax", "")
		} else {
			ctd := CreateCompactTypeDef(sub_type)

			cl.EmitInst("mov", "rsi", ctd.label, "")
			cl.EmitInst("call", "format_string", "", "")
		}

		cl.EmitInst("mov", "rsi", build_v, "")
		cl.EmitInst("call", "append_string", "", "")
		cl.EmitInst("mov", build_v, "rdi", "")
	}

	cl.EmitInst("mov", "rax", build_v, "")

	cl.PopLocalVars(1)

	return str_type, Ok
}

func (cl *Closure) CompileDup(t *Token) (*Type, error) {
	if len(t.Children) != 2 {
		return nil, ParsingError(t, "wrong # args to dup, wanted 1, got %d",
			len(t.Children)-1)
	}

	val_tok := t.Children[1]

	val_type, err := cl.CompileToken(val_tok, false)
	if err != nil {
		return nil, err
	}

	if val_type.base != VAL_Tuple {
		return nil, ParsingError(t, "dup requires a tuple, got %s",
			val_type.base.String())
	}

	num_elems := len(val_type.param)

	cl.EmitInst("mov", "ecx", fmt.Sprintf("%d", num_elems), "")
	cl.EmitInst("call", "duplicate_tuple", "", "")
	cl.EmitInst("mov", "rax", "rdi", "")

	return val_type, Ok
}

func (cl *Closure) CompileLambda(t *Token) (*Type, error) {
	kind, err := ParseLambdaType(t, nil)
	if err != nil {
		return nil, ParsingError(t, "%s", err.Error())
	}

	// compile code into a template closure
	template := new(Closure)
	template.uncompiled = t.Children
	template.kind = kind
	template.parent = cl

	lambda_templates = append(lambda_templates, template)

	// generate a unique name
	name := fmt.Sprintf("%s::lam%d", cl.debug_name, cl.lambda_counter)
	cl.lambda_counter += 1

	err = CompileFunction(template, name, nil)
	if err != nil {
		return nil, ParsingError(t, "%s", err.Error())
	}

	template._def = CreateFuncDef(template, name)

	cl.EmitInst("mov", "rsi", template._def.label, "")
	cl.EmitInst("call", "make_lambda", "", "")
	cl.EmitInst("mov", "rax", "rdi", "")

	return kind, Ok
}

func (cl *Closure) CompileLet(t *Token, tail bool) (*Type, error) {
	children := t.Children[1:]

	if len(children) < 3 || len(children)%2 == 0 {
		return nil, ParsingError(t, "wrong # args to let")
	}

	cl.PushLocalGroup()

	num_vars := 0

	// for each variable, compute the value and create a binding
	for i := 0; i+1 < len(children); i += 2 {
		t_var := children[i]
		t_exp := children[i+1]

		if t_var.Kind != TOK_Name {
			return nil, ParsingError(t, "expected var name, got: %s", t_var.String())
		}
		if !ValidDefName(t_var.Str) {
			return nil, ParsingError(t, "bad var name: '%s' is a reserved word", t_var.Str)
		}

		// allocate slot for local var
		if cl.locals.vars[t_var.Str] != nil {
			return nil, ParsingError(t, "duplicate var name '%s'", t_var.Str)
		}

		// compile code to generate the value
		var_type, err := cl.CompileToken(t_exp, false)
		if err != nil {
			return nil, err
		}

		cl.EmitInst("push", "rax", "", "let "+t_var.Str)
		cl.stack_ofs += 1

		cl.AddLocal(t_var.Str, var_type, cl.stack_ofs, false /* immutable */)
		num_vars += 1
	}

	cl.EmitBlank()

	// compile the body itself
	body := children[len(children)-1]

	body_type, err := cl.CompileToken(body, tail)
	if err != nil {
		return nil, err
	}

	// this also adds code to hoist captured vars
	cl.PopLocalGroup()

	cl.PopLocalVars(num_vars)

	return body_type, Ok
}

func (cl *Closure) CompileIf(t *Token, tail bool) (*Type, error) {
	children := t.Children[1:]

	if len(children) < 2 {
		return nil, ParsingError(t, "wrong # args to if")
	}

	var res_type *Type

	// check for optional "else" near the end
	if len(children) >= 4 && children[len(children)-2].Match("else") {
		children[len(children)-2] = children[len(children)-1]
		children = children[0 : len(children)-1]
	}

	// if no else clause, add one
	if (len(children) % 2) == 0 {
		else_tok := Token{Kind: TOK_Name, Str: "NIL", LineNum: t.LineNum}
		children = append(children, &else_tok)
	}

	// allocate some label counters
	lab_base := cl.lab_counter
	cl.lab_counter += (len(children) - 1) / 2

	done_name := fmt.Sprintf(".ifdone%d", lab_base)

	pos := 0
	end_pos := len(children) - 3

	for ; pos <= end_pos; pos += 2 {
		cond := children[pos]
		then := children[pos+1]

		// compile the condition
		cond_type, err := cl.CompileToken(cond, false)
		if err != nil {
			return nil, err
		}

		// require an integer for the condition
		if cond_type.base != VAL_Int {
			return nil, ParsingError(cond, "if condition must be an integer, got %s",
				cond_type.String())
		}

		else_name := fmt.Sprintf(".ifelse%d", lab_base)

		cl.EmitInst("cmp", "eax", "NIL", "")
		cl.EmitInst("je", else_name, "", "")

		// compile the body for when condition is true
		then_type, err := cl.CompileToken(then, tail)
		if err != nil {
			return nil, err
		}

		// check type is compatible with other bodies
		if res_type == nil {
			res_type = then_type
		} else if !res_type.Equal(then_type) {
			return nil, ParsingError(then, "type mismatch with 'if' results: %s and %s",
				res_type.String(), then_type.String())
		}

		// jump to the very end
		cl.EmitInst("jmp", done_name, "", "")

		if pos < end_pos {
			cl.EmitLabel(else_name)
			lab_base += 1
		}
	}

	// compile the ELSE expression
	else_name := fmt.Sprintf(".ifelse%d", lab_base)
	cl.EmitLabel(else_name)

	else_type, err := cl.CompileToken(children[end_pos+2], tail)
	if err != nil {
		return nil, err
	}

	if !res_type.Equal(else_type) {
		return nil, ParsingError(t, "type mismatch with 'if' results: %s and %s",
			res_type.String(), else_type.String())
	}

	// emit the done target
	cl.EmitLabel(done_name)

	return res_type, Ok
}

func (cl *Closure) CompileWhile(t *Token) (*Type, error) {
	if len(t.Children) != 3 {
		return nil, ParsingError(t, "wrong # args to while")
	}

	cond := t.Children[1]
	body := t.Children[2]

	// create labels
	lab_num := cl.lab_counter
	cl.lab_counter += 1

	cond_label := fmt.Sprintf(".while%d", lab_num)
	end_label := fmt.Sprintf(".wend%d", lab_num)

	cl.EmitLabel(cond_label)

	// eval condition....
	cond_type, err := cl.CompileToken(cond, false)
	if err != nil {
		return nil, err
	}

	// require an integer for the condition
	if cond_type.base != VAL_Int {
		return nil, ParsingError(cond, "while condition must be an integer, got %s",
			cond_type.String())
	}

	// jump to end if value is not true
	cl.EmitInst("cmp", "eax", "NIL", "")
	cl.EmitInst("je", end_label, "", "")

	// body....
	_, err = cl.CompileToken(body, false)
	if err != nil {
		return nil, err
	}

	// jump back to beginning
	cl.EmitInst("jmp", cond_label, "", "")

	cl.EmitLabel(end_label)

	// ensure the result is NIL
	cl.EmitInst("mov", "rax", "NIL", "")

	return int_type, Ok
}

func (cl *Closure) CompileForRange(t *Token) (*Type, error) {
	children := t.Children[1:]

	if len(children) != 4 {
		return nil, ParsingError(t, "wrong # args to for")
	}

	// create labels
	lab_num := cl.lab_counter
	cl.lab_counter += 1

	loop_label := fmt.Sprintf(".forloop%d", lab_num)
	end_label := fmt.Sprintf(".forend%d", lab_num)

	cl.PushLocalGroup()

	// get the variable name
	t_var := children[0]

	if t_var.Kind != TOK_Name {
		return nil, ParsingError(t, "expected var name, got: %s", t_var.String())
	}
	if !ValidDefName(t_var.Str) {
		return nil, ParsingError(t, "bad var name: '%s' is a reserved word", t_var.Str)
	}

	// we need three stack slots: the var, the target and the step.

	// compute start
	kind, err := cl.CompileToken(children[1], false)
	if err != nil {
		return nil, err
	}
	if kind.base != VAL_Int {
		return nil, ParsingError(t, "type mismatch in for loop, wanted int, got %s",
			kind.String())
	}

	cl.EmitInst("push", "rax", "", "start")
	cl.stack_ofs += 1

	start_v := EncodeLocal(cl.stack_ofs)

	cl.AddLocal(t_var.Str, int_type, cl.stack_ofs, true)

	// compute end
	kind, err = cl.CompileToken(children[2], false)
	if err != nil {
		return nil, err
	}
	if kind.base != VAL_Int {
		return nil, ParsingError(t, "type mismatch in for loop, wanted int, got %s",
			kind.String())
	}

	cl.EmitInst("push", "rax", "", "end")
	cl.stack_ofs += 1

	end_v := EncodeLocal(cl.stack_ofs)

	// compute the step
	cl.EmitInst("mov", "eax", start_v, "")
	cl.EmitInst("cmp", "eax", end_v, "")
	cl.EmitInst("mov", "eax", "1", "")
	cl.EmitInst("mov", "rcx", "-1", "")
	cl.EmitInst("cmovg", "rax", "rcx", "")

	cl.EmitInst("push", "rax", "", "step")
	cl.stack_ofs += 1

	step_v := EncodeLocal(cl.stack_ofs)

	// run the body -- it will always run at least once
	// [ unless we extend the syntax with an explicit step... ]
	cl.EmitLabel(loop_label)

	body := children[3]

	_, err = cl.CompileToken(body, false)
	if err != nil {
		return nil, err
	}

	// check if reached the end
	// [ this assumes step is either -1 or +1 ]
	cl.EmitInst("mov", "eax", start_v, "")
	cl.EmitInst("cmp", "eax", end_v, "")
	cl.EmitInst("je", end_label, "", "")

	// add the step and loop back
	cl.EmitInst("add", "eax", step_v, "")
	cl.EmitInst("mov", start_v, "eax", "")
	cl.EmitInst("jmp", loop_label, "", "")

	cl.EmitLabel(end_label)

	// ensure the result is always NIL
	cl.EmitInst("mov", "rax", "NIL", "")

	// restore stuff
	cl.PopLocalGroup()
	cl.PopLocalVars(3)

	return int_type, Ok
}

func (cl *Closure) CompileForEach(t *Token, kind BaseType) (*Type, error) {
	children := t.Children[1:]

	if len(children) != 4 {
		return nil, ParsingError(t, "wrong # args to for-each")
	}

	// create labels
	lab_num := cl.lab_counter
	cl.lab_counter += 1

	loop_label := fmt.Sprintf(".eachloop%d", lab_num)
	done_label := fmt.Sprintf(".eachdone%d", lab_num)

	cl.PushLocalGroup()

	// compile the expression (the array/map/string).
	// need to do this NOW so we know the array's sub-type.
	obj_tok := children[2]

	obj_type, err := cl.CompileToken(obj_tok, false)
	if err != nil {
		return nil, err
	}

	var elem_type *Type

	switch kind {
	case VAL_Array:
		if obj_type.base != VAL_Array {
			return nil, ParsingError(t, "for-each requires an array, got %s",
				obj_type.String())
		}

		elem_type = obj_type.sub
		//?? op_begin = OP_ARR_BEGIN

	case VAL_String:
		if obj_type.base != VAL_String {
			return nil, ParsingError(t, "str-for-each requires a string, got %s",
				obj_type.String())
		}

		elem_type = int_type // character, actually
		//?? op_begin = OP_STR_BEGIN
	}

	// get the variable names
	t_idx := children[0]
	t_elem := children[1]

	if t_idx.Kind != TOK_Name {
		return nil, ParsingError(t, "expected var name, got: %s", t_idx.String())
	}
	if t_elem.Kind != TOK_Name {
		return nil, ParsingError(t, "expected var name, got: %s", t_elem.String())
	}
	if !ValidDefName(t_idx.Str) {
		return nil, ParsingError(t, "bad var name: '%s' is a reserved word", t_idx.Str)
	}
	if !ValidDefName(t_elem.Str) {
		return nil, ParsingError(t, "bad var name: '%s' is a reserved word", t_elem.Str)
	}
	if t_idx.Str == t_elem.Str {
		return nil, ParsingError(t, "duplicate var name '%s'", t_idx.Str)
	}

	// we occupy three stack slots: two for the variables and
	// one to remember the object we are iterating over.

	cl.EmitInst("push", "rax", "", "obj")
	cl.stack_ofs += 1

	obj_slot := cl.stack_ofs

	// create the two variables, initial values are zero
	cl.EmitInst("xor", "rax", "rax", "")
	cl.EmitInst("push", "rax", "", "index")
	cl.stack_ofs += 1

	idx_slot := cl.stack_ofs

	cl.EmitInst("push", "rax", "", "elem")
	cl.stack_ofs += 1

	elem_slot := cl.stack_ofs

	cl.AddLocal(t_idx.Str, int_type, idx_slot, true)
	cl.AddLocal(t_elem.Str, elem_type, elem_slot, true)

	// check if gone past the end...
	// [ by coincidence, arrays AND strings begin with a 32-bit length ]

	cl.EmitLabel(loop_label)

	switch kind {
	case VAL_Array, VAL_String:
		cl.EmitInst("mov", "rsi", EncodeLocal(obj_slot), "obj")
		cl.EmitInst("mov", "ecx", "[rsi]", "length")
		cl.EmitInst("mov", "rax", EncodeLocal(idx_slot), "idx")
		cl.EmitInst("cmp", "eax", "ecx", "")
		cl.EmitInst("jge", done_label, "", "")
	}

	// load the next element...
	switch kind {
	case VAL_Array:
		cl.EmitInst("mov", "rdi", "rsi", "")
		cl.EmitInst("mov", "rcx", "rax", "")
		cl.EmitInst("call", "index_array", "", "")
		cl.EmitInst("mov", "rax", "[rdi]", "")

	case VAL_String:
		cl.EmitInst("call", "index_string", "", "")
	}

	cl.EmitInst("mov", EncodeLocal(elem_slot), "rax", "")

	// compile the body
	body := children[3]

	_, err = cl.CompileToken(body, false)
	if err != nil {
		return nil, err
	}

	// increment index and loop
	cl.EmitInst("mov", "rax", EncodeLocal(idx_slot), "")
	cl.EmitInst("inc", "rax", "", "")
	cl.EmitInst("mov", EncodeLocal(idx_slot), "rax", "")
	cl.EmitInst("jmp", loop_label, "", "")

	cl.EmitLabel(done_label)

	// ensure the result is always NIL
	cl.EmitInst("mov", "rax", "NIL", "")

	// restore stuff
	cl.PopLocalGroup()
	cl.PopLocalVars(3)

	return int_type, Ok
}

func ParsingError(t *Token, format string, a ...interface{}) error {
	return LocErrorf(t.LineNum, format, a...)
}
